MODELDIR=./
NATIVE=../../all_pdb_over9/PDB/6ESQ.pdb
SCORES=./complex_scores.csv
MMALIGN=../../../src/complex_assembly/MMalign
OUTNAME=./mmscores.csv
python3 ../../../src/complex_assembly/run_mmalign.py --modeldir $MODELDIR \
--native $NATIVE --scores $SCORES \
--MMalign $MMALIGN --outname $OUTNAME
