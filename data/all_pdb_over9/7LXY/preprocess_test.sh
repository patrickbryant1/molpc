#Write joint files using all states in the biological assemblies and
#Get all true interactions from the selected PDB files
PDBDIR=../../data/all_pdb_over9/PDB/
PDBID=7LXY
META=../../data/all_pdb_over9/selected_complexes.csv
OUTDIR=./
python3 ../../src/preprocess/all_pdb_over9/get_ints_allow_err.py --pdbdir $PDBDIR \
--pdbid $PDBID --meta $META --outdir $OUTDIR

#Match the pdb sequences to seqres and see which are thereby interacting
#Also double check the stoichiometry and write the stoichiometry
#of each chain in meta according to the PDB file
PDBDIR=./
META=../../data/all_pdb_over9/selected_complexes.csv
python3 ../../src/preprocess/all_pdb_over9/pdbseq_to_useq.py --pdbdir $PDBDIR --meta $META
