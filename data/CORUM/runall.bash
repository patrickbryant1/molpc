#!/bin/bash -x

# Download coreComplexes.txt



#wget https://mips.helmholtz-muenchen.de/corum/download/coreComplexes.txt.zip
#unzip coreComplexes.txt.zip

# And extract big complexes >=10 chains

dir=`basename $1 .txt`


gawk -F "\t" '{print $6}' $1 | gawk -F  ";" '{if  (NF>=10 ) print $0 }' > $dir/bigcomplex.txt
gawk -F "\t" '{print $6}' $1 | gawk -F  ";" '{if  (NF>=1 && NF<=6  ) print $0 }' > $dir/smallcomplex.txt
#gawk -F "\t" '{print $6}' allComplexes.txt | gawk -F  ";" '{if  (NF>=10 ) print $0 }' > bigcomplex.txt
#gawk -F "\t" '{print $6}' allComplexes.txt | gawk -F  ";" '{if  (NF>=3 && NF<=6  ) print $0 }' > smallcomplex.txt


# Download (manually fasta and tab files from uniprot)
sed "s/;/\n/g" $dir/bigcomplex.txt  | sort -u > $dir/bigcomplex-ids.txt
sed "s/;/\n/g" $dir/smallcomplex.txt  | sort -u > $dir/smallcomplex-ids.txt

#gunzip uniprot-yourlist\ M202202225BF3C56A578D7D6DFD1FC81EE5DA773042CC6BY.*gz
# Rename
#mv uniprot-yourlist\ M202202225BF3C56A578D7D6DFD1FC81EE5DA773042CC6BY.fasta bigcomplex-ids.fasta
#mv uniprot-yourlist\ M202202225BF3C56A578D7D6DFD1FC81EE5DA773042CC6BY.tab bigcomplex-ids.tab

# for small the same

# Extra fasta files for each complex
mkdir $dir/bigcomplexes
rm $dir/bigcomplexes/*
#k=0
for i in `cat $dir/bigcomplex.txt`
do
    # k=$((k+1))
    k=`grep $i allComplexes.txt | head -1 | gawk '{print $1}'`
    #echo $i $k
    for j in `echo $i | sed "s/\;/ /g"`
    do
	echo -n "> $j PDB: " >> $dir/bigcomplexes/$k.fasta
	grep $j allchains-ids.tab |head -1 | gawk -F "\t" '{print $7}' >> $dir/bigcomplexes/$k.fasta
	grep $j allchains-ids.tab |head -1 | gawk -F "\t" '{print $8}' >> $dir/bigcomplexes/$k.fasta
    done
done


# Extract some summary information

echo "Name,NumChain,NumPDB,NumMissing,Complete[%]" > $dir/summary.csv
for i in $dir/bigcomplexes/*fasta ; do j=` grep -c \> $i ` ; k=` grep  -cE "PDB: $" $i` ; m=$((j-k)) ; n=$((j-m)); f=$((100*$m/$j))  ; echo $i,$j,$m,$n,$f ; done >> $dir/summary.csv


# Extra fasta files for each complex
mkdir $dir/smallcomplexes
rm $dir/smallcomplexes/*
k=0
for i in `cat smallcomplex.txt`
do
    #k=$((k+1))
    k=`grep $i allComplexes.txt |head -1 | gawk '{print $1}'`
    for j in `echo $i | sed "s/\;/ /g"`
    do
	echo -n "> $j PDB: " >> $dir/smallcomplexes/$k.fasta
	grep $j allchains-ids.tab | gawk -F "\t" '{print $7}' >> $dir/smallcomplexes/$k.fasta
	grep $j allchains-ids.tab | gawk -F "\t" '{print $8}' >> $dir/smallcomplexes/$k.fasta
    done
done


# Extract some summary information

echo "Name,NumChain,NumPDB,NumMissing,Complete[%]" > $dir/summary-small.csv
for i in $dir/smallcomplexes/*fasta
do
    j=` grep -c \> $i `
    k=` grep  -cE "PDB: $" $i`
    m=$((j-k))
    n=$((j-m))
    f=$((100*$m/$j))
    echo $i,$j,$m,$n,$f
done >> $dir/summary-small.csv


# All


# Extra fasta files for each complex
gawk -F "\t" '{print $6}' $dir.txt | grep -v subunits > $dir/allchains.txt
sed "s/;/\n/g" $dir/allchains.txt  | sort -u > $dir/allchains-ids.txt

#mkdir -p $dir/all
#rm $dir/all/*
##k=0
#for i in `cat $dir/allchains.txt`
#do
#    #k=$((k+1))
#    k=`grep $i allComplexes.txt | gawk '{print $1}' | head -1`
#    #echo $i,$k
#    for j in `echo $i | sed "s/\;/ /g"`
#    do
#	#echo $j
#	echo -n "> $j PDB: " >> $dir/all/$k.fasta
#	grep $j allchains-ids.tab | gawk -F "\t" '{print $9}' >> $dir/all/$k.fasta
#	grep $j allchains-ids.tab | gawk -F "\t" '{print $8}' >> $dir/all/$k.fasta
#    done
#done


# Extract from mmseq
for i in seq/*.m8 ; do  gawk '{ if (i==0) printf "%s\t",$1;i=i+1; s=substr($2,0,4) ; if (! match(S,s)) { S=((S)(s)) ; printf "%s;",s}};END{printf "\n"}' $i ;done


for i in $dir/all/*.fasta ; do k=`basename $i .fasta` ; echo -n $k " "  ; for j in `grep \> $i | gawk '{print $2}'` ; do l=`grep -E "^$j\s" $dir/allchains-pdb.tsv |  gawk '{print $2}'  ` ; echo -n $l " " ; done  ; echo " " ; done >  $dir/allcomplexes-pdb.tsv

for i in $dir/all/*.fasta ; do k=`basename $i .fasta` ;  for j in `grep \> $i | gawk '{print $2}'` ; do l=`grep -E "^$j\s" $dir/allchains-pdb.tsv |  gawk '{print $2}'` ; echo $k " " $l  ;  done  ;  done >  $dir/allcomplexes-pdbchains.tsv


for i in $dir/all/*.fasta ; do k=`basename $i .fasta` ;  for j in  `grep -Ew ^$k $dir/allcomplexes-pdbchains.tsv | gawk '{print $2}' | sed "s/\;/\n/g" | sort -u ` ; do l=` grep -Ew ^$k $dir/allcomplexes-pdbchains.tsv | grep -c $j ` ; echo $k,$j,$l ; done ; done > $dir/allcomplexes-numhits.csv

# allcomplexes-maxghits.cst

python3 <<EOF
import pandas as pd
a=pd.read_csv("$dir/allcomplexes-numhits.csv",names=["name","pdb","num"])
b=a.groupby(["name"]).max()
b.to_csv("$dir/allcomplexes-maxhits.csv")
EOF


echo "Name,NumChain,NumPDB,NumMissing,Complete[%],NumPDBhomology,NumMissingHomology,CompleteHomology[%],MaxChainsHomology,CoverageHomology[%]" > $dir/all.csv
for i in  $dir/all/*.fasta
do
    l=`basename $i .fasta`
    j=` grep -c \> $i `
    k=` grep  -cE "PDB: $" $i`
    m=$((j-k))
    n=$((j-m))
    f=$((100*$m/$j))
    o=`grep -wE ^$l  $dir/allcomplexes-pdb.tsv | gawk '{print NF - 1} ' `
    p=$o
    q=$((j-p))
    r=$((100*$p/$j))
    u=`grep -wE ^$l  $dir/allcomplexes-maxhits.csv |  gawk -F "," '{print $3}' `
    if [ -z ${u} ]; then u=0; fi
    #echo $l,$u,$j
    t=$((100*${u}/${j})) 
    echo $l,$j,$m,$n,$f,$o,$q,$r,${u},${t}
done >>  $dir/all.csv
