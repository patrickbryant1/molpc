

#Networks
montage 1OS4_network.png  1QMV_network.png  6ID0_network.png  6QW6_network.png  \
6RGQ_network.png  6WLZ_network.png  7AE1_network.png  7DVQ_network.png -tile 4x2 -geometry +2+2 nets.png


#6RGQ path
montage sub_network1.png \
sub_network2.png \
sub_network3.png \
sub_network4.png \
sub_network6.png \
sub_network7.png \
sub_network8.png \
sub_network10.png \
sub_network11.png \
sub_network13.png \
sub_network14.png \
sub_network15.png \
sub_network16.png \
sub_network17.png \
sub_network18.png \
sub_network20.png \
sub_network21.png \
sub_network22.png \
sub_network23.png \
sub_network24.png \
sub_network25.png \
sub_network26.png -geometry +2+2 -tile 6x4 6rgq_path11.png