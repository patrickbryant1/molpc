import argparse
import sys
import os
import numpy as np
import pandas as pd
from collections import Counter, defaultdict
import glob
import subprocess
from scipy.spatial import distance
from Bio.SVDSuperimposer import SVDSuperimposer
import pdb

parser = argparse.ArgumentParser(description = '''Analyse interface overlaps between different binary interactions.''')

parser.add_argument('--modeldir', nargs=1, type= str, default=sys.stdin, help = 'Path to data with binary complexes.')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb ids and interacting chains.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Path to output csv')


##############FUNCTIONS###############

def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''

    chain_coords = {}
    with open(pdbfile) as file:
        for line in file:
            record = parse_atm_record(line)
            if record['atm_name']!='CA':
                continue

            if record['chain'] in [*chain_coords.keys()]:
                chain_coords[record['chain']].append([record['x'],record['y'],record['z']])
            else:
                chain_coords[record['chain']]= [[record['x'],record['y'],record['z']]]

    return chain_coords

def check_overlap(row_i, row_j, modeldir,oc):
    '''Check if the new rotated chain overlaps with any in the complex
    '''

    #Read pdb files
    pdbi_coords = read_pdb(modeldir+row_i.Source+'_'+row_i.Chain1+'-'+row_i.Source+'_'+row_i.Chain2+'.pdb')
    pdbj_coords = read_pdb(modeldir+row_j.Source+'_'+row_j.Chain1+'-'+row_j.Source+'_'+row_j.Chain2+'.pdb')


    #Set the coordinates to be superimposed.
    #coords will be put on top of reference_coords.
    reference_coords = np.array(pdbi_coords[oc])
    coords =  np.array(pdbj_coords[oc])
    sup = SVDSuperimposer()
    sup.set(reference_coords, coords)
    sup.run()
    rot, tran = sup.get_rotran()
    #Rotate coords on reference_coords (coords from non-overlapping (no) chain
    #to its new relative position/orientation)
    #This is done for the missing chain from pdbj
    no_j = np.setdiff1d([*pdbj_coords.keys()],oc)[0]
    no_coords_j = np.dot(np.array(pdbj_coords[no_j]), rot) + tran

    #Check overlap
    no_i = np.setdiff1d([*pdbi_coords.keys()],oc)[0]
    no_coords_i = pdbi_coords[no_i]

    print('Checking overlap...')
    #Compare with rotated_coords
    #Calc 2-norm
    mat = np.append(no_coords_j,no_coords_i,axis=0)
    a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
    dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
    l1 = len(no_coords_j)
    contact_dists = dists[l1:,:l1] #Lower triangular
    overlap = np.argwhere(contact_dists<=5)

    #Check that the overlap is less then 50% of the shortest chain
    if overlap.shape[0]>0.5*min(l1,len(no_coords_i)):
        print('Overlap found')
        return True
    else:
        print('No overlap')
        return False





################MAIN###############
#Parse args
args = parser.parse_args()
#Data
modeldir = args.modeldir[0]
meta = pd.read_csv(args.meta[0])
outname = args.outname[0]

#Go through all chains and see if there are overlapping interfaces
chains = np.unique(np.concatenate([meta.Chain1.values,meta.Chain2.values]))

#Save the overlaps
overlap_df = {'Source1':[], 'Source2':[],'Dimer1':[], 'Dimer2':[]}

for chain in chains:
    sel = meta[(meta.Chain1==chain)|(meta.Chain2==chain)]
    sel_inds = sel.index

    if len(sel_inds)>1:
        #Go through all combinations of dimers and check the overlaps
        for i in range(len(sel_inds)):
            row_i = sel.loc[sel_inds[i]]
            for j in range(i+1,len(sel_inds)):
                row_j = sel.loc[sel_inds[j]]
                overlap = check_overlap(row_i, row_j, modeldir, chain)

                if overlap==True:
                    overlap_df['Source1'].append(row_i.Source)
                    overlap_df['Source2'].append(row_j.Source)
                    overlap_df['Dimer1'].append(row_i.Chain1+'-'+row_i.Chain2)
                    overlap_df['Dimer2'].append(row_j.Chain1+'-'+row_j.Chain2)

#Create overlap df
overlap_df = pd.DataFrame.from_dict(overlap_df)
overlap_df.to_csv(outname, index=None)
