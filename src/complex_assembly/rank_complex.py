import argparse
import sys
import os
import numpy as np
import pandas as pd
from collections import Counter, defaultdict
import glob
import subprocess
import matplotlib.pyplot as plt
import pdb

parser = argparse.ArgumentParser(description = '''Analyse the relationship between the TM-score and pDockQ.''')

parser.add_argument('--pDockQ_df', nargs=1, type= str, default=sys.stdin, help = 'Path to pDockQ scores for complexes.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to outdir')


##############FUNCTIONS###############
def rank_models(pDockQ_df, outdir):
    '''Analyse the relationship between pdockq and tmscore
    '''

    av_pdockq = []
    sum_pdockq = []
    nchains = []
    ninterfaces = []
    models = pDockQ_df.scored_model.unique()

    for model in models:
        sel = pDockQ_df[pDockQ_df.scored_model==model]
        av_pdockq.append(sel.pDockQ.mean())
        sum_pdockq.append(np.sum(sel.pDockQ.values))
        nchains.append(np.unique(np.concatenate([sel.Chain1.values,sel.Chain2.values])).shape[0])
        ninterfaces.append(len(sel))


    #df
    complex_pdockq_df = pd.DataFrame()
    complex_pdockq_df['model']=[model.split('/')[-1] for model in models]
    complex_pdockq_df['ninterfaces']=ninterfaces
    complex_pdockq_df['nchains']=nchains
    complex_pdockq_df['av_pDockQ']=av_pdockq
    complex_pdockq_df['sum_pDockQ']=sum_pdockq

    complex_pdockq_df = complex_pdockq_df.sort_values(by='sum_pDockQ',ascending=False).reset_index()
    top_model = complex_pdockq_df.loc[0]
    print('Top pDockQ:',top_model.sum_pDockQ, 'for model',top_model.model)
    pdb.set_trace()
    #Plot relationships
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.scatter(av_pdockq,tmscores,s=1)
    plt.xlabel('pDockQ')
    plt.ylabel('TM-score')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'pDockQ_vs_TMscore.png',dpi=300)
    plt.close()



    pdb.set_trace()

################MAIN###############
#Parse args
args = parser.parse_args()
pDockQ_df = pd.read_csv(args.pDockQ_df[0])
outdir = args.outdir[0]

rank_models(pDockQ_df, outdir)
#Get best model

pdb.set_trace()
