import argparse
import sys
import os
import numpy as np
import pandas as pd
from collections import Counter, defaultdict
import glob
import subprocess
from scipy.spatial import distance
from circuit import hamilton, all_paths
import pdb

parser = argparse.ArgumentParser(description = '''Analyse overlaps in the assembled complexes.''')

parser.add_argument('--complexdir', nargs=1, type= str, default=sys.stdin, help = 'Path to data with assembled complexes.')


##############FUNCTIONS###############

def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''
    pdb_chains = {}
    pdb_coords = {}
    with open(pdbfile) as file:
        for line in file:
            record = parse_atm_record(line)
            if record['chain'] in [*pdb_chains.keys()]:
                pdb_chains[record['chain']].append(line)
            else:
                pdb_chains[record['chain']] = [line]


            if record['atm_name']=='CA':
                if record['chain'] in [*pdb_coords.keys()]:
                    pdb_coords[record['chain']].append([record['x'],record['y'],record['z']])
                else:
                    pdb_coords[record['chain']] = [[record['x'],record['y'],record['z']]]

    return pdb_chains, pdb_coords


def check_overlaps(pdb_coords):
    '''Check overlapping chains and remove if found
    '''
    chains = [*pdb_coords.keys()]

    keep_chains = []
    print('Checking overlaps...')
    for i in range(len(chains)-1):
        overlap_found = False
        coords_i = pdb_coords[chains[i]]
        l1 = len(coords_i) #Length of chain 1
        for j in range(i+1,len(chains)):
            coords_j = pdb_coords[chains[j]]
            #Check CA overlap
            #Calc 2-norm
            mat = np.append(coords_i, coords_j,axis=0)
            a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
            dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
            contact_dists = dists[l1:,:l1]
            overlap = np.argwhere(contact_dists<=5) #1 Å threshold

            if overlap.shape[0]>0.5*min(l1,len(coords_j)): #If over 50% overlap
                print('Overlap found',chains[i],chains[j])
                overlap_found = True
                break

        if overlap_found == True:
            continue
        else:
            keep_chains.append(chains[i])
    #Add the last chain
    keep_chains.append(chains[-1])
    return keep_chains

def write_pdb(pdb_chains, keep_chains, outname):
    '''Write the non-overlapping chains to a pdb file
    '''

    with open(outname, 'w') as file:
        for chain in keep_chains:
            for line in pdb_chains[chain]:
                file.write(line)


################MAIN###############
#Parse args
args = parser.parse_args()
#Data
complexdir = args.complexdir[0]

complexes = glob.glob(complexdir+'*.pdb')
for complex in complexes:
    pdb_chains, pdb_coords = read_pdb(complex)
    try:
        #Check overlaps
        keep_chains = check_overlaps(pdb_coords)
    except:
        pdb.set_trace()
    #Write the non-overlapping chains
    write_pdb(pdb_chains, keep_chains, complex)
