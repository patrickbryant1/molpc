#Run the assembly pipeline starting from predicted pairwise interactions
#########INPUTS and PATHS##########
BASE=/proj/berzelius-2021-29/users/x_patbr/results/molpc/all_pdb_over9/guided_trimer/ #Path to predicted pairs from AF-multimer
COMPLEXDIR=$BASE/assembly/$ID/complex/ #Where all the output for the complex assembly will be directed
CODEDIR=/proj/berzelius-2021-29/users/x_patbr/molpc/src/complex_assembly
#########OUTPUTS#########
#Ranked complexes

#########PIPELINE#########
#Make directories
mkdir -p $COMPLEXDIR

#Copy all predicted unique chain interactions to reflect all possible interactions
SUB_PDBDIR=$BASE
OUTDIR=$BASE/assembly/$ID/
USEQS=/proj/berzelius-2021-29/users/x_patbr/molpc/data/all_pdb_over9/PDB/$ID'_useqs.csv'
SUBSIZE=3 #Switch this to 2 for all pairs
INTERACTIONS=/proj/berzelius-2021-29/users/x_patbr/molpc/data/all_pdb_over9/PDB/$ID'_ints.csv' #Known interactions
INTCHAIN2SEQ=/proj/berzelius-2021-29/users/x_patbr/molpc/data/all_pdb_over9/PDB/$ID'_seqs.csv' #Chain in known interactions to sequence in meta
GET_ALL=0 #If to get all interactions anyway (1) or not (0)
python3 $CODEDIR/copy_preds.py --complex_id $ID --pdbdir $SUB_PDBDIR --outdir $OUTDIR \
--useqs $USEQS --subsize $SUBSIZE --interactions $INTERACTIONS --intchain2seq $INTCHAIN2SEQ --get_all $GET_ALL

#Rewrite AF predicted complexes to have proper numbering and chain labels
PDBDIR=$BASE/assembly/$ID/
OUTDIR=$BASE/assembly/$ID/
python3 $CODEDIR/rewrite_af_pdb.py --pdbdir $PDBDIR --pdb_id $ID --outdir $OUTDIR

#Write all pairs
PAIRDIR=$PDBDIR/pairs/
META=$PDBDIR/meta.csv #where to write all interactions
#It is necessary that the first unique chain is named A-..N for and the second N-... and so on
mkdir $PAIRDIR
#Glob for all files with each chain in order (A,B,C,D) A-->B,C,D; B--> C,D; C-->D
python3 $CODEDIR/write_all_pairs.py --pdbdir $PDBDIR --pairdir $PAIRDIR --meta $META \
--interactions $INTERACTIONS --get_all $GET_ALL

#Assemble from pairs
#Find all non-overlapping paths that connect all nodes and assemble these
NETWORK=$META
OUTDIR=$COMPLEXDIR
python3 $CODEDIR/find_paths_assemble.py --network $NETWORK \
--pairdir $PAIRDIR --outdir $OUTDIR


#Score all complexes
MODELDIR=$COMPLEXDIR
PLDDTDIR=$PDBDIR/plddt/
DT=8
OUTNAME=$COMPLEXDIR/complex_scores.csv
python3 $CODEDIR/score_entire_complex.py --modeldir $MODELDIR --plddtdir $PLDDTDIR \
--path_df $PATHDF --distance_threshold $DT --outname $OUTNAME
