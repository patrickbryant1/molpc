import argparse
import sys
import os
import numpy as np
import pandas as pd
from collections import defaultdict
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Get plDDT per chain.''')

parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to data.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS###############



################MAIN###############
#Parse args
args = parser.parse_args()
#Data
pdbdir = args.pdbdir[0]
outdir = args.outdir[0]

#Fetch the plDDT per chain
#Check if file is present
files = glob.glob(pdbdir+'*/result*.pkl')
for filename in files:
    subid = filename.split('/')[-2]
    #Load pkl file
    metrics = np.load(filename,allow_pickle=True)
    plDDT = metrics['plddt']
    #Save plDDT
    np.save(outdir+subid+'.npy',plDDT)
