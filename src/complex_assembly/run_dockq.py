import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from sklearn import metrics
from collections import defaultdict
from Bio import pairwise2
import subprocess
import pdb

parser = argparse.ArgumentParser(description = '''Score complexes.''')
parser.add_argument('--DockQ', nargs=1, type= str, default=sys.stdin, help = 'Path to DockQ program.')
parser.add_argument('--native', nargs=1, type= str, default=sys.stdin, help = 'Path to native structure.')
parser.add_argument('--modeldir', nargs=1, type= str, default=sys.stdin, help = 'Path to directory with models of all modeled complexes.')
parser.add_argument('--score_df', nargs=1, type= str, default=sys.stdin, help = 'Path to scored complexes.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'The name of the output csv with pDockQ')


################FUNCTIONS#################

def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''
    pdb_chains = {}
    pdb_seqs = {}
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    with open(pdbfile) as file:
        for line in file:
            if not line.startswith('ATOM'):
                continue

            record = parse_atm_record(line)
            if record['chain'] in [*pdb_chains.keys()]:
                pdb_chains[record['chain']].append(line)
                if record['atm_name']=='CA':
                    pdb_seqs[record['chain']] += three_to_one[record['res_name']]
            else:
                pdb_chains[record['chain']] = [line]
                pdb_seqs[record['chain']] = ''

    return pdb_chains, pdb_seqs

def rewrite_native(modeldir, model, native):
    '''Rewrite the native structure so that the chain names match
    those of the assembled complexes
    '''

    model_pdb, model_seqs = read_pdb(model)
    native_pdb, native_seqs = read_pdb(native)

    native_to_model_chain = {}
    #Find best match for each native seqs
    model_chains = [*model_seqs.keys()]
    for native_chain in native_seqs:
        top_score = 0
        for model_chain in model_chains:
            score = pairwise2.align.globalxx(native_seqs[native_chain],model_seqs[model_chain])[0][2]
            if score>top_score:
                best_match = model_chain
                top_score = score

        #print(native_seqs[native_chain])
        #print(model_seqs[best_match])
        #Update chains
        model_chains = np.setdiff1d(model_chains, best_match)
        #Save match
        native_to_model_chain[native_chain]=best_match

    #Write the native pdb with the new chain names
    with open(modeldir+native.split('/')[-1],'w') as file:
        for chain in native_to_model_chain:
            chain_pdb = native_pdb[chain]
            for line in chain_pdb:
                file.write(line[:21]+native_to_model_chain[chain]+line[22:])

    return modeldir+native.split('/')[-1], [*model_seqs.keys()]

def run_dockq(DockQ, model, native, chains):
    '''
    Run multi-interface DockQ
    #./DockQ.py examples/1A2K_r_l_b.model.pdb examples/1A2K_r_l_b.pdb -native_chain1 A B -perm1
    https://github.com/bjornwallner/DockQ

    !!!!!!Make sure the chain names are the same in the model and the native structure!!!!!!!!!!!!
    '''

    chain_scores = {'Chain':[], 'DockQ':[]}
    for chain in chains:
        # DockQ model native -model_chain1 chain -perm2
        dockq_out = subprocess.check_output([DockQ, model , native, '-model_chain1', chain, '-perm2', '-no_needle']) #Performs optimal structural alignment
        for line in dockq_out.decode().split('\n'):
            if 'Best score' in line:
                chain_scores['Chain'].append(chain)
                chain_scores['DockQ'].append(float(line.split(')')[0].split('(')[1]))
                break
    #df
    score_df = pd.DataFrame.from_dict(chain_scores)
    score_df['PDB'] = native.split('/')[-1].split('.')[0]
    return score_df

#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
DockQ = args.DockQ[0]
native = args.native[0]
modeldir = args.modeldir[0]
score_df = pd.read_csv(args.score_df[0])
outname = args.outname[0]

#Get the top scoring complex
top_complex = score_df.loc[0].Model
#Rewrite the native file to match the complex chain naming
new_native, chains = rewrite_native(modeldir, modeldir+'complex'+str(int(top_complex))+'.pdb', native)
#Run DockQ
score_df = run_dockq(DockQ, modeldir+'complex'+str(int(top_complex))+'.pdb', new_native, chains)
score_df.to_csv(outname, index = None)
