import argparse
import sys
import os
import numpy as np
import pandas as pd
from collections import Counter, defaultdict
import glob
import subprocess
import copy
from Bio.SVDSuperimposer import SVDSuperimposer
import pdb

parser = argparse.ArgumentParser(description = '''Perform structural alignment in sequential order.''')

parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb ids and interacting chains.')
parser.add_argument('--path_df', nargs=1, type= str, default=sys.stdin, help = 'csv containing assembly paths to explore.')
parser.add_argument('--pairdir', nargs=1, type= str, default=sys.stdin, help = 'Path to data with complexes.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')


##############FUNCTIONS###############

def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''
    pdb_chains = {}
    chain_coords = {}
    with open(pdbfile) as file:
        for line in file:
            record = parse_atm_record(line)
            if record['chain'] in [*pdb_chains.keys()]:
                pdb_chains[record['chain']].append(line)
                chain_coords[record['chain']].append([record['x'],record['y'],record['z']])
            else:
                pdb_chains[record['chain']] = [line]
                chain_coords[record['chain']]= [[record['x'],record['y'],record['z']]]


    return pdb_chains, chain_coords

def intialize_complex(row_i, row_j, pairdir):
    '''Add the inital links
    '''

    #Get overlap chain
    oc = np.intersect1d([row_i.Chain1,row_i.Chain2],[row_j.Chain1,row_j.Chain2])[0]

    #Read pdb from rows i and j
    #Get the right order of chains
    if os.path.exists(pairdir+row_i.Source+'_'+row_i.Chain1+'-'+row_i.Source+'_'+row_i.Chain2+'.pdb'):
        pdbi = pairdir+row_i.Source+'_'+row_i.Chain1+'-'+row_i.Source+'_'+row_i.Chain2+'.pdb'
    else:
        pdbi = pairdir+row_i.Source+'_'+row_i.Chain2+'-'+row_i.Source+'_'+row_i.Chain1+'.pdb'

    if os.path.exists(pairdir+row_j.Source+'_'+row_j.Chain1+'-'+row_j.Source+'_'+row_j.Chain2+'.pdb'):
        pdbj = pairdir+row_j.Source+'_'+row_j.Chain1+'-'+row_j.Source+'_'+row_j.Chain2+'.pdb'
    else:
        pdbj = pairdir+row_j.Source+'_'+row_j.Chain2+'-'+row_j.Source+'_'+row_j.Chain1+'.pdb'

    pdbi_chains, pdbi_coords = read_pdb(pdbi)
    pdbj_chains, pdbj_coords = read_pdb(pdbj)

    #Set the coordinates to be superimposed.
    #coords will be put on top of reference_coords.
    reference_coords = np.array(pdbi_coords[oc])
    coords =  np.array(pdbj_coords[oc])
    sup = SVDSuperimposer()
    sup.set(reference_coords, coords)
    sup.run()
    rot, tran = sup.get_rotran()
    #Rotate coords on reference_coords (coords from new chain to its new relative position/orientation)
    #This is done for the missing chain from pdbj
    new_chain = np.setdiff1d([*pdbj_chains.keys()],oc)[0]
    new_chain_coords = np.dot(np.array(pdbj_coords[new_chain]), rot) + tran

    #Create a dict to keep track of all coords and pdb contents
    complex_coords = {}
    complex_pdb = {}
    #Add pdbi
    for chain in pdbi_coords:
        complex_coords[chain]=np.array(pdbi_coords[chain])
        complex_pdb[chain]=pdbi_chains[chain]
    #Add the new chain
    complex_coords[new_chain]=new_chain_coords
    complex_pdb[new_chain]=pdbj_chains[new_chain]

    return complex_coords, complex_pdb


def write_pdb(complex_coords, complex_pdb, pdbfile):
    '''Write all chains into one single pdb file
    Update the coords to the rotated ones
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    '''
    with open(pdbfile, 'w') as file:
        for chain in complex_pdb:
            chain_pdb = complex_pdb[chain]
            chain_coords = complex_coords[chain]
            for i in range(len(chain_pdb)):
                line = chain_pdb[i]
                coord = chain_coords[i]

                #Get coords in str and calc blanks
                x,y,z =  format(coord[0],'.3f'), format(coord[1],'.3f'), format(coord[2],'.3f')
                x_blank = ' '*(8-len(x))
                y_blank = ' '*(8-len(y))
                z_blank = ' '*(8-len(z))
                try:
                    outline = line[:30]+x_blank+x+y_blank+y+z_blank+z+line[54:]
                    file.write(outline)
                except:
                    pdb.set_trace()






################MAIN###############
#Parse args
args = parser.parse_args()
#Data
meta = pd.read_csv(args.meta[0])
path_df = pd.read_csv(args.path_df[0])
pairdir = args.pairdir[0]
outdir = args.outdir[0]

#Check that there are paths
if len(path_df)<1:
    print('No paths. Try running higher-order complexes.')
    sys.exit()

#Create all complexes in path df
for path in path_df.Path.unique():
    print('Assembling complex',path,'out of', path_df.Path.unique().shape[0])
    circuit = path_df[path_df.Path==path]
    circuit = circuit.reset_index()
    if len(circuit)<2:
        print('No path...')
        continue
    ########################################################
    #Store the chains present in the current complex
    #Initalize complex
    row_i = circuit.loc[0]
    row_j = circuit.loc[1]
    complex_coords, complex_pdb = intialize_complex(row_i, row_j, pairdir)

    #Go through the pre-determined circuit and add chains
    for i in range(2,len(circuit)):
        row_i = circuit.loc[i]
        #Get overlap chain
        oc = np.intersect1d([row_i.Chain1,row_i.Chain2],[*complex_coords.keys()])[0]
        #Save the new chain
        new_chain = np.setdiff1d([row_i.Chain1,row_i.Chain2],oc)[0]
        #print('Adding interaction',i+1,'out of',len(circuit))
        #Read pdb from rowi
        #Get the right order of chains
        if os.path.exists(pairdir+row_i.Source+'_'+row_i.Chain1+'-'+row_i.Source+'_'+row_i.Chain2+'.pdb'):
            pdbi = pairdir+row_i.Source+'_'+row_i.Chain1+'-'+row_i.Source+'_'+row_i.Chain2+'.pdb'
        else:
            pdbi = pairdir+row_i.Source+'_'+row_i.Chain2+'-'+row_i.Source+'_'+row_i.Chain1+'.pdb'

        pdbi_chains, pdbi_coords = read_pdb(pdbi)
        #Set the coordinates to be superimposed.
        #coords will be put on top of reference_coords.
        reference_coords = complex_coords[oc]
        coords =  np.array(pdbi_coords[oc])
        sup = SVDSuperimposer()
        sup.set(reference_coords, coords)
        sup.run()
        rot, tran = sup.get_rotran()
        #Rotate coords on reference_coords (coords from new chain to its new relative position/orientation)
        #This is done for the missing chain from pdbi
        new_chain_coords = np.dot(np.array(pdbi_coords[new_chain]), rot) + tran
        #Save the new chain to the complex
        complex_coords[new_chain]=new_chain_coords
        complex_pdb[new_chain]=pdbi_chains[new_chain]

    #Write the complex
    write_pdb(complex_coords, complex_pdb, outdir+'complex'+str(path)+'.pdb')
