import argparse
import sys
import os
import numpy as np
import pandas as pd
from collections import Counter, defaultdict
import glob
import subprocess
import pdb

parser = argparse.ArgumentParser(description = '''Perform structural alignment towards the native structure.''')

parser.add_argument('--model_id', nargs=1, type= str, default=sys.stdin, help = 'Model id.')
parser.add_argument('--model', nargs=1, type= str, default=sys.stdin, help = 'Path to the top modelled complex.')
parser.add_argument('--native', nargs=1, type= str, default=sys.stdin, help = 'Path to native complex')
parser.add_argument('--MMalign', nargs=1, type= str, default=sys.stdin, help = 'Path to MMalign executable')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Path to output csv')


##############FUNCTIONS###############
def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''
    pdb_chains = []

    with open(pdbfile) as file:
        for line in file:
            if not line.startswith('ATOM'):
                continue
            record = parse_atm_record(line)
            if record['chain'] in pdb_chains:
                continue
            else:
                pdb_chains.append(record['chain'])

    return pdb_chains

def check_complex(complex):
    '''Check that the complex does indeed contain the number of required chains
    '''

    chains = read_pdb(complex)

    return len(chains)

def run_mmalign(MMalign, pdb1, pdb2):
    '''Align pdb1 with 2 and fetch the TMscore and RMSD
    '''

    #Performs optimal structural alignment
    output = subprocess.check_output([MMalign, pdb1, pdb2])
    #Parse TM-score
    output = output.decode("utf-8").split('\n')
    for line in output:
        #Structure 1 is the native --> use TM-score normalized by that
        if 'TM-score' in line and 'normalized by length of Structure_1' in line:
            tmscore = float(line.split(' ')[1])

        if 'RMSD=' in line:
            rmsd = float(line.split(',')[1].split(' ')[-1])

    return tmscore, rmsd


################MAIN###############
#Parse args
args = parser.parse_args()
model_id = args.model_id[0]
model = args.model[0]
native = args.native[0]
MMalign = args.MMalign[0]
outname = args.outname[0]


#Check chains
model_chains = read_pdb(model)
native_chains = read_pdb(native)
#Run MMalign
#If chains are missing - see how big the subcomponent is
if len(model_chains)<len(native_chains):
    tmscore, rmsd = run_mmalign(MMalign, model, native)
else:
    tmscore, rmsd = run_mmalign(MMalign, native, model)

#Save
score_df = pd.DataFrame()
score_df['ID'] = [model_id]
score_df['nchains_in_model'] = len(model_chains)
score_df['nchains_in_native'] = len(native_chains)
score_df['TMscore'] = tmscore
score_df['RMSD'] = rmsd 
#Save df
score_df.to_csv(outname)
