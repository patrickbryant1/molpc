import numpy as np
import pandas as pd
# import networkx as nx
# import matplotlib.pyplot as plt
# G=nx.Graph()
# G.add_nodes_from(edges)
# G = nx.from_numpy_array(edges)
# df = pd.DataFrame(edges)
# G = nx.from_pandas_edgelist(df,0,1)
# nx.draw(G, with_labels = True)
# plt.show()
import pdb

def hamilton(edges):
    '''Find a path that visits all nodes once without repeats
    By aquiring all paths that visit all nodes of length n (the number of nodes),
    this requirement is satisfied.
    Note that this version does not handle self-loops (which should not be present in the input)
    https://www.educative.io/edpresso/how-to-implement-depth-first-search-in-python
    '''


    nodes = np.unique(edges)
    num_nodes = len(nodes)


    def dfs(visited, edges, node, level,depth):
        if node not in visited:
            #print(node, level)
            depth.append(level)
            visited.append(node) #add note to visited
            level+=1
            #get all neighbors
            neighbors = edges[np.argwhere(edges[:,0]==node)[:,0]][:,1]
            #search all neighbors
            for neighbour in neighbors:
                dfs(visited, edges, neighbour, level,depth)

    #Go through all nodes as a starting point and find
    #all connections to that node
    for node in nodes:
        visited = []
        level = 1
        depth = []
        dfs(visited, edges, node, level,depth)
        #Check if Hamiltonian circuit
        #print(visited,depth)
        if depth[-1]==num_nodes and len(visited)==num_nodes:
            break


    #Construct circuit edges
    circuit_n1 = []
    circuit_n2 = []
    for i in range(1,len(visited)):
        #Both forward and reverse order
        circuit_n1.append(visited[i-1])
        circuit_n2.append(visited[i])
        circuit_n2.append(visited[i-1])
        circuit_n1.append(visited[i])

    circuit_df = pd.DataFrame()
    circuit_df['Chain1'] = circuit_n1
    circuit_df['Chain2'] = circuit_n2

    return circuit_df


def all_paths(edges):
    '''Find all paths that visits all nodes
    https://www.educative.io/edpresso/how-to-implement-depth-first-search-in-python
    '''


    nodes = np.unique(edges)
    num_nodes = len(nodes)


    def bfs(graph, node, path):
        queue.append(node)

        while queue:
            s = queue.pop(0)

            print (s, end = " ")
            print(np.unique(path).shape[0])
            if np.unique(path).shape[0]==num_nodes:
                break
            for neighbour in graph[s]:
                if neighbour not in path:
                    path.append(s)
                    path.append(neighbour)
                    queue.append(neighbour)
                    



    #Go through all nodes as a starting point and find
    #all connections to that node
    graph = {}
    for node in nodes:
        #get all neighbors
        graph[node] = edges[np.argwhere(edges[:,0]==node)[:,0]][:,1]

    #Go through all nodes and traverse until all nodes are included
    paths = {}
    lens = {}
    for node in graph:
        queue = []
        path = []
        bfs(graph,node,path)
        paths[node] = path
        lens[node] = np.unique(path).shape[0]

    #Construct circuit edges
    circuit_n1 = []
    circuit_n2 = []
    path_ind = []
    pi=0
    for key in paths:
        path = paths[key]
        pi+=1
        for i in range(0,len(path)-1,2):
            #Both forward and reverse order for later merge
            circuit_n1.append(path[i])
            circuit_n2.append(path[i+1])
            circuit_n2.append(path[i])
            circuit_n1.append(path[i+1])
            path_ind.append(pi)
            path_ind.append(pi)


    circuit_df = pd.DataFrame()
    circuit_df['Chain1'] = circuit_n1
    circuit_df['Chain2'] = circuit_n2
    circuit_df['Path'] = path_ind


    return circuit_df
