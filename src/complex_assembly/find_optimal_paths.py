import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import itertools
import math
import copy
import pdb

parser = argparse.ArgumentParser(description = '''Find optimal paths.''')
parser.add_argument('--network', nargs=1, type= str, default=sys.stdin, help = 'Path to csv containing pairwise interactions.')
parser.add_argument('--overlap_pair', nargs=1, type= str, default=sys.stdin, help = 'Path to csv containing all overlapping pairs')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'The name of the output csv with all scores')



##############FUNCTIONS##############

def find_paths(edges, sources, overlap_pair):
    '''Find all paths that visits all nodes fulfilling the criteria:
    1. No overlapping interfaces according to "overlaps"
    '''

    #Get all nodes
    nodes = np.unique(edges)
    num_nodes = len(nodes)
    #Save all paths
    node_paths = []
    source_paths = []
    edge_paths = []
    #Any path connecting all nodes will contain n-1 edges
    #Initialise paths starting at A - these will be all paths that contain A
    sps = edges[np.argwhere(edges=='A')[:,0]]
    ssr = sources[np.argwhere(edges=='A')[:,0]]
    for i in range(len(sps)):
        node_paths.append(['A'])
        node_paths[i].append(np.setdiff1d(sps[i],'A')[0])
        source_paths.append([ssr[i]])
        edge_paths.append(['A-'+np.setdiff1d(sps[i],'A')[0]])
    #Add nodes that are not present in the path already conditioned on having no overlapping interfaces
    complete_paths = [] #Finished paths containing all nodes
    complete_sources = []
    itercount = 0
    while len(node_paths)>0:
        itercount+=1
        cpath_nodes = node_paths.pop(0) #Current nodes in path
        cpath_sources = source_paths.pop(0) #current sources in path
        cpath_edges = edge_paths.pop(0)
        #Create df to merge
        source_edge_df = pd.DataFrame()
        source_edge_df['Source']=cpath_sources
        source_edge_df['Dimer']=cpath_edges
        #Check completeness
        if np.unique(cpath_nodes).shape[0]==num_nodes:
            complete_paths.append(cpath_nodes)
            complete_sources.append(cpath_sources)
            continue
        #Get the current node
        cn = cpath_nodes[-1]
        #Get all edges to the current node
        cedges = edges[np.argwhere(edges==cn)[:,0]]
        csources = sources[np.argwhere(edges==cn)[:,0]]
        #Get all of these that are new
        new_nodes = np.setdiff1d(cedges[cedges!=cn], cpath_nodes)
        for new_node in new_nodes:
            new_edges = cedges[np.argwhere(cedges==new_node)[:,0]]
            new_sources = csources[np.argwhere(cedges==new_node)[:,0]]

            #Check overlaps
            for i in range(len(new_edges)):
                new_edge = '-'.join(new_edges[i])
                new_source = new_sources[i]
                #Fwd
                edge_overlaps = pd.merge(overlap_pair, source_edge_df, left_on=['Dimer1', 'Source1'], right_on=['Source', 'Dimer'], how='inner')
                if len(edge_overlaps)>0:
                    continue
                #Rv
                edge_overlaps = pd.merge(overlap_pair, source_edge_df, left_on=['Dimer2', 'Source2'], right_on=['Source', 'Dimer'], how='inner')
                if len(edge_overlaps)>0:
                    continue

                #Save the new edge - if no overlaps
                npath_nodes = copy.deepcopy(cpath_nodes)
                npath_sources = copy.deepcopy(cpath_sources)
                npath_edges = copy.deepcopy(cpath_edges)
                npath_nodes.append(new_node)
                npath_sources.append(new_source)
                npath_edges.append(new_edge)
                #Save paths
                node_paths.append(npath_nodes)
                source_paths.append(npath_sources)
                edge_paths.append(npath_edges)


        if len(node_paths)>100000:
            node_paths = node_paths[-100000:]
            source_paths = source_paths[-100000:]
            edge_paths = edge_paths[-100000:]

        if itercount%1000==0:
            print(itercount, len(cpath_nodes), len(node_paths))


    #Create a df of all paths
    path_df = {'Chain1':[], 'Chain2':[], 'Source':[],'Path':[]}
    pn=1
    for i in range(len(complete_paths)):
        path = complete_paths[i]
        source = complete_sources[i]
        for i in range(len(path)-1):
            path_df['Chain1'].append(path[i])
            path_df['Chain2'].append(path[i+1])
            path_df['Source'].append(source[i])
            path_df['Path'].append(pn)
        pn+=1
    path_df = pd.DataFrame.from_dict(path_df)

    return path_df


#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
network = pd.read_csv(args.network[0])
overlap_pair = pd.read_csv(args.overlap_pair[0])
outname = args.outname[0]

#Get all edges
edges = np.array(network[['Chain1', 'Chain2']])
sources = np.array(network['Source'])
#Find paths
path_df = find_paths(edges, sources, overlap_pair)
if path_df.Path.unique().shape[0]>0:
    #Save
    path_df.to_csv(outname, index=None)
print('Created', path_df.Path.unique().shape[0], 'possible non-overlapping paths')
