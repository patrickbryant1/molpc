import argparse
import sys
import os
import numpy as np
import pandas as pd
from collections import defaultdict
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Extract the sequences and coordinates from individual chains from a pdb file.''')

parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to data.')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb ids and interacting chains.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')


def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[16]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record


def read_all_chain_seqs_seqres(pdbname):
    '''Read the sequence of all chains in a pdb file
    '''
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    with open(pdbname) as pdbfile:
        chain_sequences = {} #Sequence
        prev_res_no=''
        for line in pdbfile:
            if not line.startswith('SEQRES'):
                continue
            else:
                chain = line[11]
                residues = line[19:70].split()
                for item in residues:
                    if item not in [*three_to_one.keys()]:
                        continue
                    if chain in [*chain_sequences.keys()]:
                        chain_sequences[chain]+=three_to_one[item]
                    else:
                        chain_sequences[chain]=three_to_one[item]

    return chain_sequences

def read_all_chain_seqs_str(pdbname):
    '''Read the sequence of all chains in a pdb file
    '''
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    with open(pdbname) as pdbfile:
        chain_sequences = {} #Sequence
        prev_res_no=''
        for line in pdbfile:
            if not line.startswith('ATOM'):
                continue
            else:
                record = parse_atm_record(line)
                if record['atm_name']!='CA' or record['res_no']==prev_res_no:
                    continue
                if record['chain'] in [*chain_sequences.keys()]:
                    chain_sequences[record['chain']]+=three_to_one[record['res_name']]
                else:
                    chain_sequences[record['chain']]=three_to_one[record['res_name']]

                prev_res_no = record['res_no']

    return chain_sequences


def write_fasta(seq1, seq2, pdbid, ch1, ch2, outdir):
    '''Write the fasta sequences individually and in dimer configuration
    '''

    #Write each individual chain
    with open(outdir+pdbid+'_'+ch1+'.fasta','w') as file:
        file.write('>'+pdbid+'_'+ch1+'\n')
        file.write(seq1)

    with open(outdir+pdbid+'_'+ch2+'.fasta','w') as file:
        file.write('>'+pdbid+'_'+ch2+'\n')
        file.write(seq2)

    #Both together
    with open(outdir+'dimer/'+pdbid+'_'+ch1+'-'+pdbid+'_'+ch2+'.fasta','w') as file:
        file.write('>'+pdbid+'_'+ch1+'-'+pdbid+'_'+ch2+'\n')
        file.write(seq1+seq2)



##################MAIN#######################

#Parse args
args = parser.parse_args()
#Data
pdbdir = args.pdbdir[0]
meta = pd.read_csv(args.meta[0])
outdir = args.outdir[0]

seq1 = []
seq2 = []
l1 = []
l2 = []
for i in range(len(meta)):
    row = meta.loc[i]
    pdbid = row.PDB
    #Get SEQRES
    chain_sequences_seqres = read_all_chain_seqs_seqres(pdbdir+pdbid+'.pdb')
    #Get Structure seq
    chain_sequences_str = read_all_chain_seqs_str(pdbdir+pdbid+'.pdb')
    #Chains
    ch1, ch2 = row.Chain1, row.Chain2
    #Compare lengths of seqres and structure sequences
    if len([*chain_sequences_seqres.keys()])>1:
        if len(chain_sequences_seqres[ch1])>1.2*len(chain_sequences_str[ch1]):
            chain_seq1 = chain_sequences_str[ch1]
            print(ch1,len(chain_sequences_seqres[ch1]),len(chain_sequences_str[ch1]))
        else:
            chain_seq1 = chain_sequences_seqres[ch1]

        if len(chain_sequences_seqres[ch2])>1.2*len(chain_sequences_str[ch2]):
            chain_seq2 = chain_sequences_str[ch2]
            print(ch2,len(chain_sequences_seqres[ch2]),len(chain_sequences_str[ch2]))
        else:
            chain_seq2 = chain_sequences_seqres[ch2]
    else:
        chain_seq1 = chain_sequences_str[ch1]
        chain_seq2 = chain_sequences_str[ch2]

    #Write seqs
    write_fasta(chain_seq1, chain_seq2, pdbid, ch1, ch2, outdir)

    #Save seqs and lens
    seq1.append(chain_seq1)
    seq2.append(chain_seq2)
    l1.append(len(chain_seq1))
    l2.append(len(chain_seq2))

#Add to df
meta['Seq1']=seq1
meta['Seq2']=seq2
meta['l1']=l1
meta['l2']=l2
meta['Id1']=meta.PDB+'_'+meta.Chain1
meta['Id2']=meta.PDB+'_'+meta.Chain2
#Save
meta.to_csv(args.meta[0],index=None)
