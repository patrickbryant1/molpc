# This directory contains all scripts and information necessary to run the complex assembly from predicted pairwise interactions.

The script **assemble.sh** handles the entire assembly process
given that all pairs in a given complex are predicted with
AlphaFold-multimer. The structural alignment - key to the
sequential addition of protein chains - is done with MMalign.
