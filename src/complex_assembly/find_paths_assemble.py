import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import itertools
import math
import copy
from collections import Counter, defaultdict
from Bio.SVDSuperimposer import SVDSuperimposer
import pdb

parser = argparse.ArgumentParser(description = '''Find optimal paths.''')
parser.add_argument('--network', nargs=1, type= str, default=sys.stdin, help = 'Path to csv containing pairwise interactions.')
parser.add_argument('--pairdir', nargs=1, type= str, default=sys.stdin, help = 'Path to dir containing all overlapping pairs')
parser.add_argument('--max_paths', nargs=1, type= int, default=sys.stdin, help = 'Maximal number of intermediate paths')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Where to write all complexes')



##############FUNCTIONS##############

def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''
    pdb_chains = {}
    chain_coords = {}
    chain_CA_inds = {}

    with open(pdbfile) as file:
        for line in file:
            record = parse_atm_record(line)
            if record['chain'] in [*pdb_chains.keys()]:
                pdb_chains[record['chain']].append(line)
                chain_coords[record['chain']].append([record['x'],record['y'],record['z']])
                ca_ind+=1
                if record['atm_name']=='CA':
                    chain_CA_inds[record['chain']].append(ca_ind)


            else:
                pdb_chains[record['chain']] = [line]
                chain_coords[record['chain']]= [[record['x'],record['y'],record['z']]]
                chain_CA_inds[record['chain']]= []
                #Reset CA ind
                ca_ind = 0


    return pdb_chains, chain_coords, chain_CA_inds

def check_overlaps(cpath_coords,cpath_CA_inds,n_coords,n_CA_inds):
    '''Check assembly overlaps of new chain
    '''
    #Check CA overlap
    n_CAs = n_coords[n_CA_inds] #New chain CAs
    l1 = (len(n_CAs))
    #Go through all previous CAs and compare
    for i in range(len(cpath_coords)):
        p_CAs = cpath_coords[i][cpath_CA_inds[i]]
        #Calc 2-norm
        mat = np.append(n_CAs, p_CAs,axis=0)
        a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
        dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
        contact_dists = dists[l1:,:l1]
        overlap = np.argwhere(contact_dists<=5) #5 Å threshold
        if overlap.shape[0]>0.5*min(l1,len(p_CAs)): #If over 50% overlap
            return True

    return False

def find_paths(edges, sources, pairdir, max_paths, outdir):
    '''Find all paths that visits all nodes fulfilling the criteria:
    No overlapping chains (50% of shortest chain's CAs within 5 Å btw two chains)
    '''

    #Get all nodes
    nodes = np.unique(edges)
    num_nodes = len(nodes)
    #Save the unique subpaths to not keep repeats in RAM
    subpath_coords = {}
    subpath_CA_inds = {}
    #Save all paths, sources, pdbs and coords
    node_paths = []
    source_paths = []

    #Any path connecting all nodes will contain n-1 edges
    #Initialise paths starting at A - these will be all paths that contain A
    #(which has to be present if a complete path is to be found)
    sps = edges[np.argwhere(edges=='A')[:,0]]
    ssr = sources[np.argwhere(edges=='A')[:,0]]
    for i in range(len(sps)):
        node_paths.append([*sps[i]])
        source_paths.append([ssr[i],ssr[i]])
        #Read source
        pdb_chains, chain_coords, chain_CA_inds = read_pdb(pairdir+ssr[i]+'_'+sps[i][0]+'-'+ssr[i]+'_'+sps[i][1]+'.pdb')
        #Add to path
        coord_path = []
        CA_ind_path = []
        for chain in sps[i]:
            coord_path.append(np.array(chain_coords[chain]))
            CA_ind_path.append(np.array(chain_CA_inds[chain]))

        #Add to subpath
        subpath_coords[source_paths[-1][0]+'_'+node_paths[-1][0]+'-'+source_paths[-1][1]+'_'+node_paths[-1][1]] = coord_path
        subpath_CA_inds[source_paths[-1][0]+'_'+node_paths[-1][0]+'-'+source_paths[-1][1]+'_'+node_paths[-1][1]] = CA_ind_path

    #Add nodes that are not present in the path already conditioned on having no overlapping interfaces
    complete_paths = [] #Finished paths containing all nodes
    complete_sources = [] #Sources containing all nodes
    complete_coords = [] #Coords for all nodes
    itercount = 0 #Keep track of iterations
    while len(node_paths)>0:
        itercount+=1
        cpath_nodes = node_paths.pop(0) #Current nodes in path
        cpath_sources = source_paths.pop(0) #current sources in path

        #Get the path id
        path_id = ''
        for pi in range(len(cpath_nodes)):
            path_id+=cpath_sources[pi]+'_'+cpath_nodes[pi]+'-'
        path_id = path_id[:-1]

        cpath_coords = [] #current coords in path
        cpath_CA_inds = [] #current CA coords in path
        #Fetch all coords from the subpaths of path_id
        fetch_queue = [path_id]
        while len(fetch_queue)>0:
            fid = fetch_queue.pop(0)
            fetch_coords = subpath_coords[fid]
            fetch_CA_inds = subpath_CA_inds[fid]
            #Append to cpath_coords if arr otherwise to fetch queue
            for fi in range(len(fetch_coords)):
                if type(fetch_coords[fi])==str:
                    fetch_queue.append(fetch_coords[fi])
                else:
                    cpath_coords.append(fetch_coords[fi])
                    cpath_CA_inds.append(fetch_CA_inds[fi])

        #Rearrange the order
        if len(cpath_coords)>2:
            cpath_coords = cpath_coords[::-1]
            cpath_CA_inds = cpath_CA_inds[::-1]
        #Check completeness
        if np.unique(cpath_nodes).shape[0]==num_nodes:
            #Save if complete and continue
            complete_paths.append(cpath_nodes)
            complete_sources.append(cpath_sources)
            complete_coords.append(cpath_coords)
            continue

        #Get the current node - this only allows for additions to the current node?
        cn = cpath_nodes[-1]

        #Get all edges to the current node
        cedges = edges[np.argwhere(edges==cn)[:,0]]
        csources = sources[np.argwhere(edges==cn)[:,0]]
        #Get all of these that are new
        new_nodes = np.setdiff1d(cedges[cedges!=cn], cpath_nodes)
        for new_node in new_nodes:
            #Get all sources harboring the new node
            new_sources = csources[np.argwhere(cedges==new_node)[:,0]]
            for i in range(len(new_sources)):
                new_source = new_sources[i]
                #Get the pair
                if os.path.exists(pairdir+new_source+'_'+cn+'-'+new_source+'_'+new_node+'.pdb'):
                    pdb_chains, chain_coords, chain_CA_inds = read_pdb(pairdir+new_source+'_'+cn+'-'+new_source+'_'+new_node+'.pdb')
                else:
                    pdb_chains, chain_coords, chain_CA_inds = read_pdb(pairdir+new_source+'_'+new_node+'-'+new_source+'_'+cn+'.pdb')

                #Align the overlapping chains
                #Set the coordinates to be superimposed.
                #coords will be put on top of reference_coords.
                sup = SVDSuperimposer()
                sup.set(cpath_coords[-1],np.array(chain_coords[cn])) #(reference_coords, coords)
                sup.run()
                rot, tran = sup.get_rotran()
                #Rotate coords from new node to its new relative position/orientation
                n_coords = np.dot(np.array(chain_coords[new_node]), rot) + tran

                #Check overlaps
                overlap = check_overlaps(cpath_coords,cpath_CA_inds,n_coords,chain_CA_inds[new_node])
                #Continue if overlap
                if overlap==True:
                    continue

                #Otherwise save
                #Save the new edge - if no overlaps
                npath_nodes = copy.deepcopy(cpath_nodes)
                npath_sources = copy.deepcopy(cpath_sources)

                #Append
                npath_nodes.append(new_node)
                npath_sources.append(new_source)
                #Save paths
                node_paths.append(npath_nodes)
                source_paths.append(npath_sources)
                #Save subpath
                #Add to subpath
                subpath_coords[path_id+'-'+new_source+'_'+new_node] = [path_id,n_coords]
                subpath_CA_inds[path_id+'-'+new_source+'_'+new_node] = [path_id,chain_CA_inds[new_node]]


        if len(node_paths)>max_paths:
            node_paths = node_paths[-max_paths:]
            source_paths = source_paths[-max_paths:]

       #print(itercount,len(node_paths),len(node_paths[-1]))

        #Check if node paths are out
        if len(node_paths)==0 and len(complete_paths)==0:
            print('No complete paths. Longest path:',len(cpath_nodes))
            complete_paths.append(cpath_nodes)
            complete_sources.append(cpath_sources)
            complete_coords.append(cpath_coords)

    return complete_paths, complete_sources, complete_coords

def write_pdb(complete_paths, complete_sources, complete_coords, pairdir, outdir):
    '''Write all chains into one single pdb file
    Update the coords to the roto-translated ones
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    '''

    for pi in range(len(complete_paths)):
        path = complete_paths[pi]
        sources = complete_sources[pi]
        coords = complete_coords[pi]
        #The first two chains are in reverse order
        new_coords = []
        new_coords.extend(coords[:2][::-1])
        new_coords.extend(coords[2:])
        coords = new_coords

        #Open a file to write to
        with open(outdir+'complex'+str(pi+1)+'.pdb', 'w') as file:
            for ci in range(1,len(path)):
                chain = path[ci]
                chain_coords = coords[ci]
                chain_source = sources[ci]
                #Read source
                if os.path.exists(pairdir+chain_source+'_'+chain+'-'+chain_source+'_'+path[ci-1]+'.pdb'):
                    pdb_chains, _, _ = read_pdb(pairdir+chain_source+'_'+chain+'-'+chain_source+'_'+path[ci-1]+'.pdb')
                else:
                    pdb_chains, _, _ = read_pdb(pairdir+chain_source+'_'+path[ci-1]+'-'+chain_source+'_'+chain+'.pdb')

                #If ci is 1, write the first chain also
                if ci==1:
                    first_chain = pdb_chains[path[ci-1]]
                    first_chain_coords = coords[ci-1]
                    for i in range(len(first_chain)):
                        line = first_chain[i]
                        coord = first_chain_coords[i]
                        #Get coords in str and calc blanks
                        x,y,z =  format(coord[0],'.3f'), format(coord[1],'.3f'), format(coord[2],'.3f')
                        x_blank = ' '*(8-len(x))
                        y_blank = ' '*(8-len(y))
                        z_blank = ' '*(8-len(z))
                        outline = line[:30]+x_blank+x+y_blank+y+z_blank+z+line[54:]
                        file.write(outline)

                chain_pdb = pdb_chains[chain]
                for i in range(len(chain_pdb)):
                    line = chain_pdb[i]
                    coord = chain_coords[i]
                    #Get coords in str and calc blanks
                    x,y,z =  format(coord[0],'.3f'), format(coord[1],'.3f'), format(coord[2],'.3f')
                    x_blank = ' '*(8-len(x))
                    y_blank = ' '*(8-len(y))
                    z_blank = ' '*(8-len(z))
                    outline = line[:30]+x_blank+x+y_blank+y+z_blank+z+line[54:]
                    file.write(outline)




def create_path_df(complete_paths, complete_sources, outdir):
    '''Create df of all complete paths
    '''
    #Create a df of all paths
    path_df = {'Chain1':[], 'Chain2':[], 'Source':[],'Path':[]}
    pn=1
    for i in range(len(complete_paths)):
        path = complete_paths[i]
        source = complete_sources[i]
        for i in range(len(path)-1):
            path_df['Chain1'].append(path[i])
            path_df['Chain2'].append(path[i+1])
            path_df['Source'].append(source[i+1])
            path_df['Path'].append(pn)
        pn+=1
    path_df = pd.DataFrame.from_dict(path_df)

    if path_df.Path.unique().shape[0]>0:
        #Save
        path_df.to_csv(outdir+'optimal_paths.csv', index=None)
    print('Created', path_df.Path.unique().shape[0], 'possible non-overlapping paths resulting in as many complexes')



#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
network = pd.read_csv(args.network[0])
pairdir = args.pairdir[0]
max_paths = args.max_paths[0]
outdir = args.outdir[0]

#Get all edges
edges = np.array(network[['Chain1', 'Chain2']])
sources = np.array(network['Source'])
#Find paths and assemble
complete_paths, complete_sources, complete_coords = find_paths(edges, sources, pairdir, max_paths, outdir)
#Write PDB files of all complete paths
write_pdb(complete_paths, complete_sources, complete_coords, pairdir, outdir)
#Create and save path df
create_path_df(complete_paths, complete_sources, outdir)
