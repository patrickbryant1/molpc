import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Fetch csvs matching a certain glob pattern.''')

parser.add_argument('--pattern_path', nargs=1, type= str, default=sys.stdin, help = 'Path to data and pattern to be globbed.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Path to merged csv.')


################MAIN###############
#Parse args
args = parser.parse_args()
#Data
csvs = glob.glob(args.pattern_path[0])

merged = pd.DataFrame()
for csv in csvs:
    merged = merged.append(pd.read_csv(csv))
#Get model name
if 'Model_name' in merged.columns:
    merged['PDB']=[x.split('_')[0] for x in merged.Model_name]
merged.to_csv(args.outname[0],index=None)
