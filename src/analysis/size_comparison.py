import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from sklearn import metrics
from collections import defaultdict
import pdb

parser = argparse.ArgumentParser(description = '''Compare the size of each assembled complex according to the genetic sequences to that of the native.''')
parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to dir with native complexes and .')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'The name of the output csv with all scores')


################FUNCTIONS#################
def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''
    chain_lens = {}

    with open(pdbfile) as file:
        for line in file:
            if not line.startswith('ATOM'):
                continue
            record = parse_atm_record(line)
            if record['atm_name']=='CA':
                if record['chain'] in [*chain_lens.keys()]:
                    chain_lens[record['chain']]+=1
                else:
                    chain_lens[record['chain']]=0

    return chain_lens


#################MAIN####################
#Parse args
args = parser.parse_args()
pdbdir = args.pdbdir[0]
outname = args.outname[0]
#Get all files
pdb_files = glob.glob(pdbdir+'*.pdb')
#Get sizes
size_comprison = {'ID':[],'PDB_length':[],'Genetic_length':[]}
for pdbname in pdb_files:
    pdb_chain_lens = read_pdb(pdbname)
    genetic_chain_lens = pd.read_csv(pdbname.split('.')[0]+'_useqs.csv')
    size_comprison['ID'].append(pdbname.split('/')[-1].split('.')[0])
    size_comprison['PDB_length'].append(np.sum([pdb_chain_lens[x] for x in pdb_chain_lens]))
    size_comprison['Genetic_length'].append(np.sum([len(x) for x in genetic_chain_lens.Sequence]*genetic_chain_lens.Stoichiometry.values))

#Df
df = pd.DataFrame.from_dict(size_comprison)
df.to_csv(outname, index=None)
