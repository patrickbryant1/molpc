import sys
import os
import pdb

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
import argparse
import networkx as nx
from sklearn import metrics
from scipy.stats import pearsonr, spearmanr
from collections import Counter
from scipy.optimize import curve_fit
import glob


parser = argparse.ArgumentParser(description = '''Visualize data and analyse results''')

parser.add_argument('--afm_dimer_clashes', nargs=1, type= str, required=True, help = "CSV file with AFM dimer clashes")
parser.add_argument('--afm_trimer_clashes', nargs=1, type= str, required=True, help = "CSV file with AFM trimer clashes")
parser.add_argument('--fd_dimer_clashes', nargs=1, type= str, required=True, help = "CSV file with FD dimer clashes")
parser.add_argument('--fd_trimer_clashes', nargs=1, type= str, required=True, help = "CSV file with FD trimer clashes")
parser.add_argument('--afm_guided_dimer_mmscores', nargs=1, type= str, required=True, help = "CSV file with AFM guided dimer TM-scores from MMalign")
parser.add_argument('--afm_guided_trimer_mmscores', nargs=1, type= str, required=True, help = "CSV file with AFM guided trimer TM-scores from MMalign")
parser.add_argument('--afm_all_trimer_mmscores', nargs=1, type= str, required=True, help = "CSV file with AFM alled trimer TM-scores from MMalign")
parser.add_argument('--fd_guided_dimer_mmscores', nargs=1, type= str, required=True, help = "CSV file with FD guided dimer TM-scores from MMalign")
parser.add_argument('--fd_guided_trimer_mmscores', nargs=1, type= str, required=True, help = "CSV file with FD guided trimer TM-scores from MMalign")
parser.add_argument('--fd_all_trimer_mmscores', nargs=1, type= str, required=True, help = "CSV file with FD all trimer TM-scores from MMalign")
parser.add_argument('--haddock_mmscores', nargs=1, type= str, required=True, help = "CSV file with Haddock TM-scores from MMalign")
parser.add_argument('--modelled_complexes', nargs=1, type= str, required=True, help = "CSV file with the modelled complexes")
parser.add_argument('--pdbdir', nargs=1, type= str, required=True, help = "PDB directory")
parser.add_argument('--meta', nargs=1, type= str, required=True, help = "CSV file with info about species and kingdom")
parser.add_argument('--fd_guided_trimer_neffs', nargs=1, type= str, required=True, help = "CSV file with FD guided trimer Neffs")
parser.add_argument('--afm_2_9_mmscores', nargs=1, type= str, required=True, help = "CSV file with MMscores of AFM preds with 2-9 chains not int the AFM train set")
parser.add_argument('--fd_all_trimer_mmscores_4_9', nargs=1, type= str, required=True, help = "CSV file with MMscores of FoldDock assemblies with 4-9 chains not int the AFM train set")
parser.add_argument('--selected_4_9_chains', nargs=1, type= str, required=True, help = "CSV file with selected complexes of 4-9 chains")
parser.add_argument('--all_2_9_chains', nargs=1, type= str, required=True, help = "CSV file with all complexes of 2-9 chains")
parser.add_argument('--guided_dimer_lens', nargs=1, type= str, required=True, help = "CSV file with lens for all true dimer subcomponents")
parser.add_argument('--guided_trimer_lens', nargs=1, type= str, required=True, help = "CSV file with lens for all true trimer subcomponents")
parser.add_argument('--fd_guided_trimer_cs', nargs=1, type= str, required=True, help = "CSV file with FD guided trimer complex scores")
parser.add_argument('--fd_all_trimer_cs', nargs=1, type= str, required=True, help = "CSV file with FD all trimer complex scores")
parser.add_argument('--fd_guided_trimer_subcomponent_mmscores', nargs=1, type= str, required=True, help = "CSV file with FD guided trimer subcomponent MMscores")
parser.add_argument('--afm_mcts_runtimes', nargs=1, type= str, required=True, help = "CSV file with runtimes using the guided trimer approach")
parser.add_argument('--assembled_vs_pred_if_dir', nargs=1, type= str, required=True, help = "Directory containing analysis of assembled vs predicted interfaces.")
parser.add_argument('--outdir', nargs=1, type= str, help = 'Outdir.')


#################FUNCTIONS#################

def analyse_clashes(afm_dimer_clashes, afm_trimer_clashes, fd_dimer_clashes, fd_trimer_clashes):
    '''Analyse the proportion of clashes in total and PDBs with clashes
    '''

    #AFM
    print('---------------------\nAFM Clash analysis')
    #Dimer
    nclashes = afm_dimer_clashes[afm_dimer_clashes.Clash==True].shape[0]
    print('Number of dimer clashes:',nclashes,'out of',len(afm_dimer_clashes),'|',np.round(100*nclashes/len(afm_dimer_clashes),3),'%')

    nclashpdbs_dimer = 0
    for PDB in afm_dimer_clashes.PDB.unique():
        sel = afm_dimer_clashes[afm_dimer_clashes.PDB==PDB]
        if len(sel[sel.Clash==True])>0:
            nclashpdbs_dimer+=1
    print('Number of PDBs with dimer clashes:',nclashpdbs_dimer,'out of',len(afm_dimer_clashes.PDB.unique()),'|',np.round(100*nclashpdbs_dimer/len(afm_dimer_clashes.PDB.unique()),3),'%')

    #Trimer
    nclashes = afm_trimer_clashes[afm_trimer_clashes.Clash==True].shape[0]
    print('Number of trimer clashes:',nclashes,'out of',len(afm_trimer_clashes),'|',np.round(100*nclashes/len(afm_trimer_clashes),3),'%')

    nclashpdbs_trimer = 0
    for PDB in afm_trimer_clashes.PDB.unique():
        sel = afm_trimer_clashes[afm_trimer_clashes.PDB==PDB]
        if len(sel[sel.Clash==True])>0:
            nclashpdbs_trimer+=1
    print('Number of PDBs with trimer clashes:',nclashpdbs_trimer,'out of',len(afm_trimer_clashes.PDB.unique()),'|',np.round(100*nclashpdbs_trimer/len(afm_trimer_clashes.PDB.unique()),3),'%')

    #FD
    print('---------------------\nFD Clash analysis')
    #Dimer
    nclashes = fd_dimer_clashes[fd_dimer_clashes.Clash==True].shape[0]
    print('Number of dimer clashes:',nclashes,'out of',len(fd_dimer_clashes),'|',np.round(100*nclashes/len(fd_dimer_clashes),3),'%')

    nclashpdbs_dimer = 0
    for PDB in fd_dimer_clashes.PDB.unique():
        sel = fd_dimer_clashes[fd_dimer_clashes.PDB==PDB]
        if len(sel[sel.Clash==True])>0:
            nclashpdbs_dimer+=1
    print('Number of PDBs with dimer clashes:',nclashpdbs_dimer,'out of',len(fd_dimer_clashes.PDB.unique()),'|',np.round(100*nclashpdbs_dimer/len(fd_dimer_clashes.PDB.unique()),3),'%')

    #Trimer
    nclashes = fd_trimer_clashes[fd_trimer_clashes.Clash==True].shape[0]
    print('Number of trimer clashes:',nclashes,'out of',len(fd_trimer_clashes),'|',np.round(100*nclashes/len(fd_trimer_clashes),3),'%')

    nclashpdbs_trimer = 0
    for PDB in fd_trimer_clashes.PDB.unique():
        sel = fd_trimer_clashes[fd_trimer_clashes.PDB==PDB]
        if len(sel[sel.Clash==True])>0:
            nclashpdbs_trimer+=1
    print('Number of PDBs with trimer clashes:',nclashpdbs_trimer,'out of',len(fd_trimer_clashes.PDB.unique()),'|',np.round(100*nclashpdbs_trimer/len(fd_trimer_clashes.PDB.unique()),3),'%')


def plot_net(outdir):
    '''Plot networks
    '''
    gdf = pd.DataFrame()
    gdf['Chain1'] = ['1','1','1','2','2','3','3','4','4','5','5','6','6','7','7','8','8','9','9','10','10','11','11','12','12','13','13','14','14']
    gdf['Chain2'] = ['2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']
    #Graph
    G=nx.from_pandas_edgelist(gdf,'Chain1','Chain2')

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    pos = nx.spring_layout(G,seed=0)  # positions for all nodes
    nx.draw(G,pos,with_labels=True, node_size=100, node_color="skyblue", alpha=0.75)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'branch_network.svg',dpi=300, format='svg')
    plt.close()


def plot_mmscores_lc(afm_guided_dimer_mmscores, afm_guided_trimer_mmscores, afm_all_trimer_mmscores, fd_guided_dimer_mmscores, fd_guided_trimer_mmscores, fd_all_trimer_mmscores, haddock_mmscores, outdir):
    '''Plot the TM-scores from MMalign from the large complexes
    '''


    #Guided dimer scores for FD vs AFM
    afm_guided_dimer_mmscores_complete = afm_guided_dimer_mmscores[afm_guided_dimer_mmscores.nchains_in_model==afm_guided_dimer_mmscores.nchains_in_native]
    fd_guided_dimer_mmscores_complete = fd_guided_dimer_mmscores[fd_guided_dimer_mmscores.nchains_in_model==fd_guided_dimer_mmscores.nchains_in_native]
    #Print complete
    print('\nGuided dimer:')
    print('Number of complete using AFM-v2:',len(afm_guided_dimer_mmscores_complete))
    print('Median TM-score using AFM-v2:',afm_guided_dimer_mmscores_complete.TMscore.median())
    print('Number of complete using FD:',len(fd_guided_dimer_mmscores_complete))
    print('Median TM-score using FD:',fd_guided_dimer_mmscores_complete.TMscore.median())

    #merge
    guided_dimer_merged = pd.merge(afm_guided_dimer_mmscores_complete, fd_guided_dimer_mmscores_complete,on='ID', how='outer')
    guided_dimer_merged = guided_dimer_merged.fillna(0)
    print('Number of complete complexes across AFM-v2 and FD:',len(guided_dimer_merged))
    print('Median TM-score for AFM-v2 and FD using the joint complete set:', guided_dimer_merged.TMscore_x.median(), guided_dimer_merged.TMscore_y.median())

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.scatter(guided_dimer_merged.TMscore_x, guided_dimer_merged.TMscore_y,s=10)
    plt.plot([0,1],[0,1],linestyle='--',color='grey')
    plt.xlabel('AFM TM-score')
    plt.ylabel('FoldDock TM-score')
    plt.title('TM-score comparison using\n dimeric subcomponents')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'dimer_tmscore_comparison.png',dpi=300)
    plt.close()

    #Guided trimer scores for FD vs AFM
    afm_guided_trimer_mmscores_complete = afm_guided_trimer_mmscores[afm_guided_trimer_mmscores.nchains_in_model==afm_guided_trimer_mmscores.nchains_in_native]
    fd_guided_trimer_mmscores_complete = fd_guided_trimer_mmscores[fd_guided_trimer_mmscores.nchains_in_model==fd_guided_trimer_mmscores.nchains_in_native]

    #Print complete
    print('\nGuided trimer:')
    print('Number of complete using AFM-v2:',len(afm_guided_trimer_mmscores_complete))
    print('Median TM-score using AFM-v2:',afm_guided_trimer_mmscores_complete.TMscore.median())
    print('Number of complete using FD:',len(fd_guided_trimer_mmscores_complete))
    print('Median TM-score using FD:',fd_guided_trimer_mmscores_complete.TMscore.median())

    #merge
    guided_trimer_merged = pd.merge(afm_guided_trimer_mmscores_complete, fd_guided_trimer_mmscores_complete,on='ID', how='outer')
    guided_trimer_merged = guided_trimer_merged.fillna(0)
    print('Number of complete complexes across AFM-v2 and FD:',len(guided_trimer_merged))
    print('Median TM-score for AFM-v2 and FD using the joint complete set:', guided_trimer_merged.TMscore_x.median(), guided_trimer_merged.TMscore_y.median())

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.scatter(guided_trimer_merged.TMscore_x, guided_trimer_merged.TMscore_y,s=10)
    plt.plot([0,1],[0,1],linestyle='--',color='grey')
    plt.xlabel('AFM TM-score')
    plt.ylabel('FoldDock TM-score')
    plt.title('TM-score comparison using\n trimeric subcomponents')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'trimer_tmscore_comparison.png',dpi=300)
    plt.close()

    #All trimer
    fd_all_trimer_mmscores_complete = fd_all_trimer_mmscores[fd_all_trimer_mmscores.nchains_in_model==fd_all_trimer_mmscores.nchains_in_native]
    afm_all_trimer_mmscores_complete = afm_all_trimer_mmscores[afm_all_trimer_mmscores.nchains_in_model==afm_all_trimer_mmscores.nchains_in_native]

    #Print complete
    print('\nAll trimer:')
    print('Number of complete using AFM-v2:',len(afm_all_trimer_mmscores_complete))
    print('Median TM-score using AFM-v2:',afm_all_trimer_mmscores_complete.TMscore.median())
    print('Number of complete using FD:',len(fd_all_trimer_mmscores_complete))
    print('Median TM-score using FD:',fd_all_trimer_mmscores_complete.TMscore.median())

    #Plot all scores together
    merged_fd = pd.merge(fd_guided_dimer_mmscores_complete[['ID', 'TMscore']], fd_guided_trimer_mmscores_complete[['ID', 'TMscore']],on='ID', how='outer')
    merged_fd = pd.merge(merged_fd, fd_all_trimer_mmscores_complete[['ID', 'TMscore']],on='ID', how='outer')
    merged_afm = pd.merge(afm_guided_dimer_mmscores_complete[['ID', 'TMscore']], afm_guided_trimer_mmscores_complete[['ID', 'TMscore']],on='ID', how='outer')
    merged_afm = pd.merge(merged_afm, afm_all_trimer_mmscores_complete[['ID', 'TMscore']],on='ID', how='outer')

    #Rename
    merged_fd = merged_fd.rename(columns={'TMscore_x':'TMscore_fd_dimer', 'TMscore_y':'TMscore_fd_trimer', 'TMscore':'TMscore_fd_all'})
    merged_afm = merged_afm.rename(columns={'TMscore_x':'TMscore_afm_dimer', 'TMscore_y':'TMscore_afm_trimer', 'TMscore':'TMscore_afm_all'})
    #Fillna
    merged_fd = merged_fd.fillna(0)
    merged_afm = merged_afm.fillna(0)
    #Get missing
    print('\nMissing btw guided dimer and trimer for FD:', len(merged_fd[merged_fd['TMscore_fd_dimer']==0]),'and',len(merged_fd[merged_fd['TMscore_fd_trimer']==0]))
    print('Missing btw guided dimer and trimer for AFM:', len(merged_afm[merged_afm['TMscore_afm_dimer']==0]),'and',len(merged_afm[merged_afm['TMscore_afm_trimer']==0]))

    print('\nMissing btw guided trimer and all trimer for FD:', len(merged_fd[merged_fd['TMscore_fd_trimer']==0]),'and',len(merged_fd[merged_fd['TMscore_fd_all']==0]))
    print('\nMissing btw guided trimer and all trimer for AFM:', len(merged_afm[merged_afm['TMscore_afm_trimer']==0]),'and',len(merged_afm[merged_afm['TMscore_afm_all']==0]))
    #Merge all
    merged = pd.merge(merged_fd, merged_afm, on='ID', how='outer')
    merged = merged.fillna(0)
    print('Total from all approaches',len(merged))


    #Select when trimer approaches are nonzero
    sel = merged[(merged['TMscore_fd_trimer']!=0) & (merged['TMscore_fd_all']!=0)]
    print('Median FD all and known trimer TM-scores when both are successful:', sel['TMscore_fd_all'].median(), sel['TMscore_fd_trimer'].median(),'for',len(sel),'complexes')
    sel = merged[(merged['TMscore_afm_trimer']!=0) & (merged['TMscore_afm_all']!=0)]
    print('Median AFM all and known trimer TM-scores when both are successful:', sel['TMscore_afm_all'].median(), sel['TMscore_afm_trimer'].median(),'for',len(sel),'complexes')

    #Strip plot
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    violin_df = pd.DataFrame()
    violin_df['TM-score'] = np.concatenate([merged.TMscore_fd_dimer.values, merged.TMscore_fd_trimer.values, merged.TMscore_fd_all.values,
                                        merged.TMscore_afm_dimer.values, merged.TMscore_afm_trimer.values, merged.TMscore_afm_all.values])
    violin_df['Subcomponent'] = np.concatenate([['Native FD dimer']*len(merged), ['Native FD trimer']*len(merged),['All FD trimer']*len(merged),
                                                ['Native AFM dimer']*len(merged),  ['Native AFM trimer']*len(merged), ['All AFM trimer']*len(merged)])
    sns.swarmplot(data=violin_df, y='Subcomponent', x='TM-score',size=2, order=['Native AFM dimer', 'Native FD dimer', 'Native AFM trimer', 'Native FD trimer', 'All AFM trimer','All FD trimer'])

    medians = violin_df.groupby('Subcomponent').mean()['TM-score']
    ax.scatter([medians['Native AFM dimer'], medians['Native FD dimer'], medians['Native AFM trimer'], medians['Native FD trimer'],  medians['All AFM trimer'], medians['All FD trimer']],np.arange(6),marker='x', color='k')
    print(medians)
    plt.title('TM-score comparison')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'tmscore_comparison.png',dpi=300)
    plt.close()

    #Add the Haddock scores
    merged = pd.merge(haddock_mmscores,merged, on='ID',how='outer')
    merged = merged.rename(columns={'TM-score':'Haddock_TMscores'})
    merged = merged.loc[merged['ID'].drop_duplicates().index]
    merged = merged.fillna(0)

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    violin_df = pd.DataFrame()
    violin_df['TM-score'] = np.concatenate([merged.TMscore_fd_dimer.values, merged.TMscore_fd_trimer.values, merged.TMscore_fd_all.values,
                                        merged.TMscore_afm_dimer.values, merged.TMscore_afm_trimer.values, merged.TMscore_afm_all.values, merged.Haddock_TMscores.values])
    violin_df['Subcomponent'] = np.concatenate([['Native FD dimer']*len(merged), ['Native FD trimer']*len(merged),['All FD trimer']*len(merged),
                                                ['Native AFM dimer']*len(merged),  ['Native AFM trimer']*len(merged), ['All AFM trimer']*len(merged), ['Haddock']*len(merged)])
    sns.swarmplot(data=violin_df, y='Subcomponent', x='TM-score',size=2, order=['Haddock', 'Native AFM dimer', 'Native FD dimer', 'Native AFM trimer', 'Native FD trimer', 'All AFM trimer','All FD trimer'])

    medians = violin_df.groupby('Subcomponent').mean()['TM-score']
    ax.scatter([medians['Haddock'], medians['Native AFM dimer'], medians['Native FD dimer'], medians['Native AFM trimer'], medians['Native FD trimer'],  medians['All AFM trimer'], medians['All FD trimer']],np.arange(7),marker='x', color='k')
    print(medians)
    plt.title('TM-score comparison')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'tmscore_comparison_haddock.svg',dpi=300, format='svg')
    plt.close()



def plot_chain_distr(modelled_complexes, pdbdir, outdir):
    '''Plot a distribution of the number of chains
    '''

    stochiometry = []
    type = []
    for ind,row in modelled_complexes.iterrows():
        st_df = pd.read_csv(pdbdir+row.Complex+'_useqs.csv')
        stochiometry.append(np.sum(st_df.Stoichiometry.values))
        if len(st_df)>1:
            type.append('Heteromer')
        else:
            type.append('Homomer')

    #Plot
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    #Catplot
    plt.hist(stochiometry,bins=20)
    plt.ylabel('Count')
    plt.xlabel('Number of chains')
    plt.title('Stoichiometry distribution')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'stochiometry_distr.png',dpi=300)
    plt.close()

    stoichiometry_df = pd.DataFrame()
    stoichiometry_df['Complex']=modelled_complexes.Complex.values
    stoichiometry_df['Stoichiometry']=stochiometry
    stoichiometry_df['Oligomer type']=type
    #Add the symmetry
    stoichiometry_df = pd.merge(stoichiometry_df, modelled_complexes, on='Complex')

    #Plot
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    #Catplot
    sns.countplot(y="Symmetry", data=stoichiometry_df,
     order = stoichiometry_df['Symmetry'].value_counts().index)
    plt.xlabel('Count')
    plt.ylabel('Symmetry')
    plt.title('Stoichiometry distribution')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'symmetry_distr.png',dpi=300)
    plt.close()


    return stoichiometry_df

def mmscores_completion(fd_guided_trimer_mmscores, fd_all_trimer_mmscores, outdir):
    '''Plot the TM-scores from MMalign from the large complexes vs the %completion
    '''


    #Calc % completion
    fd_guided_trimer_mmscores['frac_complete']=100*fd_guided_trimer_mmscores.nchains_in_model/fd_guided_trimer_mmscores.nchains_in_native
    #Group in steps by 10
    tm_scores_step = []
    thresholds = []
    ra = []
    step = 10
    for i in np.arange(0,100,step):
        if i+step==100:
            sel = fd_guided_trimer_mmscores[(fd_guided_trimer_mmscores.frac_complete>=i)&(fd_guided_trimer_mmscores.frac_complete<=i+step)]
        else:
            sel = fd_guided_trimer_mmscores[(fd_guided_trimer_mmscores.frac_complete>=i)&(fd_guided_trimer_mmscores.frac_complete<i+step)]
        tm_scores_step.extend([*sel.TMscore.values])
        thresholds.extend([str(i)+'-'+str(i+step)]*len(sel))
        ra.append(sel.TMscore.median())
    swarm_df = pd.DataFrame()
    swarm_df['% completion']=thresholds
    swarm_df['TM-score']=tm_scores_step
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    sns.swarmplot(x='% completion', y='TM-score', data=swarm_df, palette='viridis')
    #plt.scatter(fd_guided_trimer_mmscores.frac_complete, fd_guided_trimer_mmscores.TMscore, s=5)
    plt.plot(np.arange(10),ra,color='grey',alpha=1, label='Median')
    plt.xticks(np.arange(10),np.unique(thresholds), rotation=45)
    plt.title('TM-score and completion')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.legend()
    plt.tight_layout()
    plt.savefig(outdir+'TM_score_frac_complete.png',dpi=300)
    plt.close()


def analyse_success(meta, modelled_complexes, fd_guided_trimer_mmscores, stoichiometry_df, fd_guided_trimer_neffs, fd_guided_trimer_subcomponent_mmscores, outdir):
    '''Analyse factors that may contribute to the assembly success.
    '''


    #Get cols from meta
    meta = meta[['PDB', 'Species', 'Kingdom']]
    meta =  meta.drop_duplicates()
    #Merge with modelled complexes
    merged = pd.merge(meta, modelled_complexes, left_on='PDB', right_on='Complex', how='right')
    #Merge with fd_guided_dimer_mmscores
    merged = pd.merge(merged, fd_guided_trimer_mmscores, left_on='PDB',right_on='ID',how='left')
    #Merge with stoichiometry_df
    merged = pd.merge(merged, stoichiometry_df, left_on='PDB', right_on='Complex')
    #fill NaNs
    merged = merged.fillna(0)
    merged = merged.rename(columns={'Symmetry_x':'Symmetry'})

    #Plot distribution per kd
    #Get top ranked models
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    sel = merged[merged.nchains_in_model==merged.nchains_in_native]
    sel = sel[sel.nchains_in_model!=0]
    print(Counter(merged.Kingdom))
    print(Counter(sel.Kingdom))
    print(sel.groupby('Kingdom').TMscore.median())
    sns.swarmplot(x='Kingdom', y='TMscore', data=sel, palette=['tab:blue','mediumseagreen','orange','magenta'], order=['Bacteria', 'Eukaryota', 'Viruses', 'Archaea'])
    plt.ylabel('TM-score')
    plt.title('TM-score and kingdom')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'TM_score_kd.png',dpi=300)
    plt.close()

    #Plot distribution per symmetry
    #Get top ranked models
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    print(Counter(merged.Symmetry))
    print(Counter(sel.Symmetry))
    print(sel.groupby('Symmetry').TMscore.median())
    print('Median for any symmetry:',sel[sel.Symmetry!='Asymmetrical'].TMscore.median(), 'n=',len(sel[sel.Symmetry!='Asymmetrical']))
    sns.swarmplot(y='Symmetry', x='TMscore', data=sel)
    plt.xlabel('TM-score')
    plt.title('TM-score and Symmetry')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'TM_score_symmetry.png',dpi=300)
    plt.close()
    #Get the best complex for each symmetry
    for symmetry in sel.Symmetry.unique():
        symsel = sel[sel.Symmetry==symmetry]
        symsel = symsel[symsel.TMscore==max(symsel.TMscore)]
        print(symmetry, symsel.PDB.values[0], symsel.TMscore.values[0])

    #Plot Neff vs TMscore
    #Group the neff per ID
    fd_guided_trimer_neffs = fd_guided_trimer_neffs[fd_guided_trimer_neffs.Type=='paired']
    pdbid_neffs = {'ID':[],'Neff':[]}
    for pdbid in sel.ID.unique():
        sel_neff =fd_guided_trimer_neffs[fd_guided_trimer_neffs.PDB==pdbid]
        if len(sel_neff)>0:
            pdbid_neffs['ID'].append(pdbid)
            pdbid_neffs['Neff'].append(sel_neff['Neff'].values.mean())
        else:
            continue
    neff_df = pd.DataFrame.from_dict(pdbid_neffs)
    neff_to_tmscore = pd.merge(neff_df, merged, on='ID',how='left')
    #Bin the Neff scores
    step = 500
    neff_to_tmscore['Bin']=''
    for t in np.arange(0,1000,step):
        neff_to_tmscore.at[neff_to_tmscore[(neff_to_tmscore.Neff>=t)&(neff_to_tmscore.Neff<t+step)].index, 'Bin']=str(t)+'-'+str(t+step)+'\nn='+str(len(neff_to_tmscore[(neff_to_tmscore.Neff>=t)&(neff_to_tmscore.Neff<t+step)]))
    neff_to_tmscore.at[neff_to_tmscore[neff_to_tmscore.Neff>=t+step].index, 'Bin']='≥'+str(t+step)+'\nn='+str(len(neff_to_tmscore[neff_to_tmscore.Neff>=t+step]))

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    neff_order = neff_to_tmscore.sort_values(by='Neff').Bin.unique()
    #sns.violinplot(x='Bin',y='TMscore',data=neff_to_tmscore,order=neff_order)
    sns.swarmplot(x='Bin',y='TMscore',data=neff_to_tmscore,order=neff_order)
    plt.xticks(np.arange(len(neff_order)), neff_order)
    plt.xlabel('Average Neff')
    plt.ylabel('TM-score')
    plt.title('TM-score and Neff')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'tmscore_neff.png',dpi=300)
    plt.close()

    #Plot TM-score vs oligomer type
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    sns.swarmplot(x='Oligomer type',y='TMscore',data=sel)
    plt.ylabel('TM-score')
    plt.title('TM-score and Oligomer type')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'tmscore_oligomer_type.png',dpi=300)
    plt.close()
    #Print medians
    print(sel.groupby('Oligomer type').TMscore.median())
    print(Counter(sel['Oligomer type'].values))

    #Group the fd_guided_trimer_subcomponent_mmscores per ID
    fd_guided_trimer_subcomponent_mmscores_grouped = {'ID':[], 'Avg_TMscore':[]}
    for pdbid in fd_guided_trimer_subcomponent_mmscores.ID.unique():
        sel_sub = fd_guided_trimer_subcomponent_mmscores[fd_guided_trimer_subcomponent_mmscores.ID==pdbid]
        fd_guided_trimer_subcomponent_mmscores_grouped['ID'].append(pdbid)
        fd_guided_trimer_subcomponent_mmscores_grouped['Avg_TMscore'].append(sel_sub.TMscore.mean())
    fd_guided_trimer_subcomponent_mmscores_grouped = pd.DataFrame.from_dict(fd_guided_trimer_subcomponent_mmscores_grouped)
    #Merge
    sel = pd.merge(sel, fd_guided_trimer_subcomponent_mmscores_grouped, on='ID',how='left')

    #Plot avg TM-score for the subcomponents vs oligomer type
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    sns.swarmplot(x='Oligomer type',y='Avg_TMscore',data=sel)
    plt.ylabel('Subcomponent TM-score')
    plt.title('Subcomponent TM-score and Oligomer type')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'subcomponent_tmscore_oligomer_type.png',dpi=300)
    plt.close()
    #Print medians
    print('Subcomponent TM-score per oligomer type',sel.groupby('Oligomer type').Avg_TMscore.median())

    #Plot TM-score vs nchains
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.scatter(sel.nchains_in_model,sel.TMscore,s=5)
    plt.title('TM-score and number of chains')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.xlabel('Number of chains')
    plt.ylabel('TM-score')
    plt.tight_layout()
    plt.savefig(outdir+'tmscore_nchains.png',dpi=300)
    plt.close()

def analyse_all_trimer_per_symmetry(stoichiometry_df, fd_all_trimer_mmscores, outdir):
    '''Analyse the success per symmetry
    '''
    merged = pd.merge(stoichiometry_df, fd_all_trimer_mmscores,left_on='Complex',right_on='ID')
    #Get the complete complexes
    merged = merged[merged.nchains_in_model==merged.nchains_in_native]
    #Get highest per symmetry
    for symmetry in merged.Symmetry.unique():
        sel = merged[merged.Symmetry==symmetry]
        sel =sel[sel.TMscore==max(sel.TMscore)]
        print(symmetry, sel.Complex.values[0], sel.TMscore.values[0])


    #Plot the TM-score distribution
    fig,ax = plt.subplots(figsize=(18/2.54,9/2.54))
    plt.hist(merged.TMscore,color='tab:blue',alpha=0.5,bins=10)
    #plt.title('All FD trimer TM-scores (n=91)')
    plt.xlabel('TM-score')
    plt.ylabel('Count')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'FD_all_trimer_mmscore_distr.png',dpi=300, format='png')
    plt.close()

    #Plot distribution per symmetry
    #Get top ranked models
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    print(Counter(merged.Symmetry))
    print(merged.groupby('Symmetry').TMscore.median())
    print('Median for any symmetry:',merged[merged.Symmetry!='Asymmetrical'].TMscore.median(), 'n=',len(merged[merged.Symmetry!='Asymmetrical']))
    sns.swarmplot(y='Symmetry', x='TMscore', data=merged)
    plt.xlabel('TM-score')
    plt.title('TM-score and Symmetry')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'TM_score_symmetry_all_fd_trimer.svg',dpi=300, format='svg')
    plt.close()


def plot_afm_2_9_mmscore_distr(afm_2_9_mmscores, fd_all_trimer_mmscores_4_9, all_2_9_chains, outdir):
    '''Plot the socre distribution of the AFM TM-scores
    '''

    #Get afm_2_9_mmscores top ranked
    afm_2_9_mmscores = afm_2_9_mmscores[afm_2_9_mmscores.model_num==1]
    #Merge
    merged = pd.merge(fd_all_trimer_mmscores_4_9, afm_2_9_mmscores, left_on='ID', right_on='pdbid',how='left')
    #Set incomplete assemblies to 0
    merged.at[merged[merged.nchains_in_model!=merged.nchains_in_native].index,'TMscore']=0
    merged['MMscore']=merged.MMscore.fillna(0)
    #Swarm
    swarm_df = pd.DataFrame()
    swarm_df['TM-score']=np.concatenate([merged.TMscore.values,merged.MMscore.values])
    swarm_df['Number of chains']=np.concatenate([merged.nchains_in_native.values,merged.nchains_in_native.values])
    swarm_df['Method']=np.concatenate([['MCTS']*len(merged),['AFM E2E']*len(merged)])
    swarm_df['Oligomer']=np.concatenate([merged['class'].values,merged['class'].values])

    means_mcts = swarm_df[swarm_df.Method=='MCTS'].groupby('Number of chains').mean()['TM-score']
    means_mcts_complete =  merged[merged.nchains_in_model==merged.nchains_in_native].groupby('nchains_in_native').mean()['TMscore']
    print('Number complete for MCTS',len(merged[merged.nchains_in_model==merged.nchains_in_native]))
    print('Number complete for AFM E2E',len(merged[merged.MMscore!=0]))
    means_afm_e2e = swarm_df[swarm_df.Method=='AFM E2E'].groupby('Number of chains').mean()['TM-score']
    means_mcts_complete_afm_e2e =  merged[merged.nchains_in_model==merged.nchains_in_native].groupby('nchains_in_native').mean()['MMscore']
    print('MCTS',means_mcts)
    print('AFM E2E',means_afm_e2e)
    #Violin plot
    fig,ax = plt.subplots(figsize=(18/2.54,9/2.54))
    sns.stripplot(x='Number of chains',y='TM-score',hue='Method', data=swarm_df, dodge=True, size=2)
    plt.scatter(np.arange(6)-0.225,means_mcts.values,marker='x',color='k')
    plt.scatter(np.arange(6)+0.225,means_afm_e2e.values,marker='x',color='k')
    plt.xticks(np.arange(6),[4,5,6,7,8,9])
    plt.title('MCTS vs end-to-end predictions')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'tmscores_4_9.svg',dpi=300, format='svg')
    plt.close()

    #Plot mers
    means_mcts = swarm_df[swarm_df.Method=='MCTS'].groupby('Oligomer').mean()['TM-score']
    means_afm_e2e = swarm_df[swarm_df.Method=='AFM E2E'].groupby('Oligomer').mean()['TM-score']
    print('MCTS',means_mcts)
    print('AFM E2E',means_afm_e2e)
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    sns.stripplot(x='Oligomer',y='TM-score',hue='Method', data=swarm_df, dodge=True, size=2)
    plt.scatter(np.arange(2)-0.225,means_mcts.values,marker='x')
    plt.scatter(np.arange(2)+0.225,means_afm_e2e.values,marker='x')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'oligomer_2_9.png',dpi=300)
    plt.close()

    #Analyse AFM 2-9
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    afm_2_9_mmscores = pd.merge(all_2_9_chains, afm_2_9_mmscores[['pdbid', 'MMscore']], on='pdbid', how='left')
    afm_2_9_mmscores['MMscore'] = afm_2_9_mmscores['MMscore'].fillna(0)
    sns.swarmplot(x='num_chains',y='MMscore', data=afm_2_9_mmscores, size=2)
    means_afm_e2e = afm_2_9_mmscores.groupby('num_chains').mean()['MMscore']
    plt.scatter(np.arange(8),means_afm_e2e.values,marker='x',color='k')
    plt.xlabel('Number of chains')
    plt.ylabel('TM-score')
    plt.title('AFM E2E per chain')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'afm_2_9.svg',dpi=300, format='svg')
    plt.close()



def get_n_ints(pdbdir, outdir):
    '''Analyse the number of interactions for each complex
    '''

    ints = glob.glob(pdbdir+'*_ints.csv')

    ints_per_complex = []
    contacts_per_int = []
    for name in ints:
        complex_ints = pd.read_csv(name)
        ints_per_complex.append(len(complex_ints))
        contacts_per_int.extend([*complex_ints.n_contacts.values])

    #Plot number of ints
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.hist(ints_per_complex,bins=20)
    plt.xlabel('Number of interactions per complex')
    plt.ylabel('Count')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.title('Interactions per complex')
    plt.tight_layout()
    plt.savefig(outdir+'ints_per_complex.png',dpi=300)
    plt.close()
    print('Average number of interactions per complex:',np.round(np.average(ints_per_complex),1))

    #Plot number of contacts
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.hist(contacts_per_int,bins=20)
    plt.xlabel('Number of contacts per interaction')
    plt.ylabel('Count')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.title('Contacts per interaction')
    plt.tight_layout()
    plt.savefig(outdir+'contacts_per_int.png',dpi=300)
    plt.close()
    print('Average number of contacts per interaction:',np.round(np.average(contacts_per_int),1))

def subcomponent_length_distribution(guided_dimer_lens, guided_trimer_lens, outdir):
    '''Analyse the lengths of the true dimer and trimer subcomponents
    '''

    print('Number of true dimers with over 3000 residues:',len(guided_dimer_lens[guided_dimer_lens.length>3000]),'out of', len(guided_dimer_lens))
    print('Number of true trimers with over 3000 residues:',len(guided_trimer_lens[guided_trimer_lens.length>3000]),'out of', len(guided_trimer_lens))

def complex_lengths(pdbdir, outdir):
    '''Analyse the length of entire complexes
    '''

    useqs = glob.glob(pdbdir+'*_useqs.csv')

    complex_lens = []
    for name in useqs:
        useq = pd.read_csv(name)
        entire_length = 0
        for ind,row in useq.iterrows():
            entire_length+=len(row.Sequence)*row.Stoichiometry
        complex_lens.append(entire_length)

    complex_lens = np.array(complex_lens)
    print('Number of complexes with over 3000 residues:',len(complex_lens[complex_lens>3000]),'out of', len(complex_lens))


def complex_score_analysis_guided(fd_guided_trimer_cs, fd_guided_trimer_mmscores, fd_guided_trimer_subcomponent_mmscores, outdir):
    '''Analyse when a complex has a high TM-score
    '''

    #Group the cs per id
    grouped_CS = {'ID':[], 'n_ints':[], 'av_IF_plDDT':[], 'n_contacts':[], 'n_IF_residues':[]}
    for id in fd_guided_trimer_cs.ID.unique():
        sel = fd_guided_trimer_cs[fd_guided_trimer_cs.ID==id]
        grouped_CS['ID'].append(id)
        grouped_CS['n_ints'].append(sel.n_ints.sum())
        grouped_CS['av_IF_plDDT'].append(np.average(sel.sum_av_IF_plDDT/sel.n_ints))
        grouped_CS['n_contacts'].append(sel.n_contacts.sum())
        grouped_CS['n_IF_residues'].append(sel.n_IF_residues.sum())

    grouped_CS = pd.DataFrame.from_dict(grouped_CS)
    #Merge
    merged = pd.merge(grouped_CS, fd_guided_trimer_mmscores, on='ID', how='left')
    merged['Complete']=0
    merged.loc[merged[merged.nchains_in_model==merged.nchains_in_native].index, 'Complete']=1

    #Threshold
    merged['above_t']=0
    merged.loc[merged[merged.TMscore>=0.9].index, 'above_t']=1
    merged['n_IF_residues_per_chain']=merged.n_IF_residues/merged.nchains_in_model
    merged['n_contacts_per_chain']=merged.n_contacts/merged.nchains_in_model
    merged['IF plDDT⋅log(IF contacts)'] =merged.av_IF_plDDT*np.log10(merged.n_contacts+0.001)
    merged['n_ints_per_chain']=merged.n_ints/merged.nchains_in_model

    #Fill NaNs
    merged = merged.fillna(0)
    #Go through features and create ROC
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    nicer_names = {'av_IF_plDDT':'plDDT', 'n_IF_residues_per_chain':'Residues/chain', 'n_contacts_per_chain':'Contacts/chain',
                'IF plDDT⋅log(IF contacts)':'mpDockQ', 'n_ints_per_chain':'Interactions/chain'}

    for feature in ['av_IF_plDDT' , 'n_IF_residues_per_chain', 'n_contacts_per_chain', 'n_ints_per_chain', 'IF plDDT⋅log(IF contacts)']:
        #Create ROC
        fpr, tpr, threshold = metrics.roc_curve(merged.Complete.values, merged[feature].values, pos_label=1)
        roc_auc = metrics.auc(fpr, tpr)
        plt.plot(fpr, tpr, label =nicer_names[feature]+': %0.2f' % roc_auc)

    plt.plot([0,1],[0,1],linewidth=1,linestyle='--',color='grey')
    plt.legend(fontsize=9)
    plt.title('ROC for assembly completion')
    plt.xlabel('FPR')
    plt.ylabel('TPR')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'ROC_fd_guided_trimer.png',format='png',dpi=300)
    plt.close()

    #Plot pDockQ vs TM-score
    sel = merged[merged.Complete==1]
    #Create ROC
    sel['Above median']=0
    sel.loc[sel[sel.TMscore>=sel.TMscore.median()].index,'Above median']=1
    fpr, tpr, threshold = metrics.roc_curve(sel['Above median'].values, sel['IF plDDT⋅log(IF contacts)'].values, pos_label=1)
    roc_auc = metrics.auc(fpr, tpr)
    print('ROC AUC as a function of above median TM-score using mpDockQ:', roc_auc)
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.scatter(sel['IF plDDT⋅log(IF contacts)'], sel.TMscore,s=5,label='ROC AUC:%0.2f' % roc_auc)
    plt.xlabel('mpDockQ')
    plt.ylabel('TM-score')
    plt.title('mpDockQ vs TM-score')
    plt.legend()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'pdockq_tmscore.png',format='png',dpi=300)
    plt.close()
    print('Spearman R', spearmanr(sel['IF plDDT⋅log(IF contacts)'].values, sel.TMscore.values),'for mpDockQ vs TM-score')



    #Group the fd_guided_trimer_subcomponent_mmscores per ID
    fd_guided_trimer_subcomponent_mmscores_grouped = {'ID':[], 'Avg_TMscore':[]}
    for pdbid in fd_guided_trimer_subcomponent_mmscores.ID.unique():
        sel = fd_guided_trimer_subcomponent_mmscores[fd_guided_trimer_subcomponent_mmscores.ID==pdbid]
        fd_guided_trimer_subcomponent_mmscores_grouped['ID'].append(pdbid)
        fd_guided_trimer_subcomponent_mmscores_grouped['Avg_TMscore'].append(sel.TMscore.mean())
    fd_guided_trimer_subcomponent_mmscores_grouped = pd.DataFrame.from_dict(fd_guided_trimer_subcomponent_mmscores_grouped)
    #Merge with complex scores
    merged = pd.merge(fd_guided_trimer_subcomponent_mmscores_grouped, fd_guided_trimer_mmscores, on='ID',how='left')
    sel = merged[merged.nchains_in_model==merged.nchains_in_native]
    sel = sel.fillna(0)
    #Plot avg subcomponent TM-score vs complex TM-score for the complete complexes
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.scatter(sel.Avg_TMscore, sel.TMscore,s=5)
    plt.xlabel('Subcomponent TM-score')
    plt.ylabel('Complex TM-score')
    plt.title('Subcomponent TM-score\nvs Complex TM-score')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.xlim([0,1])
    plt.ylim([0,1])
    plt.tight_layout()
    plt.savefig(outdir+'sub_vs_complex_tmscore.png',format='png',dpi=300)
    plt.close()
    print('Spearman R', spearmanr(sel.Avg_TMscore.values, sel.TMscore.values),'for subcomponent vs complex TM-score')

    #Plot avg subcomponent TM-score vs complex TM-score for all complexes (incomplete as well)
    merged = merged.fillna(0)
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.scatter(merged.Avg_TMscore, merged.TMscore,s=5)
    plt.xlabel('Subcomponent TM-score')
    plt.ylabel('Complex TM-score')
    plt.title('Subcomponent TM-score\nvs Complex TM-score (all complexes)')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.xlim([0,1])
    plt.ylim([0,1])
    plt.tight_layout()
    plt.savefig(outdir+'sub_vs_complex_tmscore_all.png',format='png',dpi=300)
    plt.close()
    print('Spearman R', spearmanr(merged.Avg_TMscore.values, merged.TMscore.values),'for subcomponent vs complex TM-score using all complexes (inlcuding incomplete ones)')


    #Create ROC
    t = 0.8
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    #All
    merged['above_t']=0
    merged.loc[merged[merged.TMscore>=0.8].index, 'above_t']=1
    print(Counter(merged.above_t))
    fpr, tpr, threshold = metrics.roc_curve(merged.above_t.values, merged['Avg_TMscore'].values, pos_label=1)
    roc_auc = metrics.auc(fpr, tpr)
    plt.plot(fpr, tpr, label = 'All complexes'+': %0.2f' % roc_auc)
    #Complete
    sel['above_t']=0
    sel.loc[sel[sel.TMscore>=0.8].index, 'above_t']=1
    print(Counter(sel.above_t))
    fpr, tpr, threshold = metrics.roc_curve(sel.above_t.values, sel['Avg_TMscore'].values, pos_label=1)
    roc_auc = metrics.auc(fpr, tpr)
    plt.plot(fpr, tpr, label = 'Complete complexes'+': %0.2f' % roc_auc)
    plt.plot([0,1],[0,1],linestyle='--',color='grey')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.title('ROC for complexes with TM-score ≥0.8 as a \nfunction of the average subcomponent TM-score', fontsize=8)
    plt.xlabel('FPR')
    plt.ylabel('TPR')
    plt.legend()
    plt.tight_layout()
    plt.savefig(outdir+'ROC_sub_vs_complex_tmscore_all.png',format='png',dpi=300)
    plt.close()

def complex_score_analysis_all(fd_all_trimer_cs, fd_all_trimer_mmscores, outdir):
    '''Analyse when a complex has a high TM-score
    '''

    #Group the cs per id
    grouped_CS = {'ID':[], 'n_ints':[], 'av_IF_plDDT':[], 'n_contacts':[], 'n_IF_residues':[]}
    for id in fd_all_trimer_cs.ID.unique():
        sel = fd_all_trimer_cs[fd_all_trimer_cs.ID==id]
        grouped_CS['ID'].append(id)
        grouped_CS['n_ints'].append(sel.n_ints.sum())
        grouped_CS['av_IF_plDDT'].append(np.average(sel.sum_av_IF_plDDT/sel.n_ints))
        grouped_CS['n_contacts'].append(sel.n_contacts.sum())
        grouped_CS['n_IF_residues'].append(sel.n_IF_residues.sum())

    grouped_CS = pd.DataFrame.from_dict(grouped_CS)
    #Merge
    merged = pd.merge(grouped_CS, fd_all_trimer_mmscores, on='ID', how='left')
    #Fill NaNs
    #merged = merged.fillna(0)
    merged = merged.dropna()
    merged['Complete']=0
    merged['Complete_above_t']=0
    merged.at[merged[merged.nchains_in_model==merged.nchains_in_native].index, 'Complete']=1
    t=0.8
    merged.at[merged[(merged.nchains_in_model==merged.nchains_in_native)&(merged.TMscore>=t)].index, 'Complete_above_t']=1
    print(len(merged[merged.Complete_above_t==1]),'complete complexes with a TM-score of at least',t)
    merged['n_IF_residues_per_chain']=merged.n_IF_residues/merged.nchains_in_model
    merged['n_contacts_per_chain']=merged.n_contacts/merged.nchains_in_model
    merged['plDDT⋅log(contacts)'] =merged.av_IF_plDDT*np.log10(merged.n_contacts+0.001)
    merged['n_ints_per_chain']=merged.n_ints/merged.nchains_in_model
    merged['Completion'] = merged.nchains_in_model/merged.nchains_in_native

    #Do a simple sigmoid fit
    def sigmoid(x, L ,x0, k, b):
        y = L / (1 + np.exp(-k*(x-x0)))+b
        return (y)

    sel = merged[merged.Complete==1]
    plddt_if_contacts = sel['plDDT⋅log(contacts)'].values
    tmscores = sel.TMscore.values
    xdata =plddt_if_contacts[np.argsort(plddt_if_contacts)]
    ydata = tmscores[np.argsort(tmscores)]
    p0 = [max(ydata), np.median(xdata),1,min(ydata)] # this is an mandatory initial guess
    popt, pcov = curve_fit(sigmoid, xdata, ydata,p0, method='dogbox')
    print('L=',np.round(popt[0],3),'x0=',np.round(popt[1],3), 'k=',np.round(popt[2],3), 'b=',np.round(popt[3],3))
    y = sigmoid(merged['plDDT⋅log(contacts)'].values, *popt)
    merged['mpDockQ']=y

    print('Number of complete above',t,':',len(merged[merged.Complete_above_t==1]))

    #Go through features and create ROC
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    nicer_names = {'av_IF_plDDT':'plDDT', 'n_IF_residues_per_chain':'Residues/chain', 'n_contacts_per_chain':'Contacts/chain',
                  'n_ints_per_chain':'Interactions/chain', 'plDDT⋅log(contacts)':'plDDT⋅log(contacts)', 'mpDockQ':'mpDockQ'}

    colors = {'av_IF_plDDT':'darkblue', 'n_IF_residues_per_chain':'magenta', 'n_contacts_per_chain':'orange',
                  'n_ints_per_chain':'darkgreen', 'plDDT⋅log(contacts)':'tab:blue', 'mpDockQ':'grey'}

    #Inverse n_IF_residues_per_chain, n_contacts_per_chain and n_ints_per_chain
    merged['n_IF_residues_per_chain']=1/merged['n_IF_residues_per_chain']
    merged['n_contacts_per_chain']=1/merged['n_contacts_per_chain']
    merged['n_ints_per_chain']=1/merged['n_ints_per_chain']

    for feature in nicer_names:
        #Create ROC
        fpr, tpr, threshold = metrics.roc_curve(merged.Complete_above_t.values, merged[feature].values, pos_label=1)
        roc_auc = metrics.auc(fpr, tpr)
        if feature == 'mpDockQ':
            plt.plot(fpr, tpr, label =nicer_names[feature]+': %0.2f' % roc_auc, color=colors[feature], linestyle='--')
        else:
            plt.plot(fpr, tpr, label =nicer_names[feature]+': %0.2f' % roc_auc, color=colors[feature])

    plt.plot([0,1],[0,1],linewidth=1,linestyle='--',color='grey')
    plt.legend(fontsize=9)
    plt.title('ROC for complete assemblies\n with TM-score ≥ '+str(t))
    plt.xlabel('FPR')
    plt.ylabel('TPR')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'ROC_fd_all_trimer.png',format='png',dpi=300)
    plt.close()

    fpr10t = threshold[np.argwhere(fpr<0.105)[-1,0]]
    print('TPR at FPR 10%:', tpr[np.argwhere(fpr<0.105)[-1,0]], 'threshold:',fpr10t)
    print('Median TM-score at FPR 10%',merged[merged['mpDockQ']>fpr10t].TMscore.median())
    print('Median TM-score for complete models at FPR 10%',merged[(merged['mpDockQ']>fpr10t)&(merged.Complete==1)].TMscore.median(),len(merged[(merged['mpDockQ']>fpr10t)&(merged.Complete==1)]),'complexes')
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))


    x_mpDockQ = merged['plDDT⋅log(contacts)'].values
    x_mpDockQ = x_mpDockQ[np.argsort(x_mpDockQ)]
    y_mpDockQ = sigmoid(x_mpDockQ, *popt)
    plt.plot(x_mpDockQ,y_mpDockQ,color='grey',label='mpDockQ')
    plt.scatter(merged['plDDT⋅log(contacts)'], merged['TMscore'],s=5,c=merged.Completion)
    clb=plt.colorbar(orientation='vertical',label='% completion')
    clb.ax.tick_params(labelsize=8)
    plt.xlabel('plDDT⋅log(contacts)')
    plt.ylabel('TM-score')
    plt.title('mpDockQ and TM-score\ncoloured by % completion')
    plt.legend()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'mpdockq_tmscore.png',format='png',dpi=300)
    plt.close()

    #Select FPR 10%
    sel = merged[merged.mpDockQ>fpr10t]
    print('Selected',len(sel),'complexes')
    print(sel[['ID','nchains_in_model', 'nchains_in_native','TMscore','mpDockQ']])
    print(fpr10t)

def analyse_runtime(afm_mcts_runtimes, afm_guided_trimer_mmscores, outdir):
    '''Analyse runtime
    '''

    merged = pd.merge(afm_mcts_runtimes, afm_guided_trimer_mmscores,on='ID')
    print('Average MCTS assemlby time in hours using known trimeric subcomponents:',merged.runtime_in_seconds.mean()/3600)

def analyse_ifs_pred_assembly(assembled_vs_pred_if_dir, outdir):
    '''Analyse the assembled vs predicted interfaces
    '''
    score_files = glob.glob(assembled_vs_pred_if_dir+'*_if_comp.csv')
    #Save
    results = {'ID':[], 'pred_IF_avg':[],'assembled_IF_avg':[]}
    for name in score_files:
        df = pd.read_csv(name)
        results['ID'].append(name.split('/')[-1].split('_')[0])
        results['pred_IF_avg'].append(df.loc[df.Source.dropna().index].DockQ.mean())
        results['assembled_IF_avg'].append(df.loc[np.setdiff1d(np.arange(len(df)),df.Source.dropna().index)].DockQ.mean())

    #Create df of fetched scores
    results_df = pd.DataFrame.from_dict(results)
    results_df = results_df.dropna()
    print('Number of complexes with assembled interfaces:',len(results_df))
    print('Average DockQ for pred ifs:',results_df.pred_IF_avg.mean())
    print('Average DockQ for new ifs:',results_df.assembled_IF_avg.mean())


#################MAIN####################

#Parse args
args = parser.parse_args()
#Clashes
afm_dimer_clashes = pd.read_csv(args.afm_dimer_clashes[0])
afm_trimer_clashes = pd.read_csv(args.afm_trimer_clashes[0])
fd_dimer_clashes = pd.read_csv(args.fd_dimer_clashes[0])
fd_trimer_clashes = pd.read_csv(args.fd_trimer_clashes[0])
#MMscores
afm_guided_dimer_mmscores = pd.read_csv(args.afm_guided_dimer_mmscores[0])
afm_guided_trimer_mmscores = pd.read_csv(args.afm_guided_trimer_mmscores[0])
afm_all_trimer_mmscores = pd.read_csv(args.afm_all_trimer_mmscores[0])
fd_guided_dimer_mmscores = pd.read_csv(args.fd_guided_dimer_mmscores[0])
fd_guided_trimer_mmscores = pd.read_csv(args.fd_guided_trimer_mmscores[0])
fd_all_trimer_mmscores = pd.read_csv(args.fd_all_trimer_mmscores[0])
haddock_mmscores = pd.read_csv(args.haddock_mmscores[0])

modelled_complexes = pd.read_csv(args.modelled_complexes[0])
pdbdir = args.pdbdir[0]
meta = pd.read_csv(args.meta[0])
#Neff FD known trimers
fd_guided_trimer_neffs = pd.read_csv(args.fd_guided_trimer_neffs[0])

#AFM - using 2-9 chains
afm_2_9_mmscores = pd.read_csv(args.afm_2_9_mmscores[0])
#FoldDock assembly of 4-9 chains
fd_all_trimer_mmscores_4_9 = pd.read_csv(args.fd_all_trimer_mmscores_4_9[0])
selected_4_9_chains = pd.read_csv(args.selected_4_9_chains[0])
selected_4_9_chains = selected_4_9_chains.rename(columns={'pdbid':'ID'})
fd_all_trimer_mmscores_4_9 = pd.merge(selected_4_9_chains,fd_all_trimer_mmscores_4_9,on='ID',how='left')
fd_all_trimer_mmscores_4_9 = fd_all_trimer_mmscores_4_9.fillna(0)
fd_all_trimer_mmscores_4_9 = fd_all_trimer_mmscores_4_9[['ID','nchains_in_model', 'num_chains', 'TMscore']]
fd_all_trimer_mmscores_4_9 = fd_all_trimer_mmscores_4_9.rename(columns={'num_chains':'nchains_in_native'})
#All
all_2_9_chains = pd.read_csv(args.all_2_9_chains[0])
#Lengths of subcomponents
guided_dimer_lens = pd.read_csv(args.guided_dimer_lens[0])
guided_trimer_lens = pd.read_csv(args.guided_trimer_lens[0])
#Complex scores - to evaluate when a complex is successful
fd_guided_trimer_cs = pd.read_csv(args.fd_guided_trimer_cs[0])
fd_all_trimer_cs = pd.read_csv(args.fd_all_trimer_cs[0])
fd_guided_trimer_subcomponent_mmscores = pd.read_csv(args.fd_guided_trimer_subcomponent_mmscores[0])
#Run times
afm_mcts_runtimes = pd.read_csv(args.afm_mcts_runtimes[0])
#Assembly vs pred ifs
assembled_vs_pred_if_dir = args.assembled_vs_pred_if_dir[0]
outdir = args.outdir[0]
#Plot and analyse
#analyse_clashes(afm_dimer_clashes, afm_trimer_clashes, fd_dimer_clashes, fd_trimer_clashes)
#plot_net(outdir)
#plot_mmscores_lc(afm_guided_dimer_mmscores, afm_guided_trimer_mmscores, afm_all_trimer_mmscores, fd_guided_dimer_mmscores, fd_guided_trimer_mmscores, fd_all_trimer_mmscores, haddock_mmscores, outdir)
#mmscores_completion(fd_guided_trimer_mmscores, fd_all_trimer_mmscores, outdir)
#stoichiometry_df = plot_chain_distr(modelled_complexes, pdbdir, outdir)
#analyse_success(meta, modelled_complexes, fd_guided_trimer_mmscores, stoichiometry_df, fd_guided_trimer_neffs, fd_guided_trimer_subcomponent_mmscores, outdir)
#analyse_all_trimer_per_symmetry(stoichiometry_df, fd_all_trimer_mmscores, outdir)
#plot_afm_2_9_mmscore_distr(afm_2_9_mmscores, fd_all_trimer_mmscores_4_9, all_2_9_chains, outdir)
#get_n_ints(pdbdir, outdir)
#subcomponent_length_distribution(guided_dimer_lens, guided_trimer_lens, outdir)
#complex_lengths(pdbdir, outdir)
complex_score_analysis_guided(fd_guided_trimer_cs, fd_guided_trimer_mmscores, fd_guided_trimer_subcomponent_mmscores, outdir)
#complex_score_analysis_all(fd_all_trimer_cs, fd_all_trimer_mmscores, outdir)
#analyse_runtime(afm_mcts_runtimes, afm_guided_trimer_mmscores, outdir)
#analyse_ifs_pred_assembly(assembled_vs_pred_if_dir, outdir)
