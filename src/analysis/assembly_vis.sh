#Assemble for visualisation
#Assemble all complexes according to the optimal path df
MODEL=../../data/all_pdb_over9/6ESQ/6ESQ_complex204.pdb #Path to model
OUTDIR=../../data/all_pdb_over9/6ESQ/
#python3 ./assembly_vis.py --model $MODEL --outdir $OUTDIR
#Visualise the assembly steps on the network
PATHDF=../../data/all_pdb_over9/6ESQ/optimal_paths.csv
META=../../data/all_pdb_over9/PDB/6ESQ_ints.csv
SCORES=../../data/all_pdb_over9/6ESQ/complex_scores.csv
OUTDIR=../../data/all_pdb_over9/6ESQ/
python3 ./network_vis.py --path_df $PATHDF \
--meta $META --complex_scores $SCORES --outdir $OUTDIR
