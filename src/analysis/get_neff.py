import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import subprocess
import pdb

parser = argparse.ArgumentParser(description = '''Perform structural alignment towards the native structure.''')

parser.add_argument('--msadir', nargs=1, type= str, default=sys.stdin, help = 'Path to dir with modelled complexes.')
parser.add_argument('--id', nargs=1, type= str, default=sys.stdin, help = 'Path to native complex')
parser.add_argument('--MMseqs2', nargs=1, type= str, default=sys.stdin, help = 'Path to MMseqs2 executable')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to outdir')


##############FUNCTIONS###############

def run_mmseqs2(MMseqs2, msadir, id):
    '''Align pdb1 with 2 and fetch the TMscore and RMSD
    '''

    files = glob.glob(msadir+id+'*.a3m')
    names = []
    types = []
    nclusts = []
    for name in files:
        #Cluster at 62% seqid
        output = subprocess.run([MMseqs2, 'easy-cluster', name, outdir+id, '/tmp', '--min-seq-id', '0.62', '-c', '0.8', '--cov-mode', '1'])
        if output.returncode!=0:
            print('Clustering error')
            sys.exit()
        else:
            #Read clusters
            cluster_df = pd.read_csv(outdir+id+'_cluster.tsv',sep='\t',header=None)
            nclusts.append(cluster_df[0].unique().shape[0])
            name = name.split('/')[-1]
            names.append(name.split('_')[1]) #subcomponent id
            types.append(name.split('_')[2][:-4]) #paired or blocked
    #Create df
    neff_df = pd.DataFrame()
    neff_df['Component']=names
    neff_df['Type']=types
    neff_df['Neff']=nclusts
    neff_df['PDB']=id

    return neff_df


################MAIN###############
#Parse args
args = parser.parse_args()
msadir = args.msadir[0]
id = args.id[0]
MMseqs2 = args.MMseqs2[0]
outdir = args.outdir[0]

neff_df = run_mmseqs2(MMseqs2, msadir, id)
neff_df.to_csv(outdir+id+'_neff.csv', index=None)
