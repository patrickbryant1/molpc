import argparse
import sys
import os
import numpy as np
import pandas as pd
from collections import Counter, defaultdict
import glob
import subprocess
import pdb

parser = argparse.ArgumentParser(description = '''Perform structural alignment towards the native structure using subcomponents.''')

parser.add_argument('--model_id', nargs=1, type= str, default=sys.stdin, help = 'Model id.')
parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to directory with subcomponents.')
parser.add_argument('--native', nargs=1, type= str, default=sys.stdin, help = 'Path to native complex')
parser.add_argument('--MMalign', nargs=1, type= str, default=sys.stdin, help = 'Path to MMalign executable')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Path to output csv')


##############FUNCTIONS###############

def run_mmalign(MMalign, pdb1, pdb2):
    '''Align pdb1 with 2 and fetch the TMscore and RMSD
    '''

    #Performs optimal structural alignment
    output = subprocess.check_output([MMalign, pdb1, pdb2])
    #Parse TM-score
    output = output.decode("utf-8").split('\n')
    for line in output:
        #Structure 1 is the native --> use TM-score normalized by that
        if 'TM-score' in line and 'normalized by length of Structure_1' in line:
            tmscore = float(line.split(' ')[1])

        if 'RMSD=' in line:
            rmsd = float(line.split(',')[1].split(' ')[-1])

    return tmscore, rmsd


################MAIN###############
#Parse args
args = parser.parse_args()
model_id = args.model_id[0]
pdbdir = args.pdbdir[0]
native = args.native[0]
MMalign = args.MMalign[0]
outname = args.outname[0]

#Get all subcomplexes
sub_complexes = glob.glob(pdbdir+model_id+'*/*_rw.pdb') #Gets the rewritten predictions of subcomplexes
#Run MMalign
score_df = {'SUBID':[], 'TMscore':[], 'RMSD':[]}
#If chains are missing - see how big the subcomponent is
for sub_complex in sub_complexes:
    tmscore, rmsd = run_mmalign(MMalign, sub_complex, native)
    score_df['SUBID'].append(sub_complex.split('/')[-2])
    score_df['TMscore'].append(tmscore)
    score_df['RMSD'].append(rmsd)

#Save
score_df = pd.DataFrame.from_dict(score_df)
score_df['ID']=model_id
#Save df
score_df.to_csv(outname)
