#Visualize data and results
META_PETRAS=../../data/petras_lists/meta.csv
META_LENS=../../data/petras_lists/meta_lens.csv
HUMAP=../../data/humap/humap2_complexes_20200809.csv
NPATHS=../../data/test/ABCD_npaths.csv
#MARKS
DOCKQ_DIMER=../../data/petras_lists/dockq_heterodimer.csv
#Petra's lists
DOCKQ_TRIMER=../../data/petras_lists/3mer_DockQMulti_all.csv
DOCKQ_TETRAMER=../../data/petras_lists/4mer_DockQMulti_all.csv
DOCKQ_PENTAMER=../../data/petras_lists/5mer_DockQMulti_all.csv
DOCKQ_HEXAMER=../../data/petras_lists/6mer_DockQMulti_all.csv
#Assembly
DOCKQ_TETRAMER_ASS=../../data/petras_lists/4mer_assembly.csv
OUTDIR=../../plots/
python3 ./vis.py --meta_petras $META_PETRAS \
--meta_lens $META_LENS \
--humap_complexes $HUMAP \
--npaths $NPATHS \
--dockq_dimer $DOCKQ_DIMER --dockq_trimer $DOCKQ_TRIMER \
--dockq_tetramer $DOCKQ_TETRAMER --dockq_pentamer $DOCKQ_PENTAMER \
--dockq_hexamer $DOCKQ_HEXAMER \
--dockq_tetramer_ass $DOCKQ_TETRAMER_ASS \
--outdir $OUTDIR
