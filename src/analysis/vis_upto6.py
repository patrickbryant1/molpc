
import sys
import os
import pdb

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
from collections import Counter
import argparse


parser = argparse.ArgumentParser(description = '''Visualize data and results''')

parser.add_argument('--meta_petras', nargs=1, type= str, required=True, help = "CSV file with meta of Petra's Lists")
parser.add_argument('--meta_lens', nargs=1, type= str, required=True, help = "CSV file with lengths of complexes from Petra's Lists")
parser.add_argument('--humap_complexes', nargs=1, type= str, required=True, help = "CSV file with hu.MAP complexes")
parser.add_argument('--npaths', nargs=1, type= str, required=True, help = "CSV file with Number of paths with one overlap")
parser.add_argument('--dockq_dimer', nargs=1, type= str, required=True, help = "CSV file with DockQ for dimers")
parser.add_argument('--dockq_trimer', nargs=1, type= str, required=True, help = "CSV file with DockQ for trimers")
parser.add_argument('--dockq_tetramer', nargs=1, type= str, required=True, help = "CSV file with DockQ for tetramers")
parser.add_argument('--dockq_pentamer', nargs=1, type= str, required=True, help = "CSV file with DockQ for pentamers")
parser.add_argument('--dockq_hexamer', nargs=1, type= str, required=True, help = "CSV file with DockQ for hexamers")
parser.add_argument('--dockq_tetramer_ass', nargs=1, type= str, required=True, help = "CSV file with DockQ for assembled tetramers")

parser.add_argument('--outdir', nargs=1, type= str, help = 'Outdir.')


#################FUNCTIONS#################
def vis_pdb_distr(meta_petras, outdir):
    '''Visualize the distribution of different mers in the dataset
    of the non-redundant pdb files from Petras
    '''

    fig,ax = plt.subplots(figsize=(18/2.54,12/2.54))

    mer_colors = {3:'tab:blue',4:'tab:orange',5:'tab:green',6:'magenta'}
    xtick_labels = []
    xvals = []
    xind=0 #Keep track of ind
    yvals = []
    limits = []
    colors = []
    #Go through all mers and plot
    for mer in np.sort(meta_petras.Order.unique()):
        sel = meta_petras[meta_petras.Order==mer]
        counts = Counter(sel.Type)
        count_types = [*counts.keys()]
        count_vals = [*counts.values()]
        for i in np.argsort(count_vals)[::-1]:
            xind+=1
            xtick_labels.append(count_types[i])
            xvals.append(xind)
            yvals.append(count_vals[i])
            colors.append(mer_colors[mer])
        #Limit
        limits.append(len(sel.Type.unique())+1)
        xind+=1

    plt.bar(xvals, yvals,color=colors)
    plt.xticks(xvals,xtick_labels,rotation=45)

    #Divide
    mers = {'Trimers':0.75,'Tetramers':5.5,'Pentamers':12.5,'Hexamers':19.5}
    limits = np.cumsum(limits)
    for i in range(len(limits)):
        mer = [*mers.keys()][i]
        if i!=len(limits)-1:
            plt.axvline(limits[i],0,550,linestyle='--',color='gray')
        plt.text(mers[mer],560,mer)

    plt.ylabel('Count')
    plt.title('Oligomer distribution')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'meta_petras_distr.png',format='png',dpi=300)
    plt.close()


def vis_humap(humap_complexes, outdir):
    '''Visualize the hu.MAP data
    '''
    u_genes = []
    n_chains = []
    for i in range(len(humap_complexes)):
        row = humap_complexes.loc[i]
        u_genes.extend(row.Uniprot_ACCs.split())
        n_chains.append(len(row.Uniprot_ACCs.split()))
    #Plot nchains vs confidence
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    sns.kdeplot(humap_complexes.number_of_chains,humap_complexes.Confidence)
    plt.xlim([0,12])
    plt.xlabel('Number of chains')
    plt.title('Confidence and oligomeric state')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'humap_kde_nchains_conf.png',format='png',dpi=300)
    plt.close()
    #Plot nchains distr
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.hist(humap_complexes.number_of_chains)
    plt.yscale('log')
    plt.xlabel('Number of chains')
    plt.ylabel('Count')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'humap_nchains_distr.png',format='png',dpi=300)
    plt.close()

    #Plot the unique gene distribution
    counts = Counter(u_genes)
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.hist([*counts.values()],bins=10)
    plt.title('Gene occurrence')
    #plt.yscale('log')
    plt.xlabel('Gene occurrence')
    plt.ylabel('Count')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'humap_gene_distr.png',format='png',dpi=300)
    plt.close()

def plot_n_combos(npaths, outdir):
    '''Plot the number of combinations
    that visit all nodes
    '''

    x = npaths.Nodes.unique()
    y = np.power(x,x-2)
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.plot(x,y,label='No overlap')
    #Plot with one overlap
    plt.plot(x,npaths[npaths.Overlap==1]['Paths']+0.01,label='One overlap')
    plt.plot(x,npaths[npaths.Overlap==2]['Paths']+0.01,label='Two overlaps')
    plt.ylabel('Number of paths')
    plt.xlabel('Number of nodes')
    plt.title('Complete paths and nodes')
    plt.legend()
    #plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'npaths.png',format='png',dpi=300)
    plt.close()

def plot_dockq_per_chain(dockq_dimer, dockq_trimer, dockq_tetramer, dockq_pentamer, dockq_hexamer, dockq_tetramer_ass, outdir):
    '''Plot the dockq scores per chain
    '''


    scores = [dockq_dimer.DockQ.values, dockq_trimer.dockq_multiIF.values, dockq_tetramer.dockq_multiIF.values,
                dockq_pentamer.dockq_multiIF.values, dockq_hexamer.dockq_multiIF.values]
    nchains = []
    cat_scores = []
    method = []
    for i in range(len(scores)):
        nchains.extend([i+2]*len(scores[i]))
        cat_scores.extend([*scores[i]])
        method.extend(['Entire complex']*len(scores[i]))
        print('AFM',i+2,np.round(np.argwhere(scores[i]>=0.23).shape[0]/len(scores[i]),3),np.round(np.median(scores[i]),3))

    #Add assembly
    ass_scores = [dockq_tetramer_ass.DockQ.values]
    for i in range(len(ass_scores)):
        nchains.extend([i+4]*len(ass_scores[i]))
        cat_scores.extend([*ass_scores[i]])
        method.extend(['Assembly']*len(ass_scores[i]))
        print('Assembly',i+4,np.round(np.argwhere(ass_scores[i]>=0.23).shape[0]/len(ass_scores[i]),3),np.round(np.median(ass_scores[i]),3))

    score_df = pd.DataFrame()
    score_df['Number of chains'] = nchains
    score_df['DockQ'] = cat_scores
    score_df['Method'] = method
    fig,ax = plt.subplots(figsize=(12/2.54,9/2.54))
    sns.violinplot(x="Number of chains", y="DockQ",
                data=score_df, hue='Method',
                palette="Set2")

    plt.title('DockQ vs Number of chains')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'dockq_nchains.png',format='png',dpi=300)
    plt.close()

def avail_vs_len(meta_lens, dockq_dimer, dockq_trimer, dockq_tetramer, dockq_pentamer, dockq_hexamer, outdir):
    '''Analyse the results related to the complex lengths
    '''

    dockq_dimer['PDB'] = [x[:4] for x in dockq_dimer.Complex_id]

    #Concat
    dockq_res = pd.concat([dockq_trimer[['pdbid','chain1','dockq_multiIF']], dockq_tetramer[['pdbid','chain1','dockq_multiIF']],
                dockq_pentamer[['pdbid','chain1','dockq_multiIF']], dockq_hexamer[['pdbid','chain1','dockq_multiIF']]])

    #DockQ per pdb
    dockq_per_pdb = []
    for id in dockq_res.pdbid.unique():
        sel = dockq_res[dockq_res.pdbid==id]
        dockq_per_pdb.append(sel.dockq_multiIF.mean())
    dockq_df = pd.DataFrame()
    dockq_df['PDB'] = dockq_res.pdbid.unique()
    dockq_df['DockQ'] = dockq_per_pdb

    #Complex lens
    len_per_complex = []
    nchains = []
    for id in meta_lens.PDB.unique():
        sel = meta_lens[meta_lens.PDB==id]
        len_per_complex.append(sel.Seqlen.sum())
        nchains.append(len(sel))
    len_df = pd.DataFrame()
    len_df['PDB'] = meta_lens.PDB.unique()
    len_df['Length'] = len_per_complex
    len_df['Number of chains'] = nchains

    #Merge
    merged = pd.merge(len_df, dockq_df, on='PDB', how='left')
    notnan = merged.dropna()
    isnan = merged[merged.DockQ.isna()]
    #plt.scatter(notnan.Length,notnan.DockQ)
    #sns.kdeplot(notnan.Length,notnan.DockQ)

    #Hist of length of could be modeled vs not
    frac_modelled = [0.998]
    for n in np.sort(merged['Number of chains'].unique()):
        sel = merged[merged['Number of chains']==n]
        frac_modelled.append(1-len(sel[sel.DockQ.isna()])/len(sel))

    fig,ax = plt.subplots(figsize=(9/2.54,6/2.54))
    plt.scatter([2,3,4,5,6],frac_modelled,label='Entire complex',color='seagreen')
    plt.legend()
    plt.title('% Modelled vs Number of chains')
    plt.xlabel('Number of chains')
    plt.ylabel('% Modelled')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'modelled_nchains.png',format='png',dpi=300)
    plt.close()


#################MAIN####################

#Parse args
args = parser.parse_args()
#Petras
meta_petras = pd.read_csv(args.meta_petras[0])
meta_lens = pd.read_csv(args.meta_lens[0])
#HuMAP
humap_complexes = pd.read_csv(args.humap_complexes[0])
#Petras results
dockq_dimer = pd.read_csv(args.dockq_dimer[0])
dockq_trimer = pd.read_csv(args.dockq_trimer[0])
dockq_tetramer = pd.read_csv(args.dockq_tetramer[0])
dockq_pentamer = pd.read_csv(args.dockq_pentamer[0])
dockq_hexamer = pd.read_csv(args.dockq_hexamer[0])
#Assembly
dockq_tetramer_ass = pd.read_csv(args.dockq_tetramer_ass[0])
#Path complexity
npaths = pd.read_csv(args.npaths[0])
outdir = args.outdir[0]

#Vis
#vis_pdb_distr(meta_petras, outdir)
#vis_humap(humap_complexes, outdir)
#plot_n_combos(npaths, outdir)
plot_dockq_per_chain(dockq_dimer, dockq_trimer, dockq_tetramer, dockq_pentamer, dockq_hexamer, dockq_tetramer_ass, outdir)
#avail_vs_len(meta_lens, dockq_dimer, dockq_trimer, dockq_tetramer, dockq_pentamer, dockq_hexamer, outdir)
