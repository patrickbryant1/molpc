import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import matplotlib.pyplot as plt
import networkx as nx
import pdb

parser = argparse.ArgumentParser(description = '''Print the order of link additions in a stepwise manner for a predetermined path.''')

parser.add_argument('--path_df', nargs=1, type= str, default=sys.stdin, help = 'csv containing assembly paths to explore.')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb ids and interacting chains.')
parser.add_argument('--complex_scores', nargs=1, type= str, default=sys.stdin, help = 'Scores for each assembly path.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Output directory.')

#################FUNCTIONS#################
def plot_nets(G, subnet, outdir, ind):
	'''Plot networks
	'''

	fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
	pos = nx.spring_layout(G,seed=0)  # positions for all nodes
	nx.draw(G,pos,with_labels=True, node_size=100, node_color="skyblue", alpha=0.75)
	nx.draw_networkx_edges(G,pos, edgelist=[tuple(x) for x in subnet], width=5, edge_color="tab:red",  arrowsize=20, arrowstyle='fancy')
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	plt.tight_layout()
	plt.savefig(outdir+'sub_network'+str(ind)+'.png',dpi=300)
	plt.close()

################MAIN###############
#Parse args
args = parser.parse_args()
#Data
path_df = pd.read_csv(args.path_df[0])
meta = pd.read_csv(args.meta[0])
complex_scores = pd.read_csv(args.complex_scores[0])
outdir = args.outdir[0]


#Top scoring path
path = int(complex_scores.loc[0].Model)
#Get circuit
circuit = path_df[path_df.Path==path]
circuit = circuit.reset_index()
#Graph
G=nx.from_pandas_edgelist(meta,'Chain1','Chain2')
#Current node
cn = circuit.loc[0]['Chain1']
bfs = [cn+'-'+circuit.loc[0]['Chain2']]
step = 0
subnet=circuit.loc[:1,['Chain1','Chain2']].values
plot_nets(G, subnet,outdir, 1)

for i in range(1,len(circuit)):
	row = circuit.loc[i]
	subnet=circuit.loc[:i,['Chain1','Chain2']].values
	plot_nets(G, subnet,outdir, i)
pdb.set_trace()
