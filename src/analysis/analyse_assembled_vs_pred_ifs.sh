IDS=../../data/all_pdb_over9/guided_trimer_pdb/complete_complexes.txt
PDBDIR=../../data/all_pdb_over9/PDB/
MODELDIR=../../data/all_pdb_over9/guided_trimer_pdb/
MMALIGN=/home/patrick/molpc_lab/src/complex_assembly/MMalign
DOCKQ=/home/patrick/DockQ/DockQ.py
OUTDIR=../../data/all_pdb_over9/guided_trimer_pdb/
for LN in {1..58}
do
  ID=$(sed -n $LN'p' $IDS)
  PDB=$PDBDIR/$ID'.pdb'
  MODEL=$MODELDIR/$ID'_best_complex.pdb'
  ASSEMBLY=$MODELDIR/$ID'_optimal_path.csv'
  OUTPUT=$OUTDIR$ID'_if_comp.csv'
  if test -f $OUTPUT; then
    echo $ID
  else
    echo $ID
    python3 ./DockQcomparison.py -p $PDB -m $MODEL \
    -a $ASSEMBLY  -M $MMALIGN -D $DOCKQ -o $OUTPUT
  fi
done
