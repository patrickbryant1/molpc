import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob

import pdb

parser = argparse.ArgumentParser(description = '''Fetch all DockQ scores and merge into one csv.''')
parser.add_argument('--modeldir', nargs=1, type= str, default=sys.stdin, help = 'Path to directory with models of all assembled complexes.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'The name of the output csv with all pDockQ scores')


################FUNCTIONS#################

def fetch_dockq(modeldir, outname):
    '''Fetch all csvs
    '''
    score_files = glob.glob(modeldir+'*/complex/dockq.csv')
    score_df = pd.DataFrame()
    for name in score_files:
        score_df = score_df.append(pd.read_csv(name))


    pdb.set_trace()
    score_df.to_csv(outname, index=None)

#################MAIN####################

#Parse args
args = parser.parse_args()

modeldir = args.modeldir[0]
outname = args.outname[0]
fetch_dockq(modeldir, outname)
