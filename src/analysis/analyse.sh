AFM_DIMER_CLASHES=../../data/all_pdb_over9/AFM_v2_dimer_clashes.csv
AFM_TRIMER_CLASHES=../../data/all_pdb_over9/AFM_v2_trimer_clashes.csv
FD_DIMER_CLASHES=../../data/all_pdb_over9/FD_dimer_clashes.csv
FD_TRIMER_CLASHES=../../data/all_pdb_over9/FD_guided_trimer_clashes.csv
AFM_GUIDED_DIMER_MMSCORES=../../data/all_pdb_over9/AFM_v2_guided_dimer_mmscores.csv
AFM_GUIDED_TRIMER_MMSCORES=../../data/all_pdb_over9/AFM_v2_guided_trimer_mmscores.csv
AFM_ALL_TRIMER_MMSCORES=../../data/all_pdb_over9/AFM_v2_all_trimer_mmscores.csv
FD_GUIDED_DIMER_MMSCORES=../../data/all_pdb_over9/FD_guided_dimer_mmscores.csv
FD_GUIDED_TRIMER_MMSCORES=../../data/all_pdb_over9/FD_guided_trimer_mmscores.csv
FD_ALL_TRIMER_MMSCORES=../../data/all_pdb_over9/FD_all_trimer_mmscores.csv
HADDOCK_MMSCORES=../../data/all_pdb_over9/Haddock_TMscores.csv
MODELLED_COMPLEXES=../../data/all_pdb_over9/modelled_complexes.csv
PDBDIR=../../data/all_pdb_over9/PDB/
META=../../data/all_pdb_over9/selected_complexes.csv
FD_GUIDED_TRIMER_NEFFS=../../data/all_pdb_over9/FD_guided_trimer_neff.csv
AFM_2_9_MMSCORES=../../data/selected_4_9_chains/all_mmscores_AFM_2_9.csv
FD_ALL_TRIMER_MMSCORES_4_9=../../data/selected_4_9_chains/all_trimer_FD_mmscores.csv
SELECTED_4_9=../../data/selected_4_9_chains/selected_complexes.csv
ALL_2_9=../../data/selected_4_9_chains/ordered_complexes_2_9.csv
GUIDED_DIMER_LENS=../../data/all_pdb_over9/dimer_lens.csv
GUIDED_TRIMER_LENS=../../data/all_pdb_over9/trimer_lens.csv
FD_GUIDED_TRIMER_CS=../../data/all_pdb_over9/FD_guided_trimer_complex_scores.csv
FD_ALL_TRIMER_CS=../../data/all_pdb_over9/FD_all_trimer_complex_scores.csv
FD_GUIDED_TRIMER_SUBCOMPONENT_MMSCORES=../../data/all_pdb_over9/FD_guided_trimer_subcomponent_mmscores.csv
AFM_MCTS_RUNTIMES=../../data/all_pdb_over9/mcts_guided_trimer_afmv2_runtimes.csv
ASSEMBLED_VS_PRED_IF_DIR=../../data/all_pdb_over9/guided_trimer_pdb/
OUTDIR=../../plots/
python3 ./vis_analysis.py --afm_dimer_clashes $AFM_DIMER_CLASHES \
--afm_trimer_clashes $AFM_TRIMER_CLASHES \
--fd_dimer_clashes $FD_DIMER_CLASHES \
--fd_trimer_clashes $FD_TRIMER_CLASHES \
--afm_guided_dimer_mmscores $AFM_GUIDED_DIMER_MMSCORES \
--afm_guided_trimer_mmscores $AFM_GUIDED_TRIMER_MMSCORES \
--afm_all_trimer_mmscores $AFM_ALL_TRIMER_MMSCORES \
--fd_guided_dimer_mmscores $FD_GUIDED_DIMER_MMSCORES \
--fd_guided_trimer_mmscores $FD_GUIDED_TRIMER_MMSCORES \
--fd_all_trimer_mmscores $FD_ALL_TRIMER_MMSCORES \
--haddock_mmscores $HADDOCK_MMSCORES \
--modelled_complexes $MODELLED_COMPLEXES \
--pdbdir $PDBDIR \
--meta $META \
--fd_guided_trimer_neffs $FD_GUIDED_TRIMER_NEFFS \
--afm_2_9_mmscores $AFM_2_9_MMSCORES \
--fd_all_trimer_mmscores_4_9 $FD_ALL_TRIMER_MMSCORES_4_9 \
--selected_4_9_chains $SELECTED_4_9 \
--all_2_9_chains $ALL_2_9 \
--guided_dimer_lens $GUIDED_DIMER_LENS \
--guided_trimer_lens $GUIDED_TRIMER_LENS \
--fd_guided_trimer_cs $FD_GUIDED_TRIMER_CS \
--fd_all_trimer_cs $FD_ALL_TRIMER_CS \
--fd_guided_trimer_subcomponent_mmscores $FD_GUIDED_TRIMER_SUBCOMPONENT_MMSCORES \
--afm_mcts_runtimes $AFM_MCTS_RUNTIMES \
--assembled_vs_pred_if_dir $ASSEMBLED_VS_PRED_IF_DIR \
--outdir $OUTDIR
