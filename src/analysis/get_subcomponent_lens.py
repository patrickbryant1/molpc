import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import subprocess
import pdb

parser = argparse.ArgumentParser(description = '''Get the length of subcomponents.''')

parser.add_argument('--pattern', nargs=1, type= str, default=sys.stdin, help = 'Pattern to glob for.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Path to write results to (.csv)')


##############FUNCTIONS###############

def get_lens(filenames, outname):
    '''Get lengths of all subcomponents
    '''

    results = {'subcomponent':[], 'length':[]}
    for name in filenames:
        results['subcomponent'].append(name.split('/')[-1].split('.')[0])
        with open(name, 'r') as file:
            for line in file:
                results['length'].append(np.array(line.rstrip().split('|')[-1].split('-'),dtype='int').sum())
                break

    #Save
    results_df = pd.DataFrame.from_dict(results)
    results_df.to_csv(outname, index=None)


################MAIN###############
#Parse args
args = parser.parse_args()
filenames = glob.glob(args.pattern[0])
get_lens(filenames, args.outname[0])
