import argparse
import sys
import os
import numpy as np
import pandas as pd
from collections import Counter, defaultdict
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Analyse clashes in predicted models.''')

parser.add_argument('--datadir', nargs=1, type= str, default=sys.stdin, help = 'Path to data with assembled complexes.')
parser.add_argument('--model_id', nargs=1, type= str, default=sys.stdin, help = 'Model id to glob for.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory.')

##############FUNCTIONS###############

def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''
    pdb_chains = {}
    pdb_coords = {}
    with open(pdbfile) as file:
        for line in file:
            if not line.startswith('ATOM'):
                continue
            record = parse_atm_record(line)
            if record['chain'] in [*pdb_chains.keys()]:
                pdb_chains[record['chain']].append(line)
                pdb_coords[record['chain']].append([record['x'],record['y'],record['z']])
            else:
                pdb_chains[record['chain']] = [line]
                pdb_coords[record['chain']] = [[record['x'],record['y'],record['z']]]


    return pdb_chains, pdb_coords


def check_clashes(pdb_coords):
    '''Check overlapping chains and remove if found
    '''
    chains = [*pdb_coords.keys()]
    clash=False
    print('Checking clashes...')
    for i in range(len(chains)-1):
        overlap_found = False
        coords_i = pdb_coords[chains[i]]
        l1 = len(coords_i) #Length of chain 1
        for j in range(i+1,len(chains)):
            coords_j = pdb_coords[chains[j]]
            #Check CA overlap
            #Calc 2-norm
            mat = np.append(coords_i, coords_j,axis=0)
            a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
            dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
            contact_dists = dists[l1:,:l1]
            clashes = np.argwhere(contact_dists<=1) #1 Å threshold

            if clashes.shape[0]>0: #If clash
                print('Clash found',chains[i],chains[j])
                clash = True
                return clash

    return clash


################MAIN###############
#Parse args
args = parser.parse_args()
#Data
datadir = args.datadir[0]
models = glob.glob(datadir+args.model_id[0]+'*/ranked_0.pdb')
outdir = args.outdir[0]
#Go through all models
model_names = []
clashes = []
for model in models:
    try:
        pdb_chains, pdb_coords = read_pdb(model)
        #Check clashes
        clash = check_clashes(pdb_coords)
        model_names.append(model.split('/')[-2])
        clashes.append(clash)
    except:
        continue
#df
clash_df = pd.DataFrame()
clash_df['Model_name']=model_names
clash_df['Clash']=clashes
clash_df.to_csv(outdir+args.model_id[0]+'_clashes.csv', index=None)
