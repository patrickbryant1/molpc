import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import shutil
from collections import Counter, defaultdict
import pdb

parser = argparse.ArgumentParser(description = '''Visualise the assembly path.''')
parser.add_argument('--model', nargs=1, type= str, default=sys.stdin, help = 'Path to PDB complex.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Where to write all complexes')



##############FUNCTIONS##############

def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''
    pdb_chains = {}

    with open(pdbfile) as file:
        for line in file:
            record = parse_atm_record(line)
            if record['chain'] in [*pdb_chains.keys()]:
                pdb_chains[record['chain']].append(line)
            else:
                pdb_chains[record['chain']] = [line]



    return pdb_chains

def write_pdb(model, outdir):
    '''Write PDB in a stepwise fashion
    '''
    cn=0
    with open(outdir+'complex.pdb', 'w') as file:
        for chain in model:
            cn+=1
            for line in model[chain]:
                file.write(line)
            #Copy complex
            shutil.copy(outdir+'complex.pdb', outdir+'sub_complex'+str(cn)+'.pdb')


#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
model = read_pdb(args.model[0])
outdir = args.outdir[0]

#Write PDB file in assembly order
write_pdb(model, outdir)
