#Write fasta
FASTADIR=../../../data/petras_lists/6mer/fasta/
OUTDIR=../../../data/petras_lists/6mer/
#python3 ./write_ind_fasta.py --fastadir $FASTADIR --outdir $OUTDIR

#Check species
SPECIES_CSV=../../../data/petras_lists/6mer/hexamer_species.tsv
python3 ./get_same_species.py --species_csv $SPECIES_CSV --outdir $OUTDIR
