#Get all data necessary for the analysis
#and perform some initial EDA


#############Petra's list##############
#1 fuse all meta information into one csv
DATADIR=../../../data/petras_lists/
#python3 fetch_meta.py --datadir $DATADIR

###############hu.MAP###############
#EDA - extract unique genes and plot
#nchains vs confidence
HUMAP=../../../data/humap/humap2_complexes_20200809.csv
PLOTDIR=../../../plots/
OUTDIR=../../../data/humap/
#python3 ./ectract_humap.py --humap_complexes $HUMAP --plotdir $PLOTDIR --outdir $OUTDIR

#Complex to PDB - see how many complexes are in the PDB already
HUMAP=../../../data/humap/humap2_complexes_20200809.csv
UNI_TO_PDB=../../../data/humap/uniprot_to_pdb.tsv
NCHAINS=3 #Min number of chains
OUTDIR=../../../data/humap/
# python3 humap_complex_in_pdb.py --humap_complexes $HUMAP \
# --uniprot_to_pdb $UNI_TO_PDB --nchains $NCHAINS --outdir $OUTDIR
