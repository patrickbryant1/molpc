import argparse
import sys
import os
import pandas as pd
import numpy as np
import itertools
import shutil
import glob
import pdb


parser = argparse.ArgumentParser(description = '''Create the folder structure for all MSAs to run the prediction using AF-multimer.''')

parser.add_argument('--msadir', nargs=1, type= str, default=sys.stdin, help = 'Path to data.')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'PDB id.')
parser.add_argument('--sub_size', nargs=1, type= int, default=sys.stdin, help = 'Size of the smallest sub-component to predict (e.g. predict all dimers (2), trimers (3) or entire complex (1)).')
parser.add_argument('--complex_size', nargs=1, type= int, default=sys.stdin, help = 'Size of the entire complex (number of chains, e.g. 3 = trimer).')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory (where to create the folder structure). Include /in end')


#########Functions###########
def read_fasta(fastaname):
    '''Read all chains and sequences in a fasta file
    '''
    
    with open(fastaname, 'r') as file:
        seq=''
        for line in file:
            line = line.rstrip()
            if line[0]=='>':
                fastaid=line[1:]
            else:
                seq+=line

        return seq

def reorder(seqs):
    '''Analyse which seqs are identical and reorder
    '''

    chains = np.sort([*seqs.keys()])
    identities = {}
    #Get all mappings for all consecutive chains (in order)
    for i in range(len(chains)):
        identities[chains[i]] = []
        for j in range(i+1,len(chains)):
            if seqs[chains[i]]==seqs[chains[j]]:
                identities[chains[i]].append(chains[j])
        
    #Create mapping
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    mapping = {}
    ai = 0
    fetched_keys = []
    for key in identities:
        if key in fetched_keys:
            continue

        mapping[alphabet[ai]]=key
        fetched_keys.append(key)
        ai+=1
        for ci in identities[key]:
            mapping[alphabet[ai]]=ci
            fetched_keys.append(ci)
            ai+=1
    #Get reverse mapping
    reverse_mapping = dict(zip([*mapping.values()], [*mapping.keys()]))
    return mapping, reverse_mapping, identities

def create_folder_structure(sel, msadir, complex_size, sub_size, outdir):
    '''Create the folder structure for AF-multimer
    For each type,
    If numeric --> skip n-1 letters in folder assignment
    E.g. A2B --> make folder A, skip B, make folder C
    '''
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    for i in range(len(sel)):
        print(i, 'out of', len(sel))
        row = sel.loc[i]


        try:
            #Get seqs
            seqs = {}
            for cn in range(complex_size):
                seqs[alphabet[cn]] = read_fasta(msadir+row.PDB+'_'+alphabet[cn]+'.fasta')
        except:
            print('Sequence not available', row.PDB)
            continue
        #Reorder
        mapping, reverse_mapping, identities = reorder(seqs)
        
        #Create all combinations
        combinations = [x for x in itertools.combinations(mapping.keys(),sub_size)]
        #Go through all combinations
        for combo in combinations:
            #Create the combo dir
            msa_chain_dir = outdir+row.PDB+'_'+''.join(combo)
            #Check if dir exists
            if os.path.exists(msa_chain_dir):
                print('Directory '+msa_chain_dir+' exists...')
                continue
            else:
                os.mkdir(msa_chain_dir)
                os.mkdir(msa_chain_dir+'/msas')
            
            #Write fasta
            with open(outdir+row.PDB+'_'+''.join(combo)+'.fasta', 'w') as file:
                for chain in combo:
                    file.write('>'+row.PDB+'_'+chain+'\n')
                    file.write(seqs[mapping[chain]]+'\n')
            
            #Get all mapings for combo
            combo_map = [mapping[x] for x in combo]
            #Go through the combo and create the structure
            combo_chains = [*combo]
            ucid = 0 #What id to use
            while len(combo_chains)>0:
                ck = combo_chains.pop(0)
                uchain = alphabet[ucid] #What the output dir should be named according to AFM folder structure
                try:
                    identical_chains = identities[mapping[ck]]
                    #Get 
                    mapped_identical_chains = [reverse_mapping[x] for x in identical_chains]
                    if np.intersect1d(mapped_identical_chains,combo_chains).shape[0]>0:
                        #Increase unique chain index
                        ucid+=1
                        #Update
                        combo_chains = list(np.setdiff1d(combo_chains,mapped_identical_chains))
                except:
                    print('No identical chains')

                #Copy to msadir
                try:
                    os.mkdir(msa_chain_dir+'/msas/'+uchain)
                except:
                    print('Directory '+msa_chain_dir+'/msas/'+uchain+' exists...')
                chain_msas = glob.glob(msadir+row.PDB+'_'+mapping[ck]+'/msas/A/*_hits*')
                #Copy
                for msa in chain_msas:
                    shutil.copyfile(msa, msa_chain_dir+'/msas/'+uchain+'/'+msa.split('/')[-1])
                
                #Increase unique chain index
                ucid+=1
        
    


################MAIN###############
#Parse args
args = parser.parse_args()
#Data
msadir = args.msadir[0]
meta = pd.read_csv(args.meta[0])
sub_size = args.sub_size[0]
complex_size = args.complex_size[0]
outdir = args.outdir[0]

#Select all complexes of size complex_size
sel = meta[meta.Order==complex_size]
sel = sel.reset_index()
create_folder_structure(sel, msadir, complex_size, sub_size, outdir)
