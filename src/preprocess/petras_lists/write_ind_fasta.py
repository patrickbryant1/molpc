import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Write the unique fasta chains for each complex and also fetch the stoichiometry of these to be compiled into a df.''')

parser.add_argument('--fastadir', nargs=1, type= str, default=sys.stdin, help = 'Path to data.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')



##################MAIN#######################
def read_fasta(filename, outdir):
    '''Read all fasta files and get the sequence lengths
    '''

    id_to_seq = {}
    id_to_stoichiometry = {}
    with open(filename, 'r') as file:
        for line in file:
            line = line.rstrip()
            if line[0]=='>':
                #Get id
                id=line[1:]
                #Stoichiometry
                if id in [*id_to_stoichiometry.keys()]:
                    id_to_stoichiometry[id]+=1
                else:
                    id_to_stoichiometry[id]=1
            else:
                id_to_seq[id]=line

    #DF
    #Unique seqs
    pdbid=filename.split('/')[-1].split('.')[0]
    useq_df = pd.DataFrame()
    useq_df['Stoichiometry']=[*id_to_stoichiometry.values()]
    useq_df['SeqID']=range(1,len(useq_df)+1)
    useq_df['PDB']=pdbid
    ordered_seqs = []
    for key in id_to_stoichiometry:
        ordered_seqs.append(id_to_seq[key])
    useq_df['Sequence']=ordered_seqs
    #Save df
    useq_df.to_csv(outdir+'PDB/'+pdbid+'_useqs.csv', index=None)
    #All chain seqs
    alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    chain_seqs = {'Useq':[],'Sequence':[]}
    for ind, row in useq_df.iterrows():
        chain_seqs['Useq'].extend([row.SeqID]*row.Stoichiometry)
        chain_seqs['Sequence'].extend([row.Sequence]*row.Stoichiometry)
    seq_df = pd.DataFrame.from_dict(chain_seqs)
    seq_df['Chain']=alphabet[:len(seq_df)]
    #Save df
    seq_df.to_csv(outdir+'PDB/'+pdbid+'_seqs.csv', index=None)

    #Write useqs to fasta
    write_fasta(useq_df, pdbid, outdir+'fasta/unique_chains/')


def write_fasta(useq_df, pdbid, outdir):
    '''Write fasta files with unique sequences
    '''

    for ind, row in useq_df.iterrows():
        with open(outdir+pdbid+'_'+str(row.SeqID)+'.fasta', 'w') as file:
            file.write('>'+pdbid+'_'+str(row.SeqID)+'\n')
            file.write(row.Sequence)

############MAIN#############
#Parse args
args = parser.parse_args()
#Data
fastadir = args.fastadir[0]
outdir = args.outdir[0]

for filename in glob.glob(fastadir+'/*.fasta'):
    read_fasta(filename, outdir)
