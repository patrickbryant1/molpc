import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Get the lengths of all the chains and the entire complex.''')

parser.add_argument('--fastadir', nargs=1, type= str, default=sys.stdin, help = 'Path to data.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')



##################MAIN#######################
def read_fasta(fastadir, outdir):
    '''Read all fasta files and get the sequence lengths
    '''

    dirs = glob.glob(fastadir+'*/*/all_fasta')
    #Save
    results = {'PDB':[],'Chain':[],'Seqlen':[]}
    for dir in dirs:
        for filename in glob.glob(dir+'/*.fasta'):
            with open(filename, 'r') as file:
                for line in file:
                    line = line.rstrip()
                    if line[0]=='>':
                        results['PDB'].append(line[1:5])
                        results['Chain'].append(line[-1])
                    else:
                        results['Seqlen'].append(len(line))

    #DF
    results_df = pd.DataFrame.from_dict(results)
    results_df.to_csv(outdir+'meta_lens.csv')


############MAIN#############
#Parse args
args = parser.parse_args()
#Data
fastadir = args.fastadir[0]
outdir = args.outdir[0]
read_fasta(fastadir, outdir)
