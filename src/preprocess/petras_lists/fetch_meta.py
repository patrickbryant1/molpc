import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Fetch all information of the different structures into one csv file.''')
parser.add_argument('--datadir', nargs=1, type= str, default=sys.stdin, help = 'Path to directory containg lists with pdb ids')

#################FUNCTIONS###############
def fetch_meta(datadir):
    '''Fetch all meta data into one csv
    '''

    meta = {'PDB':[],'Order':[],'Type':[]}
    for filename in glob.glob(datadir+'*.dat'):
        if 'misc' in filename: #Contains more info
            df = pd.read_csv(filename,header=None,sep='\t',dtype='str')
        else:
            df = pd.read_csv(filename,header=None,sep='\n',dtype='str')

        filename = filename.split('/')[-1].split('_')
        df['Order']=filename[0]
        df['Type']=filename[2]
        if 1 in df.columns:
            df['Type']=df[1]

        #Save
        meta['PDB'].extend([*df[0].values])
        meta['Order'].extend([*df['Order'].values])
        meta['Type'].extend([*df['Type'].values])



    #Create df
    meta_df = pd.DataFrame.from_dict(meta)
    meta_df.to_csv(datadir+'meta.csv',index=None)

#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
datadir = args.datadir[0]
fetch_meta(datadir)
