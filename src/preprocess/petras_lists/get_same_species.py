import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Check if all the genes in a pdb file are from the same species.''')
parser.add_argument('--species_csv', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with pdb ids and species info')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory')

#################FUNCTIONS###############
def check_species(species_csv, outdir):
    '''Check all pdb ids, their genes and species
    '''

    pdbinfo = {'PDB':[],'Entry':[], 'Taxonomic lineage (SPECIES)':[],
    'Taxonomic lineage (SUPERKINGDOM)':[]}
    #First order the PDB ids to be one per line
    for ind,row in species_csv.iterrows():
        row_pdbids = row.PDB.split(',')
        row_info = row[['Entry', 'Entry name', 'Taxonomic lineage (SPECIES)','Taxonomic lineage (SUPERKINGDOM)', 'Gene names']].values
        for i in range(len(row_pdbids)):
            pdbinfo['PDB'].append(row_pdbids[i])
            pdbinfo['Entry'].append(row.Entry)
            pdbinfo['Taxonomic lineage (SPECIES)'].append(row['Taxonomic lineage (SPECIES)'])
            pdbinfo['Taxonomic lineage (SUPERKINGDOM)'].append(row['Taxonomic lineage (SUPERKINGDOM)'])

    pdbinfo = pd.DataFrame.from_dict(pdbinfo)
    #Go through all pdb ids and see if they only have genes in the same speces
    same_species_ids = []
    for pdbid in pdbinfo.PDB.unique():
        sel = pdbinfo[pdbinfo.PDB==pdbid]
        if sel['Taxonomic lineage (SPECIES)'].unique().shape[0]>1:
            continue
        else:
            same_species_ids.append(pdbid)

    #Select
    same_species_info = pdbinfo[pdbinfo['PDB'].isin(same_species_ids)]
    same_species_info = same_species_info[['PDB', 'Taxonomic lineage (SPECIES)','Taxonomic lineage (SUPERKINGDOM)']].drop_duplicates()
    #Save
    same_species_info.to_csv(outdir+'pdbids_same_species.csv',index=None)

#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
species_csv = pd.read_csv(args.species_csv[0], sep='\t')
outdir = args.outdir[0]
check_species(species_csv, outdir)
