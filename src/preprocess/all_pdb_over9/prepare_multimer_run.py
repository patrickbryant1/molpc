import argparse
import sys
import os
import pandas as pd
import numpy as np
import itertools
import shutil
import glob
import pdb


parser = argparse.ArgumentParser(description = '''Create the folder structure for all MSAs to run the prediction using AF-multimer.''')

parser.add_argument('--msadir', nargs=1, type= str, default=sys.stdin, help = 'Path to data.')
parser.add_argument('--complex_id', nargs=1, type= str, default=sys.stdin, help = 'Id of complex used to make the MSA and output directories.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory (where to create the folder structure). Include /in end')

parser.add_argument('--useqs', nargs=1, type= str, default=sys.stdin, help = 'Path to sequences and stoichiometry for each chain in the complex.')
parser.add_argument('--interactions', nargs=1, type= str, default=sys.stdin, help = 'Known interactions between chains for the complex. If not present, all possible are used.')
parser.add_argument('--intchain2seq', nargs=1, type= str, default=sys.stdin, help = 'Mapping between each chain in the known interactions to the sequences in meta.')
parser.add_argument('--get_all', nargs=1, type= int, default=sys.stdin, help = 'If to get all interactions of size subsize.')
parser.add_argument('--subsize', nargs=1, type= int, default=sys.stdin, help = 'Size of the smallest sub-component to predict (e.g. predict all dimers (2), trimers (3) or entire complex (1)).')


#########Functions###########

def get_sub_combos(uints, subsize):
    '''Get all possible combinations of size subsize
    according to the uints
    '''

    uints_subsize = []
    uints_subsize_check = []
    for chain in np.unique(uints.values):
        sel = uints[(uints['Useq_x']==chain)|(uints['Useq_y']==chain)]
        sel = sel.reset_index()
        #Check that there are more than one int
        if len(sel)<2:
            #Save homo-trimer
            uints_subsize.append(np.array([chain,chain,chain]))
            continue
        for i in range(len(sel)):
            rowi = sel.loc[i]
            rowi_chains = rowi.values[1:]
            for j in range(i+1,len(sel)):
                rowj = sel.loc[j]
                rowj_chains = rowj.values[1:]
                #Get combos
                combos = [x for x in itertools.combinations(np.concatenate([rowi_chains,rowj_chains]), subsize)]
                for combo in combos:
                    if '-'.join([str(x) for x in combo]) not in uints_subsize_check:
                        uints_subsize.append(combo)
                        uints_subsize_check.append('-'.join([str(x) for x in combo]))
    return np.array(uints_subsize)

def create_folder_structure(msadir, complex_id, outdir, useqs, interactions, intchain2seq, get_all, subsize):
    '''Create the folder structure for AF-multimer
    For each type,
    If numeric --> skip n-1 letters in folder assignment
    E.g. A2B --> make folder A, skip B, make folder C
    '''
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'


    #Check if interactions are present
    if (len(interactions)>0 and get_all==False):
        #Merge the interactions to the intchain2seq to get the unique interactions
        interactions = pd.merge(interactions,intchain2seq,left_on='Chain1',right_on='Chain',how='left')
        interactions = pd.merge(interactions,intchain2seq,left_on='Chain2',right_on='Chain',how='left')
        uints = interactions[['Useq_x','Useq_y']].drop_duplicates()
        #Get the combos
        if subsize>2:
            combos = get_sub_combos(uints, subsize)
        else:
            combos = uints.values

    else:
        #Get all possible combinations of size subsize
        print('Creating all interactions of size',subsize,'...')

        #Create all combinations
        chains = []
        for ind,row in useqs.iterrows():
            chains.extend([row.SeqID]*row.Stoichiometry)
        combos = [x for x in itertools.combinations(chains, subsize)]
        #Get unique combos
        ucombos = []
        for combo in combos:
            if combo not in ucombos:
                ucombos.append(combo)
        combos = ucombos

    #Go through all combinations and create the AFM folder structure
    for combo in combos:
        combo = np.sort(combo)
        #Create the combo dir
        msa_chain_dir = outdir+complex_id+'_'+'-'.join([str(x) for x in combo])

        #Check if dir exists
        if os.path.exists(msa_chain_dir):
            print('Directory '+msa_chain_dir+' exists...')
            continue

        else:
            os.mkdir(msa_chain_dir)
            os.mkdir(msa_chain_dir+'/msas')

        #Write fasta
        with open(outdir+complex_id+'_'+'-'.join([str(x) for x in combo])+'.fasta', 'w') as file:
            for chain in combo:
                file.write('>'+complex_id+'_'+str(chain)+'\n')
                file.write(useqs[useqs.SeqID==chain]['Sequence'].values[0]+'\n')


        #Go through the combo and create the structure
        prev_chain = ''
        afmci = 0 #AFM chain index, e.g. 0 for A, 1 for B
        for chain in combo:
            if chain==prev_chain:
                afmci+=1
                continue
            uchain = alphabet[afmci]
            #Copy to msadir
            try:
                os.mkdir(msa_chain_dir+'/msas/'+uchain)
            except:
                print('Directory '+msa_chain_dir+'/msas/'+uchain+' exists...')
            chain_msas = glob.glob(msadir+complex_id+'_'+str(chain)+'/msas/A/*_hits*')
            #Copy
            for msa in chain_msas:
               shutil.copyfile(msa, msa_chain_dir+'/msas/'+uchain+'/'+msa.split('/')[-1])

            #Increase AFM chain index
            afmci+=1
            prev_chain = chain


################MAIN###############
#Parse args
args = parser.parse_args()
#Data
msadir = args.msadir[0]
complex_id = args.complex_id[0]
outdir = args.outdir[0]

useqs = pd.read_csv(args.useqs[0])
interactions = pd.read_csv(args.interactions[0])
intchain2seq = pd.read_csv(args.intchain2seq[0])
get_all = bool(args.get_all[0])
subsize = args.subsize[0]

if subsize>3:
    print('Currently only support subsizes up to 3')
    sys.exit()

#Create the folder structure for the AFM run
create_folder_structure(msadir, complex_id, outdir, useqs, interactions, intchain2seq, get_all, subsize)
