import argparse
import sys
import os
import numpy as np
from Bio.PDB.PDBParser import PDBParser
from collections import defaultdict
import sys
import glob
import pandas as pd
import pdb

parser = argparse.ArgumentParser(description = '''Get all interacting chains in a pdb file.
                                                If the biological assembly in the pdb file contains multiple states, write these
                                                into one state only.''')

parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to data with biological units.')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with pdb and sequence information')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Output directory')

##############FUNCTIONS##############

def format_line(atm_no, atm_name, res_name, chain, res_no, x,y,z,occ,B,atm_id):
    '''Format the line into PDB
    '''

    #Get blanks
    atm_no = ' '*(5-len(atm_no))+atm_no
    atm_name = atm_name+' '*(4-len(atm_name))
    res_no = ' '*(4-len(res_no))+res_no
    x =' '*(8-len(x))+x
    y =' '*(8-len(y))+y
    z =' '*(8-len(z))+z
    occ = ' '*(6-len(occ))+occ
    B = ' '*(6-len(B))+B

    line = 'ATOM  '+atm_no+'  '+atm_name+res_name+' '+chain+res_no+' '*4+x+y+z+occ+B+' '*11+atm_id+'  '
    return line

def read_all_states(pdbname, nchains):
    '''Read all states of a pdb file into a single file
    '''

    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    cat_model = {} #Cat all states into 1
    atm_no=0
    for model in struc:
        for chain in model:
            if chain.id in cat_model:
                chid = chain.id+'2'
            else:
                chid = chain.id
            #Save
            cat_model[chid]=[]
            #Reset res no
            res_no=0
            for residue in chain:
                res_no +=1
                res_name = residue.get_resname()
                if res_name not in [*three_to_one.keys()]:
                    continue
                for atom in residue:
                    atm_no+=1
                    if atm_no>99999:
                        print('More than 99999 atoms',pdbname)
                        return {}
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()
                    x, y, z = format(x,'.3f'),format(y,'.3f'),format(z,'.3f')
                    occ = atom.get_occupancy()
                    occ = format(occ, '.2f')
                    B = min(100,atom.get_bfactor())
                    B = format(B, '.2f')
                    #Format line
                    line = format_line(str(atm_no), atm_name, res_name, chain.id, str(res_no),
                    x,y,z,occ,B,atom_id[0])
                    cat_model[chid].append(line+'\n')


    #Check the cat model and nchains
    if len(cat_model.keys())!=nchains:
        print('Mismatch',nchains, len(cat_model.keys()), pdbname)
        return {}
    else:
        return cat_model

def write_joint_pdb(all_chains, outname):
    chain_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

    ci=0
    with open(outname, 'w') as file:
        for key in all_chains:
            cname=chain_names[ci]
            for line in all_chains[key]:
                file.write(line[:21]+cname+line[22:])

            ci+=1


def get_ints(pdbname):
    '''Get the interactions of all chains
    '''

    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    parser = PDBParser()
    try:
        struc = parser.get_structure('', pdbname)
    except:
        pdb.set_trace()
    chain_coords = {}
    chain_seqs = {}
    for model in struc:
        for chain in model:
            #Save
            chain_coords[chain.id]=[]
            chain_seqs[chain.id]=''
            for residue in chain:
                resname = residue.get_resname()
                for atom in residue:
                    if atom.name=='CB' or (atom.name=='CA' and resname=='GLY'):
                        chain_coords[chain.id].append(atom.get_coord())
                        chain_seqs[chain.id]+=three_to_one[resname]

    chains = [*chain_coords.keys()]
    int_chains = {'Chain1':[], 'Chain2':[], 'n_contacts':[]}
    for i in range(len(chains)):
        chi = chains[i]
        chi_coords = chain_coords[chi]
        l1 = len(chi_coords)
        for j in range(i+1,len(chains)):
            chj = chains[j]
            chj_coords = chain_coords[chj]
            l2 = len(chj_coords)
            #Calculate contacts
            #Calc 2-norm
            mat = np.append(chi_coords,chj_coords,axis=0)
            a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
            dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
            contact_dists = dists[l1:,:l1]
            contacts = np.argwhere(contact_dists<=8)
            #Write joint file if contacts
            if contacts.shape[0]>min(l1,l2)*0.1:
                int_chains['Chain1'].append(chi)
                int_chains['Chain2'].append(chj)
                int_chains['n_contacts'].append(contacts.shape[0])
                

    int_df = pd.DataFrame.from_dict(int_chains)
    seq_df = pd.DataFrame()
    seq_df['Chain']=[*chain_seqs.keys()]
    seq_df['Sequence']=[*chain_seqs.values()]

    return int_df, seq_df





################MAIN###############
#Parse args
args = parser.parse_args()
#Data
pdbdir = args.pdbdir[0]
meta = pd.read_csv(args.meta[0])
outdir = args.outdir[0]

for pdbid in meta['Entry ID'].unique():
    sel = meta[meta['Entry ID']==pdbid]
    nchains = sel['Total Number of Polymer Instances (Chains) per Assembly'].values[0]
    #Get all states into one
    if not os.path.exists(pdbdir+pdbid+'.pdb1'):
        continue
    all_chains = read_all_states(pdbdir+pdbid+'.pdb1',nchains)

    if len(all_chains.keys())>0:
        #Write a joint file
        write_joint_pdb(all_chains, outdir+pdbid+'.pdb')
        #Get interactions and pdb seqs
        int_df, seq_df = get_ints(outdir+pdbid+'.pdb')
        int_df.to_csv(outdir+pdbid+'_ints.csv',index=None)
        seq_df.to_csv(outdir+pdbid+'_seqs.csv',index=None)
    else:
        continue
