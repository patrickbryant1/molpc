import argparse
import sys
import os
import numpy as np
import pandas as pd
import pdb

parser = argparse.ArgumentParser(description = '''Parse all clustered sequences and select complexes based on the clusters.''')
parser.add_argument('--clusters', nargs=1, type= str, default=sys.stdin, help = 'Path to tsv with cluster information')
parser.add_argument('--pdb_to_species', nargs=1, type= str, default=sys.stdin, help = 'Path to tsv with species information')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with pdb and sequence information')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory')

###################FUNCTIONS###################

def check_species(pdb_to_species):
    '''Check that the PDBs have all chain seqs in the same species
    '''

    #First map all pdb ids to the species in a one-to-one relationship
    #The reason this is necessary is beacuse a uniprot ID may be present in several
    #PDB entries
    PDB = []
    species = []
    kd = []
    for ind,row in pdb_to_species.iterrows():
        for pdbid in row.PDB.split(','):
            PDB.append(pdbid)
            species.append(row['Taxonomic lineage (SPECIES)'])
            kd.append(row['Taxonomic lineage (SUPERKINGDOM)'])

    #DF
    pdb_to_species_arranged = pd.DataFrame()
    pdb_to_species_arranged['PDB']=PDB
    pdb_to_species_arranged['Species']=species
    pdb_to_species_arranged['Kingdom']=kd

    #Go through each PDB id and see that it does not contain more than one species
    keep_ids = []
    for pdbid in pdb_to_species_arranged.PDB.unique():
        sel = pdb_to_species_arranged[pdb_to_species_arranged.PDB==pdbid]
        if sel.Species.unique().shape[0]==1:
            keep_ids.append(pdbid)

    return np.array(keep_ids), pdb_to_species_arranged.drop_duplicates()



def select_complexes(merged, outdir):
    '''Select the complexes to be used
    '''

    #Get clusters per complex
    clusters_per_complex = {}
    nclusters_per_complex = {}
    for id in merged['Entry ID'].unique():
        sel = merged[merged['Entry ID']==id]
        clusters_per_complex[id] = sel.cluster.unique()
        nclusters_per_complex[id] = sel.cluster.unique().shape[0]

    #Selected complexes
    selected_complexes = []
    #Compare all complexes
    #Sort by nclusters_per_complex
    complexes = [*np.array([*nclusters_per_complex.keys()])[np.argsort([*nclusters_per_complex.values()])[::-1]]]

    while len(complexes)>0:
        complex = complexes.pop(0)
        complex_clusters = clusters_per_complex[complex] #Clusters for complex
        ncc = len(complex_clusters) #Number of clusters in ncc
        selected_complexes.append(complex) #Save the complex
        #Check all remaining complexes
        for check_complex in complexes:
            ccn = clusters_per_complex[check_complex]
            if np.intersect1d(complex_clusters,ccn).shape[0]>=len(ccn):
                complexes = [*np.setdiff1d(complexes, check_complex)]
    print('Selected',len(selected_complexes),'out of' ,len(clusters_per_complex.keys()), 'complexes based on overlap')

    #Select
    sel = merged[merged['Entry ID'].isin(selected_complexes)]
    #Select on chain length
    under50_ids = sel[sel['Polymer Entity Sequence Length']<50]['Entry ID']
    sel = sel[~sel['Entry ID'].isin(under50_ids.unique())]
    print('Selected',len(sel['Entry ID'].unique()),'out of' ,len(selected_complexes), 'complexes based on seqlen')
    sel = sel.reset_index()
    sel = sel.drop(columns={1})
    sel[sel.columns[1:]].to_csv(outdir+'selected_complexes.csv',index=None)

#################MAIN####################

#Parse args
args = parser.parse_args()
#Args
clusters = pd.read_csv(args.clusters[0], sep='\t', header=None)
pdb_to_species = pd.read_csv(args.pdb_to_species[0], sep='\t')
meta = pd.read_csv(args.meta[0])
seqids = np.array([str(x) for x in meta.SeqID.values])
meta[1]=meta['Entry ID']+'_'+seqids
outdir = args.outdir[0]
#Check species
keep_ids, pdb_to_species_arranged = check_species(pdb_to_species)
#Merge
merged = pd.merge(meta, clusters,on=1, how='left')
merged = merged.rename(columns={0:'cluster'})
#Remove complexes that have more than 30 chains in the biological assembly
merged = merged[merged['Total Number of Polymer Instances (Chains) per Assembly']<=30]
print('Originially',merged['Entry ID'].unique().shape[0],'complexes')
#Remove complexes that have less than 10 chains in the biological assembly
merged = merged[merged['Total Number of Polymer Instances (Chains) per Assembly']>=10]
print('Over 9 in assembly:',merged['Entry ID'].unique().shape[0],'complexes')
#Select the PDB ids that are in keep ids (same species)
merged = merged[merged['Entry ID'].isin(keep_ids)]
print('Same species:',merged['Entry ID'].unique().shape[0],'complexes')
#Merge to get species
merged = pd.merge(merged, pdb_to_species_arranged, left_on='Entry ID', right_on='PDB',how='left')
select_complexes(merged, outdir)
