import argparse
import sys
import os
import numpy as np
import pandas as pd
import pdb

parser = argparse.ArgumentParser(description = '''Fetch all unique chains belonging to each PDB.''')
parser.add_argument('--csv', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with pdb information')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory')

###################FUNCTIONS###################

def parse_csv(csv):
    '''Parse the csv file
    '''

    csv_dict = {}
    with open(csv, 'r') as file:
        li = 0
        for line in file:
            line = line.rstrip()
            line = line.split(',')
            if li ==0:
                for key in line:
                    if len(key)>0:
                        csv_dict[key.split('"')[1]]=[]

            else:
                if len(line[0])>1:
                    pdbid = line[0].split('"')[1]
                    cpa = int(line[4])
                    oc = int(line[5])
                try:

                    csv_dict['Sequence'].append(line[1].split('"')[1])
                    csv_dict['Entry ID'].append(pdbid)
                    csv_dict['Total Number of polymer Entity Instances (Chains) per Entity'].append(int(line[2]))
                    csv_dict['Polymer Entity Sequence Length'].append(int(line[3]))
                    csv_dict['Total Number of Polymer Instances (Chains) per Assembly'].append(cpa)
                    csv_dict['Oligomeric Count'].append(oc)
                except:
                    continue

            li+=1

    #Df
    df = pd.DataFrame.from_dict(csv_dict)

    return df


def write_fasta_assign_chains(df, outdir):
    '''Assign chain ids for each sequence in each complex
    '''

    seqids = []
    for id in df['Entry ID'].unique():
        sel = df[df['Entry ID']==id]
        for i in range(len(sel)):
            seqids.append(i+1)
    df['SeqID']=seqids

    #Write fasta for clustering
    with open(outdir+'all.fasta', 'w') as file:
        for ind,row in df.iterrows():
            file.write('>'+row['Entry ID']+'_'+str(row.SeqID)+'\n')
            file.write(row.Sequence+'\n')

    #Save df
    df.to_csv(outdir+'all.csv', index=None)
    print('Written csv to',outdir+'all.csv' )

#################MAIN####################

#Parse args
args = parser.parse_args()
#Args
csv = args.csv[0]
outdir = args.outdir[0]
df = parse_csv(csv)
write_fasta_assign_chains(df, outdir)
