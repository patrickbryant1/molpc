import argparse
import sys
import os
import numpy as np
from Bio import pairwise2
import sys
from collections import Counter
import pandas as pd
import pdb

parser = argparse.ArgumentParser(description = '''Map PDB chains to seqres and check
                                                stoichiometry/rewrite it according to the PDB file.
                                                The stoichiometry reported in PDB stats is not necessarily according to
                                                biological assembly 1.''')

parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to data with biological units.')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with pdb and sequence information')

##############FUNCTIONS##############

def match_chains(sel, pdbseqs, pdbdir, pdbid):
    '''Match seqs
    '''

    #Match each PDB seq to the seqs in sel
    useq = []
    #For all PDB seqs
    for ind1,row1 in pdbseqs.iterrows():
        alis=0 #Alignment score
        #For all useqs
        for ind2,row2 in sel.iterrows():
            #Align
            score = pairwise2.align.globalxx(row1.Sequence, row2.Sequence)[0][2]
            if score>alis:
                alis=score
                match=row2.SeqID
        useq.append(match)
    #Save matches
    pdbseqs['Useq']=useq
    #Update stoichiometry in sel
    sel['Stoichiometry']=0
    counts = Counter(useq)
    for key in counts:
        sel.at[sel[sel.SeqID==key].index[0],'Stoichiometry']=counts[key]
    sel =sel.reset_index()
    sel = sel.drop(columns={'index'})
    #Save
    pdbseqs.to_csv(pdbdir+pdbid+'_seqs.csv',index=None)
    sel.to_csv(pdbdir+pdbid+'_useqs.csv',index=None)

################MAIN###############
#Parse args
args = parser.parse_args()
#Data
pdbdir = args.pdbdir[0]
meta = pd.read_csv(args.meta[0])


for pdbid in meta['Entry ID'].unique():
    sel = meta[meta['Entry ID']==pdbid]
    nchains = sel['Total Number of Polymer Instances (Chains) per Assembly'].values[0]
    #Get all states into one
    if not os.path.exists(pdbdir+pdbid+'_seqs.csv'):
        continue
    else:
        pdbseqs = pd.read_csv(pdbdir+pdbid+'_seqs.csv')
    #Match chains, get mapping and stoichiometry
    match_chains(sel, pdbseqs, pdbdir, pdbid)
