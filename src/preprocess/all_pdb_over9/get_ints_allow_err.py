import argparse
import sys
import os
import numpy as np
from collections import defaultdict
import sys
import glob
import pandas as pd
import pdb

parser = argparse.ArgumentParser(description = '''Get all interacting chains in a pdb file.
                                                This version allows errors in the PDB file such as discontinuos chains, which
                                                was not allowed for the main analysis.
                                                If the biological assembly in the pdb file contains multiple states, write these
                                                into one state only.''')

parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to data with biological units.')
parser.add_argument('--pdbid', nargs=1, type= str, default=sys.stdin, help = 'PDB ID.')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with pdb and sequence information')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Output directory')

##############FUNCTIONS##############

def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''
    pdb_chains = {}
    pdb_coords = {}
    pdb_seqs = {}
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}
    with open(pdbfile) as file:
        for line in file:
            if not line.startswith('ATOM'):
                continue
            record = parse_atm_record(line)
            if record['chain'] in [*pdb_chains.keys()]:
                pdb_chains[record['chain']].append(line)
                if record['atm_name']=='CB' or (record['atm_name']=='CA' and record['res_name']=='GLY'):
                    pdb_coords[record['chain']].append([record['x'],record['y'],record['z']])
                if record['atm_name']=='CA':
                    pdb_seqs[record['chain']]+=three_to_one[record['res_name']]
            else:
                pdb_chains[record['chain']]= [line]
                pdb_coords[record['chain']]= []
                pdb_seqs[record['chain']]=''

    return pdb_chains, pdb_coords, pdb_seqs


def write_joint_pdb(all_chains, outname):
    chain_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

    ci=0
    with open(outname, 'w') as file:
        for key in all_chains:
            cname=chain_names[ci]
            for line in all_chains[key]:
                file.write(line[:21]+cname+line[22:])

            ci+=1


def get_ints(pdbname):
    '''Get the interactions of all chains
    '''

    all_chains, all_coords, all_seqs = read_pdb(pdbname)

    chains = [*all_coords.keys()]
    int_chains = {'Chain1':[], 'Chain2':[]}
    for i in range(len(chains)):
        chi = chains[i]
        chi_coords = np.array(all_coords[chi])
        l1 = len(chi_coords)
        for j in range(i+1,len(chains)):
            chj = chains[j]
            chj_coords = np.array(all_coords[chj])
            l2 = len(chj_coords)
            #Calculate contacts
            #Calc 2-norm
            mat = np.append(chi_coords,chj_coords,axis=0)
            a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
            dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
            contact_dists = dists[l1:,:l1]
            contacts = np.argwhere(contact_dists<=8)
            #Write joint file if contacts
            if contacts.shape[0]>min(l1,l2)*0.1:
                int_chains['Chain1'].append(chi)
                int_chains['Chain2'].append(chj)


    int_df = pd.DataFrame.from_dict(int_chains)
    seq_df = pd.DataFrame()
    seq_df['Chain']=[*all_seqs.keys()]
    seq_df['Sequence']=[*all_seqs.values()]

    return int_df, seq_df





################MAIN###############
#Parse args
args = parser.parse_args()
#Data
pdbdir = args.pdbdir[0]
pdbid = args.pdbid[0]
meta = pd.read_csv(args.meta[0])
outdir = args.outdir[0]


sel = meta[meta['Entry ID']==pdbid]
nchains = sel['Total Number of Polymer Instances (Chains) per Assembly'].values[0]
#Get all states into one
if not os.path.exists(pdbdir+pdbid+'.pdb1'):
    sys.exit()
all_chains, all_coords, all_seqs = read_pdb(pdbdir+pdbid+'.pdb1')
if len(all_chains.keys())>0:
    #Write a joint file
    write_joint_pdb(all_chains, outdir+pdbid+'.pdb')

    #Get interactions and pdb seqs
    int_df, seq_df = get_ints(outdir+pdbid+'.pdb')
    int_df.to_csv(outdir+pdbid+'_ints.csv',index=None)
    seq_df.to_csv(outdir+pdbid+'_seqs.csv',index=None)
