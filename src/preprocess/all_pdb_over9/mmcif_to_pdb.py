import argparse
import sys
import os
import numpy as np
from Bio.PDB import MMCIFParser
from collections import defaultdict
import sys
import pdb

parser = argparse.ArgumentParser(description = '''Convert .''')

parser.add_argument('--mmcif_file', nargs=1, type= str, default=sys.stdin, help = 'Path to data.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Output directory')
parser.add_argument('--id', nargs=1, type= str, default=sys.stdin, help = 'PDB id')



def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record


def format_line(atm_no, atm_name, res_name, chain, res_no, x,y,z,occ,B,atm_id):
    '''Format the line into PDB
    '''

    #Get blanks
    atm_no = ' '*(5-len(atm_no))+atm_no
    atm_name = atm_name+' '*(4-len(atm_name))
    res_no = ' '*(4-len(res_no))+res_no
    x =' '*(8-len(x))+x
    y =' '*(8-len(y))+y
    z =' '*(8-len(z))+z
    occ = ' '*(6-len(occ))+occ
    B = ' '*(6-len(B))+B

    line = 'ATOM  '+atm_no+'  '+atm_name+res_name+' '+chain+res_no+' '*4+x+y+z+occ+B+' '*11+atm_id+'  '
    return line


def convert_to_pdb(filename,outdir,id):
    '''Convert a mmcif file to PDB format
    '''
    parser = MMCIFParser()
    structure = parser.get_structure("",filename)

    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    chain_names = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                    'y', 'z']

    model = structure[0]
    atm_no=0
    chain_ind=0

    if len([c for c in model.get_chains()])>2*len(chain_names):
        print('Too many chains for PDB format')
        sys.exit()

    with open(outdir+id+'.pdb', 'w') as file:
        for chain in model:
            if chain_ind>=len(chain_names):
                chain_name = chain_names[chain_ind-len(chain_names)].upper()
            else:
                chain_name = chain_names[chain_ind]
            print('Chain',chain_name)
            res_no=0

            for residue in chain:
                res_no +=1
                res_name = residue.get_resname()
                if res_name not in [*three_to_one.keys()]:
                    print(res_name)
                    chain_ind+=1
                    break
                for atom in residue:
                    atm_no+=1
                    if atm_no>99999:
                        sys.exit()
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()
                    x, y, z = format(x,'.3f'),format(y,'.3f'),format(z,'.3f')
                    occ = atom.get_occupancy()
                    B = min(100,atom.get_bfactor())
                    #Format line
                    line = format_line(str(atm_no), atm_name, res_name, chain_name, str(res_no),
                    x,y,z,str(occ),str(B),atom_id[0])
                    file.write(line+'\n')


            if res_name in [*three_to_one.keys()]:
                file.write('TER\n')
                chain_ind+=1

##################MAIN#######################

#Parse args
args = parser.parse_args()
#Data
mmcif_file = args.mmcif_file[0]
outdir = args.outdir[0]
id = args.id[0]
#Convert to upper case
id = id.upper()

#Convert
convert_to_pdb(mmcif_file, outdir, id)
