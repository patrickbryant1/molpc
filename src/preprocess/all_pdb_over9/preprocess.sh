#Parse all seqs
CSV=../../../data/all_pdb_over9/rcsb_pdb_custom_report_20220110131114.csv
OUTDIR=../../../data/all_pdb_over9/
#python3 ./parse_uchains.py --csv $CSV --outdir $OUTDIR

#Cluster
MMSEQS=/home/patrick/mmseqs/bin/mmseqs
SEQS=../../../data/all_pdb_over9/all.fasta
SEQID=0.2
#$MMSEQS easy-cluster $SEQS $OUTDIR/clusterRes /tmp  --min-seq-id $SEQID -c 0.8 --cov-mode 1

#Parse clusters and select complexes
#Check that all chains within all complexes are in the same species
CLUSTS=../../../data/all_pdb_over9/clusterRes_cluster.tsv
PDB_SPECIES=../../../data/all_pdb_over9/pdb_to_species.tsv
META=../../../data/all_pdb_over9/all.csv
#python3 ./parse_and_select.py --clusters $CLUSTS --pdb_to_species $PDB_SPECIES \
#--meta $META --outdir $OUTDIR

#Write single chain fasta for MSA creation
SEL_COMPLEXES=../../../data/all_pdb_over9/selected_complexes.csv
OUTDIR=../../../data/all_pdb_over9/fasta/
#python3 ./write_sc_fasta.py --selected_complexes $SEL_COMPLEXES --outdir $OUTDIR

#Write joint files using all states in the biological assemblies and
#Get all true interactions from the selected PDB files
PDBDIR=../../../data/all_pdb_over9/PDB/
META=../../../data/all_pdb_over9/selected_complexes.csv
OUTDIR=$PDBDIR
#python3 ./get_ints.py --pdbdir $PDBDIR --meta $META --outdir $OUTDIR

#Match the pdb sequences to seqres and see which are thereby interacting
#Also double check the stoichiometry and write the stoichiometry
#of each chain in meta according to the PDB file
PDBDIR=../../../data/all_pdb_over9/PDB/
META=../../../data/all_pdb_over9/selected_complexes.csv
python3 ./pdbseq_to_useq.py --pdbdir $PDBDIR --meta $META

##########################################
#Create all single chain MSAs using AFM...
##########################################

#Write the folder structure for AFM to predict sub-components
MSADIR=./
COMPLEX_ID=6J0B #Id of complex to write dirs for
OUTDIR=./
USEQS=../../../data/all_pdb_over9/PDB/$COMPLEX_ID'_useqs.csv' #Unique sequences and their stoichiometry
INTERACTIONS=../../../data/all_pdb_over9/PDB/$COMPLEX_ID'_ints.csv' #Known interactions
INTCHAIN2SEQ=../../../data/all_pdb_over9/PDB/$COMPLEX_ID'_seqs.csv' #Chain in known interactions to sequence in meta
GET_ALL=1 #If to get all interactions anyway (1) or not (0)
SUBSIZE=2 #What order the subcomplexes should be

# python3 ./prepare_multimer_run.py --msadir $MSADIR \
# --complex_id $COMPLEX_ID \
# --outdir $OUTDIR \
# --useqs $USEQS \
# --interactions $INTERACTIONS \
# --intchain2seq $INTCHAIN2SEQ \
# --get_all $GET_ALL \
# --subsize $SUBSIZE
