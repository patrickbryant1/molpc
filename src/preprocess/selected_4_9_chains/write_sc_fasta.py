import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Fetch all unique chains belonging to each PDB.''')
parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to dir with csvs with sequences of the selected complexes')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory')

###################FUNCTIONS###################

def write_fasta(df, outdir):
    '''Assign chain ids for each sequence in each complex
    '''

    for ind,row in df.iterrows():
        #Write fasta for MSA creation
        try:
            with open(outdir+row['ID']+'_'+str(row.SeqID)+'.fasta', 'w') as file:
                file.write('>'+row['ID']+'_'+str(row.SeqID)+'\n')
                file.write(row.Sequence+'\n')
        except:
            pdb.set_trace()

#################MAIN####################

#Parse args
args = parser.parse_args()
#Args
pdbdir = args.pdbdir[0]
outdir = args.outdir[0]

useq_dfs = glob.glob(pdbdir+'*_useqs.csv')
for name in useq_dfs:
    df = pd.read_csv(name, dtype={'ID': 'str'})
    write_fasta(df, outdir)
