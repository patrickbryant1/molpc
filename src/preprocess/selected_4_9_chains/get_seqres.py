import argparse
import sys
import os
import numpy as np
import shutil
import sys
import glob
import pandas as pd
import pdb

parser = argparse.ArgumentParser(description = '''Get SEQRES from PDB files.''')

parser.add_argument('--datadir', nargs=1, type= str, default=sys.stdin, help = 'Path to data with biological units.')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with pdb and sequence information')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Output directory')

###################FUNCTIONS###################
def read_seqres(fastaname):
    '''Get the SEQRES per chain for a PDB file
    '''
    chain_seqs = {}
    nchains = 0
    with open(fastaname, 'r') as file:
        for line in file:
            if line[0]=='>':
                nchains+=1
            else:
                sequence = line.rstrip()
                chain_seqs[nchains]=sequence

    return chain_seqs

def write_csvs(chain_seqs, ID, outdir):
    '''Create csv with unique sequences and chain mapping
    '''

    #Get the stoichiometry
    useqs = {}
    for chain in chain_seqs:
        if chain_seqs[chain] in [*useqs.keys()]:
            useqs[chain_seqs[chain]]+=1
        else:
            useqs[chain_seqs[chain]]=1

    #Create useq df
    useq_df = pd.DataFrame()
    useq_df['Stoichiometry'] = [useqs[x] for x in useqs]
    useq_df['SeqID'] = np.arange(1,len(useqs.keys())+1)
    useq_df['Sequence'] =[x for x in useqs]
    useq_df['ID'] = ID

    #Create chain df
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    chain_df = pd.DataFrame()
    chain_mapping = []
    for ind, row in useq_df.iterrows():
        chain_mapping.extend([row.SeqID]*row.Stoichiometry)
    chain_df['Useq']=chain_mapping
    chain_df['Chain']=[x for x in alphabet[:len(chain_df)]]
    #Save
    useq_df.to_csv(outdir+ID+'_useqs.csv',index=None)
    chain_df.to_csv(outdir+ID+'_chains.csv',index=None)

################MAIN###############
#Parse args
args = parser.parse_args()
#Data
datadir = args.datadir[0]
meta = pd.read_csv(args.meta[0])
outdir = args.outdir[0]

#Go through all PDB ids
for ind, row in meta.iterrows():
    chain_seqs = read_seqres(datadir+str(row.num_chains)+'mer'+'/seqres/'+row.pdbid+'.fa')
    if len(chain_seqs.keys())!=row.num_chains:
        pdb.set_trace()
    #Copy pdb to outdir
    shutil.copy(datadir+str(row.num_chains)+'mer'+'/pdb_template/'+row.pdbid+'.pdb', outdir)
    write_csvs(chain_seqs, row.pdbid, outdir)
