import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import Counter
import pdb

parser = argparse.ArgumentParser(description = '''Select 50 complexes randomly from each oligomer type.''')
parser.add_argument('--complex_id_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to dir with all complex PDB ids')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to meta info for complexes')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory')

###################FUNCTIONS###################

def select_complexes(complex_id_dir, meta, outdir):
    '''Select complexes
    '''

    #Order by PDB id
    ordered_complexes = {'pdbid':[], 'num_chains':[]}
    #Get all complex ids
    xmer_files = glob.glob(complex_id_dir+'*.txt')
    for name in xmer_files:
        xmers = pd.read_csv(name, sep='\n', header=None)
        ordered_complexes['pdbid'].extend(xmers[0].values)
        ordered_complexes['num_chains'].extend([int(name.split('/')[-1][3])]*len(xmers))
    #Df
    ordered_complexes = pd.DataFrame.from_dict(ordered_complexes)
    #Get homo/heteromer
    ordered_complexes = pd.merge(ordered_complexes, meta[['pdbid','class']],on='pdbid',how='left')
    ordered_complexes = ordered_complexes.drop_duplicates()
    #Save the ordered complexes
    ordered_complexes.to_csv(outdir+'ordered_complexes_2_9.csv',index=None)

    #Analyse counts
    counts = Counter(ordered_complexes.num_chains.values)
    for nchains in range(2,10):
        print(nchains,counts[nchains])

    #Select
    sel_inds =[]
    for nchains in range(4,10):
        sel = ordered_complexes[ordered_complexes.num_chains==nchains]
        sel = sel.reset_index()
        nchoice = min(50,len(sel))
        sel = sel.loc[np.random.choice(len(sel),nchoice,replace=False)]
        sel_inds.extend([*sel['index'].values])

    #Select complexes
    selected_complexes = ordered_complexes.loc[sel_inds]
    #Save
    selected_complexes.to_csv(outdir+'selected_complexes.csv',index=None)

#################MAIN####################

#Parse args
args = parser.parse_args()
#Args
complex_id_dir = args.complex_id_dir[0]
meta = pd.read_csv(args.meta[0])
outdir = args.outdir[0]

select_complexes(complex_id_dir, meta, outdir)
