#Select complexes
COMPLEX_ID_DIR=../../../data/selected_4_9_chains/complex_ids_per_oligomer/
META=../../../data/selected_4_9_chains/basicinfo_2-9.csv
OUTDIR=../../../data/selected_4_9_chains/
#python3 ./select_complexes.py --complex_id_dir $COMPLEX_ID_DIR \
#--meta $META \
#--outdir $OUTDIR

#Get SEQRES
DATADIR=/proj/berzelius-2021-29/users/x_patbr/data/molpc_4_9_chains/alldata/
META=../../../data/selected_4_9_chains/selected_complexes.csv
OUTDIR=../../../data/selected_4_9_chains/PDB/
#python3 ./get_seqres.py --datadir $DATADIR \
#--meta $META --outdir $OUTDIR

#Write SC fasta
OUTDIR=../../../data/selected_4_9_chains/fasta/
python3 ./write_sc_fasta.py --pdbdir ../../../data/selected_4_9_chains/PDB/ \
--outdir $OUTDIR
