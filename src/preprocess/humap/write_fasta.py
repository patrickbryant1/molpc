import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import networkx as nx
import seaborn as sns
import matplotlib.pyplot as plt
import pdb

parser = argparse.ArgumentParser(description = '''See how many of the hu.MAP complexes are in the PDB.''')

parser.add_argument('--seqs', nargs=1, type= str, default=sys.stdin, help = 'Path to sequences (REFSEQ).')
parser.add_argument('--humap_complexes', nargs=1, type= str, default=sys.stdin, help = 'Path to hu.MAP complexes in pdb.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

###################FUNCTIONS####################
def write_fasta(humap_complexes, seqs, outdir):
    '''Write fasta files
    '''

    humap_complexes_in_pdb = humap_complexes[humap_complexes.in_pdb==1]
    humap_complexes_in_pdb = humap_complexes_in_pdb.reset_index()
    for i in range(len(humap_complexes_in_pdb)):
        row = humap_complexes_in_pdb.loc[i]
        ids = row.Uniprot_ACCs.split()
        humap_id = row.HuMAP2_ID

        #Write individual fastas
        #Go through ids and get sequence
        for id in ids:
            seq = seqs[seqs.Entry==id].Sequence.values[0]
            with open(outdir+id+'.fasta','w') as file:
                file.write('>'+id+'\n')
                file.write(seq)

        #Write all together
        with open(outdir+'entire_complex/'+humap_id+'.fasta','w') as file:
            for id in ids:
                seq = seqs[seqs.Entry==id].Sequence.values[0]
                file.write('>'+id+'\n')
                file.write(seq+'\n')


##################MAIN#######################

#Parse args
args = parser.parse_args()
#Data
seqs = pd.read_csv(args.seqs[0], sep='\t')
humap_complexes = pd.read_csv(args.humap_complexes[0])
outdir = args.outdir[0]


write_fasta(humap_complexes, seqs, outdir)
