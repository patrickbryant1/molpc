import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import networkx as nx
import seaborn as sns
import matplotlib.pyplot as plt
import pdb

parser = argparse.ArgumentParser(description = '''Extract all single chains to map to pdb and analyse data.''')

parser.add_argument('--humap_complexes', nargs=1, type= str, default=sys.stdin, help = 'Path to data.')
parser.add_argument('--plotdir', nargs=1, type= str, default=sys.stdin, help = 'Path to plot directory.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')



##################MAIN#######################

def analyse_humap(humap_complexes, plotdir, outdir):
    '''Get all single chains
    '''

    u_genes = []
    n_chains = []
    for i in range(len(humap_complexes)):
        row = humap_complexes.loc[i]
        u_genes.extend(row.Uniprot_ACCs.split())
        n_chains.append(len(row.Uniprot_ACCs.split()))


    print('Number of unique genes', np.unique(u_genes).shape[0])
    print('Number of genes in total', len(u_genes))
    #Add n chains
    humap_complexes['number_of_chains']=n_chains
    #Save
    humap_complexes.to_csv(args.humap_complexes[0],index=None)

    #Save unique genes
    u_gene_df = pd.DataFrame()
    u_gene_df['Uniprot_ACC'] = np.unique(u_genes)
    u_gene_df.to_csv(outdir+'unique_uniprot_ids.csv',index=None)

    #Plot nchains vs confidence
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    sns.kdeplot(humap_complexes.number_of_chains,humap_complexes.Confidence)
    plt.xlim([0,10])
    plt.xlabel('Number of chains')
    plt.savefig(plotdir+'kde_nchains_conf.svg',format='svg',dpi=300)
    plt.close()
    #Plot nchains distr
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.hist(humap_complexes.number_of_chains)
    plt.yscale('log')
    plt.xlabel('Number of chains')
    plt.ylabel('Count')
    plt.tight_layout()
    plt.savefig(plotdir+'nchains_distr.svg',format='svg',dpi=300)
    plt.close()

############MAIN#############
#Parse args
args = parser.parse_args()
#Data
humap_complexes = pd.read_csv(args.humap_complexes[0])
plotdir = args.plotdir[0]
outdir = args.outdir[0]
analyse_humap(humap_complexes, plotdir, outdir)
