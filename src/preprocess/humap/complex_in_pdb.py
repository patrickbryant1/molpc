import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import networkx as nx
import seaborn as sns
import matplotlib.pyplot as plt
import pdb

parser = argparse.ArgumentParser(description = '''See how many of the hu.MAP complexes are in the PDB.''')

parser.add_argument('--humap_complexes', nargs=1, type= str, default=sys.stdin, help = 'Path to hu.MAP complexes.')
parser.add_argument('--uniprot_to_pdb', nargs=1, type= str, default=sys.stdin, help = 'Path to uniprot to PDB mapping.')
parser.add_argument('--nchains', nargs=1, type= int, default=sys.stdin, help = 'Minimum number of chains.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

###################FUNCTIONS####################
def check_pdb_occ(humap_complexes, uniprot_to_pdb, nchains, outdir):
    '''Check how which hu.MAP complexes are in the PDB
    '''

    #Select on number of chains
    humap_complexes = humap_complexes[humap_complexes.number_of_chains>=3]
    humap_complexes = humap_complexes.reset_index()
    print('Number of complexes with at least',nchains, 'chains:', len(humap_complexes))
    in_pdb = []
    pdb_ids = []
    #Check if the uniprot accs map to the same PDB id
    for i in range(len(humap_complexes)):
        row = humap_complexes.loc[i]
        uni_accs = row.Uniprot_ACCs.split()
        overlapping_ids = uniprot_to_pdb[uniprot_to_pdb.From==uni_accs[0]].To.values
        for j in range(1,len(uni_accs)):
            curr_ids = uniprot_to_pdb[uniprot_to_pdb.From==uni_accs[j]].To.values
            overlapping_ids = np.intersect1d(overlapping_ids,curr_ids)
            if len(overlapping_ids)<1:
                in_pdb.append(0)
                pdb_ids.append('')
                break

        if len(overlapping_ids)>0:
            in_pdb.append(1)
            pdb_ids.append(' '.join(overlapping_ids))

    humap_complexes['in_pdb']=in_pdb
    humap_complexes['PDB_ID']=pdb_ids
    #Save
    humap_complexes.to_csv(args.humap_complexes[0],index=None)
    #Check how many are in the PDB
    sel_in_pdb = humap_complexes[humap_complexes.in_pdb==1]
    sel_in_pdb = sel_in_pdb.reset_index()
    print('There are',len(sel_in_pdb),'complexes with at least',nchains,'chains in the PDB.')
    #Save these
    #Get all PDB ids
    humap_ids = []
    pdb_ids = []
    for i in range(len(sel_in_pdb)):
        row = sel_in_pdb.loc[i]
        pdb_ids.extend(row.PDB_ID.split())
        humap_ids.extend([row.HuMAP2_ID]*len(row.PDB_ID.split()))

    pdb_in_humap = pd.DataFrame()
    pdb_in_humap['HuMAP2_ID'] = humap_ids
    pdb_in_humap['PDB_ID'] = pdb_ids
    pdb_in_humap.to_csv(outdir+'pdb_in_humap.csv',index=None)


##################MAIN#######################

#Parse args
args = parser.parse_args()
#Data
humap_complexes = pd.read_csv(args.humap_complexes[0])
uniprot_to_pdb = pd.read_csv(args.uniprot_to_pdb[0], sep='\t')
nchains = args.nchains[0]
outdir = args.outdir[0]

check_pdb_occ(humap_complexes, uniprot_to_pdb, nchains, outdir)
